import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PhoneIcon from "@material-ui/icons/ViewList";
import ListAlt from "@material-ui/icons/ListAlt";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import WithdrawIcon from "@material-ui/icons/AccountBalance";
import Typography from "@material-ui/core/Typography";
import DashboardInviteList from "./DashboardInviteList.jsx";
import DashboardInvitedList from "./DashboardInvitedList.jsx";
import DashbordProfitTabs from "./DashbordProfitTabs.jsx";
import DashboardWithdrawTabs from "./DashboardWithdrawTabs.jsx";
import Config from "../../config";
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};
const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  font: {
    fontSize: 20
  }
});

function Dashboard(props) {
  const { classes } = props;
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="secondary"
          textColor="secondary"
        >
          <Tab
            label={Config.CONST_YQMGL}
            className={classes.font}
            icon={<PhoneIcon />}
          />
          <Tab
            label={Config.CONST_BYQRLB}
            className={classes.font}
            icon={<ListAlt />}
          />
          <Tab
            label={Config.CONST_WDSY}
            className={classes.font}
            icon={<PersonPinIcon />}
          />
          {Config.CONST_PREDATOR === window.user.user.role && (
            <Tab
              label={Config.CONST_QBGL}
              className={classes.font}
              icon={<WithdrawIcon />}
            />
          )}
        </Tabs>
      </AppBar>
      {value === 0 && (
        <TabContainer>
          <DashboardInviteList />
        </TabContainer>
      )}
      {value === 1 && (
        <TabContainer>
          <DashboardInvitedList />
        </TabContainer>
      )}
      {value === 2 && (
        <TabContainer>
          <DashbordProfitTabs />
        </TabContainer>
      )}
      {value === 3 && Config.CONST_PREDATOR === window.user.user.role && (
        <TabContainer>
          <DashboardWithdrawTabs />
        </TabContainer>
      )}
    </div>
  );
}

export default withStyles(styles)(Dashboard);
