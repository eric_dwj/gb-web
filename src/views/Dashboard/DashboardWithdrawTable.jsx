import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import Tooltip from "@material-ui/core/Tooltip";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import Button from "@material-ui/core/Button";
import Config from "../../config";
import Tool from "../../api/util/common";
import { EventEmitter } from "../../api/util/events";
import withdrawServ from "../../api/service/withdraw";

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const rows = [
  {
    id: Config.CONST_ZHUANGTAI,
    numeric: true,
    disablePadding: false,
    label: Config.CONST_ZHUANGTAI
  },
  {
    id: "TXID",
    numeric: false,
    disablePadding: true,
    label: "TX"
  },
  {
    id: Config.CONST_YHID,
    numeric: true,
    disablePadding: false,
    label: Config.CONST_YHID
  },
  {
    id: Config.CONST_SHULIANG,
    numeric: true,
    disablePadding: false,
    label: Config.CONST_SHULIANG
  },
  {
    id: Config.CONST_QBSKDZ,
    numeric: true,
    disablePadding: false,
    label: Config.CONST_QBSKDZ
  },
  {
    id: Config.CONST_SHIJIAN,
    numeric: true,
    disablePadding: false,
    label: Config.CONST_SHIJIAN
  }
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            {this.props.oper === Config.CONST_PENDING && (
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={numSelected === rowCount}
                onChange={onSelectAllClick}
              />
            )}
          </TableCell>

          {rows.map(
            row => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={
                  orderBy === row.transaction_wthw_id ? order : false
                }
                style={{ fontSize: 16 }}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.transaction_wthw_id}
                    direction={order}
                    onClick={this.createSortHandler(row.transaction_wthw_id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  oper: PropTypes.string.isRequired
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
});

const calculateTotal = (list, selecteds) => {
  let total = 0;
  for (const item of list) {
    for (const selected of selecteds) {
      if (item.transaction_wthw_id === selected) {
        total = Tool.add(total, item.value);
      }
    }
  }
  return total;
};

let EnhancedTableToolbar = props => {
  const { selected, list, coin, onApprove, classes } = props;
  const numSelected = selected.length;
  const total = calculateTotal(list, selected);
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 && (
          <Typography color="inherit" variant="subtitle1">
            {`${Config.CONST_YIXUANZHONG}${numSelected}${
              Config.CONST_TJL
            }${total}${coin}`}
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 && (
          <Button
            onClick={() => {
              withdrawServ.updateStatus(coin, selected).then(resp => {
                onApprove(resp);
              });
            }}
            variant="contained"
            color="primary"
          >
            {Config.CONST_TIJIAO}
          </Button>
        )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  selected: PropTypes.array.isRequired,
  list: PropTypes.array.isRequired,
  coin: PropTypes.string.isRequired,
  onApprove: PropTypes.func.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: "auto"
  }
});

class DashboardWithdrawTable extends React.Component {
  state = {
    order: "desc",
    orderBy: "ts",
    selected: [],
    approved: false,
    page: 0,
    rowsPerPage: 10
  };
  componentDidMount = () => {
    EventEmitter.subscribe(EventEmitter.RESET_WITHDRAW_EVENTS, () => {
      this.setState({
        order: "desc",
        orderBy: "ts",
        selected: [],
        approved: false,
        page: 0,
        rowsPerPage: 10
      });
    });
  };

  processTx = tx => {
    return `${String(tx).substr(0, 5)}...${String(tx).substr(
      tx.length - 5,
      tx.length
    )}`;
  };
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";
    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }
    this.setState({ order, orderBy });
  };

  handleSelectAllClick = () => {
    const filterData = this._data.filter(
      n => n.status === Config.CONST_PENDING
    );
    if (filterData.length !== this.state.selected.length) {
      this.setState({
        selected: filterData.map(n => n.transaction_wthw_id)
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  onApprove = resp => {
    if (Array.isArray(resp.data) && resp.data.length > 0) {
      for (const item of this._data) {
        for (const ids of resp.data) {
          if (ids === item.transaction_wthw_id) {
            item.statusName = Config.CONST_CHULIZHONG;
            item.status = Config.CONST_APPROVED;
          }
        }
      }
    }
    this.setState({ selected: [], approved: true });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  _data;

  render() {
    const { classes } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    if (!this.state.approved) {
      this._data = this.props.list;
    }
    const emptyRows =
      rowsPerPage -
      Math.min(rowsPerPage, this._data.length - page * rowsPerPage);
    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          selected={selected}
          list={this._data}
          coin={this.props.coin}
          onApprove={this.onApprove}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={this._data.length}
              oper={this.props.oper}
            />
            <TableBody>
              {stableSort(this._data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.transaction_wthw_id);
                  return (
                    <TableRow
                      hover
                      onClick={event => {
                        if (n.status === Config.CONST_PENDING) {
                          this.handleClick(event, n.transaction_wthw_id);
                        }
                      }}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.transaction_wthw_id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isSelected}
                          disabled={n.status !== Config.CONST_PENDING}
                        />
                      </TableCell>
                      <TableCell align="right" style={{ fontSize: 16 }}>
                        {n.statusName}
                      </TableCell>
                      <TableCell
                        component="th"
                        scope="row"
                        padding="none"
                        style={{ fontSize: 16 }}
                      >
                        {n.txid ? this.processTx(n.txid) : ""}
                      </TableCell>
                      <TableCell align="right" style={{ fontSize: 16 }}>
                        {n.userid}
                      </TableCell>
                      <TableCell align="right" style={{ fontSize: 16 }}>
                        {n.value}
                      </TableCell>
                      <TableCell align="right" style={{ fontSize: 16 }}>
                        {n.to}
                      </TableCell>
                      <TableCell align="right" style={{ fontSize: 16 }}>
                        {n.ts}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={this._data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "上一页"
          }}
          nextIconButtonProps={{
            "aria-label": "下一页"
          }}
          labelRowsPerPage={`每页展示行数`}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

DashboardWithdrawTable.propTypes = {
  classes: PropTypes.object.isRequired,
  list: PropTypes.array.isRequired,
  oper: PropTypes.string.isRequired,
  coin: PropTypes.string.isRequired,
  withdrawTabs: PropTypes.array.isRequired
};

export default withStyles(styles)(DashboardWithdrawTable);
