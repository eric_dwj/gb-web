import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import withdrawServ from "../../api/service/withdraw";
import Search from "../Report/Search";
import DashboardWithdrawTable from "./DashboardWithdrawTable";
import { EventEmitter } from "../../api/util/events";
function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper
  }
});

class DashboardWithdrawTabs extends React.Component {
  state = {
    list: [],
    coins: [],
    title: "",
    loading: false,
    withdrawTabs: [],
    currentCoin: null,
    oper: null
  };

  componentDidMount = () => {
    if (!window.traderConfig || !window.traderConfig.coins) {
      withdrawServ.traderConfig().then(() => {
        this.setState({
          coins: window.traderConfig.coins,
          withdrawTabs: window.traderConfig.withdrawTabs
        });
      });
    } else {
      this.setState({
        coins: window.traderConfig.coins,
        withdrawTabs: window.traderConfig.withdrawTabs
      });
    }
  };

  updateSearchData = (list, currentCoin, oper) => {
    EventEmitter.dispatch(EventEmitter.RESET_WITHDRAW_EVENTS, null);
    let status = "";
    for (const tab of this.state.withdrawTabs) {
      if (tab.oper === oper) {
        status = tab.name;
        break;
      }
    }

    for (const item of list) {
      item.statusName = status;
    }
    this.setState({ list, currentCoin, oper, loading: false });
  };

  updateStatus = loading => {
    this.setState({ loading });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.state.coins.length > 0 && this.state.withdrawTabs.length > 0 && (
          <Search
            title={this.state.title}
            updateSearchData={this.updateSearchData}
            updateStatus={this.updateStatus}
            loading={this.state.loading}
            coins={this.state.coins}
            withdrawTabs={this.state.withdrawTabs}
          />
        )}
        {this.state.oper && (
          <DashboardWithdrawTable
            oper={this.state.oper}
            list={this.state.list}
            coin={this.state.currentCoin}
            withdrawTabs={this.state.withdrawTabs}
          />
        )}
      </div>
    );
  }
}

DashboardWithdrawTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(DashboardWithdrawTabs);
