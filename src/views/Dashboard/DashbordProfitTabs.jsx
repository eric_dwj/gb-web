import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import DashboardProfitTable from "./DashboardProfitTable";
import assetServ from "../../api/service/asset";
import Config from "../../config";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper
  }
});

class DashbordProfitTabs extends React.Component {
  state = {
    value: 0,
    data: [],
    currentBusiness: "",
    settledBalance: "",
    unsettledBalance: ""
  };

  componentDidMount = () => {
    this.handleChangeIndex(0, Object.values(window.conf)[0].business);
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  handleChangeIndex = (index, business) => {
    assetServ.listProfit(business).then(resp => {
      if (resp.data && resp.data.settles) {
        this.setState(
          {
            data: resp.data.settles,
            value: index,
            currentBusiness: business,
            settledBalance: resp.data.settledBalance,
            unsettledBalance: resp.data.unsettledBalance
          },
          () => {
            this.forceUpdate();
          }
        );
      }
    });
  };
  render() {
    const { classes, theme } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            variant="scrollable"
            scrollButtons="auto"
          >
            {window.conf &&
              Object.values(window.conf).map((item, i) => (
                <Tab
                  key={i}
                  label={item.name}
                  style={{ fontSize: 20, color: "white" }}
                  onClick={() => {
                    this.handleChangeIndex(i, item.business);
                  }}
                />
              ))}
          </Tabs>
        </AppBar>
        <br />
        <Typography color="primary" variant="h6">
          {`${Config.CONST_YJSYE}：${this.state.settledBalance}`}
        </Typography>
        <Typography color="primary" variant="h6">
          {`${Config.CONST_WJSJE}：${this.state.unsettledBalance}`}
        </Typography>
        <SwipeableViews index={this.state.value}>
          {window.conf &&
            Object.values(window.conf).map((item, i) => (
              <TabContainer dir={theme.direction} key={i}>
                <DashboardProfitTable
                  name={item.name}
                  business={item.business}
                  data={this.state.data}
                  currentBusiness={this.state.currentBusiness}
                />
              </TabContainer>
            ))}
        </SwipeableViews>
      </div>
    );
  }
}

DashbordProfitTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(DashbordProfitTabs);
