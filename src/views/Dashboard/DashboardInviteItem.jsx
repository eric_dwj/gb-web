import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import inviteServ from "../../api/service/invite";
import Config from "../../config";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: 10
  },
  card: {
    maxWidth: 300,
    margin: 45
  },
  font: {
    fontSize: 20,
    color: "black"
  }
});
class DashboardInviteItem extends React.Component {
  state = {
    open: false,
    setOpen: true
  };

  handleClick = () => {
    inviteServ
      .QRCode(`${window.mobile_server}?code=${this.props.code}`)
      .then(qrcode => {
        this.setState({ open: !this.state.open, qrcode });
      });
  };
  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <Card>
          <CardContent>
            <ListItem button onClick={this.handleClick}>
              <Grid container spacing={24}>
                <Grid item xs={1}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                </Grid>
                <Grid item xs={4}>
                  <ListItemText
                    primary={`${Config.CONST_YAOQIMA}:${String(
                      this.props.code
                    ).toUpperCase()}`}
                  />
                </Grid>
                <Grid item xs={6}>
                  <ListItemText
                    primary={`${Config.CONST_YAOQIMA}${
                      Config.CONST_RIQI
                    }:${inviteServ.covertTs2YYYYMMSSHHMMSS(this.props.ts)}`}
                  />
                </Grid>
              </Grid>
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Grid container spacing={24}>
                <Grid item xs={3} />
                <Grid item xs={6}>
                  {this.state.qrcode && (
                    <div className={classes.card}>
                      <Card>
                        <CardMedia
                          component="img"
                          className={classes.media}
                          image={this.state.qrcode}
                          title="qrcode"
                        />
                      </Card>
                      <br />
                      <Typography gutterBottom variant="h6" component="h6">
                        {Config.CONST_YAOQIMA}:{this.props.code}
                      </Typography>
                    </div>
                  )}
                </Grid>
              </Grid>
            </Collapse>
          </CardContent>
        </Card>
      </div>
    );
  }
}

DashboardInviteItem.propTypes = {
  classes: PropTypes.object.isRequired,
  ts: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  code: PropTypes.string.isRequired
};
export default withStyles(styles)(DashboardInviteItem);
