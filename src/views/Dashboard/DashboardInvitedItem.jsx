import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import EmailIron from "@material-ui/icons/Email";
import PhoneIron from "@material-ui/icons/Phone";
import ContactsIron from "@material-ui/icons/Contacts";
import PermIdentityIron from "@material-ui/icons/PermIdentity";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import inviteServ from "../../api/service/invite";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: 10
  },
  card: {
    maxWidth: 300,
    margin: 45
  }
});
class DashboardInvitedItem extends React.Component {
  state = {
    open: false,
    setOpen: true
  };

  handleClick = () => {
    inviteServ.QRCode(this.props.code).then(qrcode => {
      this.setState({ open: !this.state.open, qrcode });
    });
  };
  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <Card>
          <CardContent>
            <ListItem button onClick={this.handleClick}>
              <Grid container spacing={24}>
                <Grid item xs={1}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                </Grid>
                <Grid item xs={4}>
                  <ListItemText
                    primary={`用户ID:${this.props.userid}`}
                    titleStyle={{ fontSize: 20 }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <ListItemText
                    primary={`用户注册日期:${inviteServ.covertTs2YYYYMMSSHHMMSS(
                      this.props.ts
                    )}`}
                    titleStyle={{ fontSize: 20 }}
                  />
                </Grid>
              </Grid>
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Grid container spacing={24}>
                <Grid item xs={2} />
                <Grid item xs={2}>
                  <br />
                  <Typography gutterBottom variant="h6" component="h6">
                    {`邀请码:`}
                  </Typography>
                  <br />
                  <Typography gutterBottom variant="h6" component="h6">
                    <PermIdentityIron />
                    {`用户真实姓名:`}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    <ContactsIron />
                    {`用户QQ:`}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    <PhoneIron />
                    {`用户电话:`}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    <EmailIron />
                    {`用户电子邮件:`}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <br />
                  <Typography gutterBottom variant="h6" component="h6">
                    {this.props.code}
                  </Typography>
                  <br />

                  <Typography gutterBottom variant="h6" component="h6">
                    {this.props.name}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    {this.props.qq}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    {this.props.phone}
                  </Typography>
                  <Typography gutterBottom variant="h6" component="h6">
                    {this.props.email}
                  </Typography>
                </Grid>
              </Grid>
            </Collapse>
          </CardContent>
        </Card>
      </div>
    );
  }
}

DashboardInvitedItem.propTypes = {
  classes: PropTypes.object.isRequired,
  ts: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  code: PropTypes.string.isRequired,
  userid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  qq: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired
};
export default withStyles(styles)(DashboardInvitedItem);
