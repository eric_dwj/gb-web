import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import DashboardInviteItem from "./DashboardInviteItem.jsx";
import inviteServ from "../../api/service/invite";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Config from "../../config";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  button: {
    marginLeft: "90%"
  },
  font: {
    fontSize: 20,
    color: "black"
  }
});
class DashboardInviteList extends React.Component {
  state = {
    commissions: [],
    hasPendingCommissions: false
  };

  listPending = () => {
    const respPromise = inviteServ.listPending();
    if (respPromise) {
      respPromise.then(resp => {
        if (resp.data && resp.data.result && resp.data.result.length > 0) {
          this.setState({
            commissions: resp.data.result,
            hasPendingCommissions: true
          });
        }
      });
    }
  };
  handleGenerateCode = () => {
    inviteServ.generateCode().then(resp => {
      if (resp.data && resp.data.state === "success") {
        this.listPending();
      } else if (resp.data && resp.data.status === 10) {
        alert(`只能产生10邀请码,邀请码被使用之后,可以产生新的邀请码`);
      }
    });
  };
  componentDidMount = () => {
    this.listPending();
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={this.handleGenerateCode}
        >
          <Typography variant="h6" style={{ color: "#fff" }} align="center">
            {Config.CONST_SHENGCHENGYAOQINGMA}
          </Typography>
        </Button>
        <List
          component="nav"
          subheader={
            <ListSubheader component="div" className={classes.font}>
              {Config.CONST_WODEYAOQIMA}
            </ListSubheader>
          }
        >
          {this.state.hasPendingCommissions &&
            this.state.commissions.map((value, index) => {
              return (
                <DashboardInviteItem
                  key={index}
                  ts={value.ts}
                  role={value.role}
                  code={value.gb_commission_id}
                />
              );
            })}
        </List>
      </div>
    );
  }
}

DashboardInviteList.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(DashboardInviteList);
