import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import DashboardInvitedItem from "./DashboardInvitedItem.jsx";
import inviteServ from "../../api/service/invite";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  button: {
    marginLeft: "90%"
  }
});
class DashboardInvitedList extends React.Component {
  state = {
    commissions: [],
    hasPendingCommissions: false
  };

  listInvited = () => {
    inviteServ.listInvited().then(resp => {
      if (resp.data && resp.data.result && resp.data.result.length > 0) {
        this.setState({
          commissions: resp.data.result,
          hasPendingCommissions: true
        });
      }
    });
  };

  componentDidMount = () => {
    this.listInvited();
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <List>
          {this.state.hasPendingCommissions &&
            this.state.commissions.map((value, index) => {
              return (
                <DashboardInvitedItem
                  key={index}
                  ts={value.ts}
                  role={value.role}
                  code={value.gb_commission_id}
                  userid={value.userid}
                  name={value.name}
                  qq={value.qq}
                  phone={value.phone}
                  email={value.email}
                />
              );
            })}
        </List>
      </div>
    );
  }
}

DashboardInvitedList.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(DashboardInvitedList);
