import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import Config from "../../config";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesK33T extends React.Component {
  _numbers = [
    {
      name: "1,1,1",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "1,1,1"
    },
    {
      name: "2,2,2",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "2,2,2"
    },
    {
      name: "3,3,3",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "3,3,3"
    },
    {
      name: "4,4,4",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "4,4,4"
    },
    {
      name: "5,5,5",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "5,5,5"
    },
    {
      name: "6,6,6",
      times: this.props.conf.settle_k3_st_single_times,
      settle: window.settlement.SETTLE_K3_ST_SINGLE,
      number: "6,6,6"
    }
  ];
  _sttx = {
    name: Config.CONST_STTX,
    times: this.props.conf.settle_k3_st_all_times,
    settle: window.settlement.SETTLE_K3_ST_ALL,
    number: Config.CONST_STTX
  };
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_K33LOr3T}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
            <TableRow>
              <TableCell>
                <GamesNoteUnit
                  type={this._sttx.name}
                  times={this._sttx.times ? `${this._sttx.times}` : "NAN"}
                  settlement={this._sttx.settle}
                  handlePreorder={this.props.handlePreorder}
                  number={this._sttx.number}
                  business={this.props.business}
                  reset={this.props.reset}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesK33T.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesK33T);
