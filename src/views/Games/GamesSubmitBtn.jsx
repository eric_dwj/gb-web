import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import note from "../../api/service/note";
import Typography from "@material-ui/core/Typography";
import GamesOrderConfirmModal from "./GamesOrderConfirmModal";
import config from "../../config";
const styles = () => ({
  root: {
    paddingTop: 28
  },
  button: {
    fontSize: 20
  }
});

class GamesSubmitBtn extends React.Component {
  _waiting = true;
  state = {
    open: false,
    orders: null,
    total: "",
    alert: false
  };

  _resetOrder = () => {
    note.resetOrder();
    this.props.handleResetOrder(false);
    this.setState({ open: false });
  };

  UNSAFE_componentWillReceiveProps = () => {
    if (this._waiting) {
      this._waiting = false;
      this.setState({ open: false }, () => {
        this._waiting = true;
      });
    }
  };

  _handleCloseModal = cb => {
    this.setState({ open: false },()=>{
      cb();
    });
  }

  _handleConfirmClick = () => {
    if (
      window.user &&
      window.user.user &&
      window.user.user.role !== config.ROLE_FRESHMAN
    ) {
      this.setState({ alert: true });
    } else {
      this.setState(
        { open: true, orders: note.getOrder(), total: note.orderTotal() },
        () => {
          this.forceUpdate();
        }
      );
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid className={classes.root} container spacing={24}>
        {this.state.orders && (
          <GamesOrderConfirmModal
            handleCloseModal={this._handleCloseModal}
            open={this.state.open}
            orders={this.state.orders}
            total={this.state.total}
            isSealed={this.props.isSealed}
            handleResetOrder={this.props.handleResetOrder}
            business={this.props.business}
          />
        )}
        <Grid item xs={5} />
        <Grid item xs={1}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this._handleConfirmClick}
            disabled={this.props.isSealed}
          >
            确定
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this._resetOrder}
          >
            重置
          </Button>
        </Grid>
        <Grid item xs={5} />
        <Grid item xs={6}>
          {this.state.alert && (
            <Typography
              variant="h6"
              color="secondary"
            >{`代理用户不能下注`}</Typography>
          )}
        </Grid>
      </Grid>
    );
  }
}

GamesSubmitBtn.propTypes = {
  classes: PropTypes.object.isRequired,
  handleResetOrder: PropTypes.func.isRequired,
  business: PropTypes.string.isRequired,
  isSealed: PropTypes.bool.isRequired
};

export default withStyles(styles)(GamesSubmitBtn);
