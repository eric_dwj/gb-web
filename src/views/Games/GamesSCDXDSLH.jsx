import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config.js";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesSCDXDSLH extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BIG,
      times: this.props.conf.settle_sc_single_big_times,
      settle: window.settlement.SETTLE_SC_SINGLE_BIG,
      number: Config.CONST_BIG
    },
    {
      name: Config.CONST_SMALL,
      times: this.props.conf.settle_sc_single_small_times,
      settle: window.settlement.SETTLE_SC_SINGLE_SMALL,
      number: Config.CONST_SMALL
    },
    {
      name: Config.CONST_ODD,
      times: this.props.conf.settle_sc_single_odd_times,
      settle: window.settlement.SETTLE_SC_SINGLE_ODD,
      number: Config.CONST_ODD
    },
    {
      name: Config.CONST_EVEN,
      times: this.props.conf.settle_sc_single_even_times,
      settle: window.settlement.SETTLE_SC_SINGLE_EVEN,
      number: Config.CONST_EVEN
    },
    {
      name: Config.CONST_LONG,
      times: this.props.conf.settle_sc_long_times,
      settle: window.settlement.SETTLE_SC_LONG,
      number: Config.CONST_LONG
    },
    {
      name: Config.CONST_HU,
      times: this.props.conf.settle_sc_hu_times,
      settle: window.settlement.SETTLE_SC_HU,
      number: Config.CONST_HU
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesSCDXDSLH.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesSCDXDSLH);
