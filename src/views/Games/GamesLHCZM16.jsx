import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCZM16 extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BIG,
      times: this.props.conf.settle_lhc_zm_big_times,
      settle: window.settlement.SETTLE_LHC_ZM_BIG,
      number: Config.CONST_BIG
    },
    {
      name: Config.CONST_SMALL,
      times: this.props.conf.settle_lhc_zm_small_times,
      settle: window.settlement.SETTLE_LHC_ZM_SMALL,
      number: Config.CONST_SMALL
    },
    {
      name: Config.CONST_ODD,
      times: this.props.conf.settle_lhc_zm_odd_times,
      settle: window.settlement.SETTLE_LHC_ZM_ODD,
      number: Config.CONST_ODD
    },
    {
      name: Config.CONST_EVEN,
      times: this.props.conf.settle_lhc_zm_even_times,
      settle: window.settlement.SETTLE_LHC_ZM_EVEN,
      number: Config.CONST_EVEN
    },
    {
      name: Config.CONST_HESHUDA,
      times: this.props.conf.settle_lhc_zm_he_big_times,
      settle: window.settlement.SETTLE_LHC_ZM_HE_BIG,
      number: Config.CONST_HESHUDA
    },
    {
      name: Config.CONST_HESHUXIAO,
      times: this.props.conf.settle_lhc_zm_he_small_times,
      settle: window.settlement.SETTLE_LHC_ZM_HE_SMALL,
      number: Config.CONST_HESHUXIAO
    },
    {
      name: Config.CONST_HESHUDAN,
      times: this.props.conf.settle_lhc_zm_he_odd_times,
      settle: window.settlement.SETTLE_LHC_ZM_HE_ODD,
      number: Config.CONST_HESHUDAN
    },
    {
      name: Config.CONST_HESHUSHUANG,
      times: this.props.conf.settle_lhc_zm_he_even_times,
      settle: window.settlement.SETTLE_LHC_ZM_HE_EVEN,
      number: Config.CONST_HESHUSHUANG
    },
    {
      name: Config.CONST_WEISHUDA,
      times: this.props.conf.settle_lhc_zm_wei_big_times,
      settle: window.settlement.SETTLE_LHC_ZM_WEI_BIG,
      number: Config.CONST_WEISHUDA
    },
    {
      name: Config.CONST_WEISHUXIAO,
      times: this.props.conf.settle_lhc_zm_wei_small_times,
      settle: window.settlement.SETTLE_LHC_ZM_WEI_SMALL,
      number: Config.CONST_WEISHUXIAO
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCZM16.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCZM16);
