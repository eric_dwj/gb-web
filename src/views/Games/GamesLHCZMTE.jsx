import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/styles/withStyles";
import GamesLHCZMTENUM from "./GamesLHCZMTENUM";
import GamesLHCZMTELM from "./GamesLHCZMTELM";
import GamesMenu from "./GamesMenu";

const styles = () => {
  return {
    root: {
      width: "100%"
    }
  };
};
class GamesLHCZMTE extends React.Component {
  _waiting = true;
  state = {
    start: 0,
    end: 1,
    title: null
  };
  onClickMenu = value => {
    for (let index = 0; index < this.props.menus.length; index++) {
      const menu = this.props.menus[index];
      if (menu === value) {
        this.setState(
          { start: index, end: index + 1, title: value, reset: true },
          () => {
            this.setState({ reset: false });
          }
        );
      }
    }
  };
  componentDidMount = () => {
    this.setState({
      start: 0,
      end: 1,
      title: this.props.menus[0],
      reset: this.props.reset
    });
  };

  UNSAFE_componentWillReceiveProps = () => {
    if (this.props.reset && this._waiting) {
      this._waiting = false;
      this.setState({ reset: this.props.reset }, () => {
        this._waiting = true;
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <GamesMenu menus={this.props.menus} onClickMenu={this.onClickMenu} />
        {this.state.title && this.state.end && (
          <GamesLHCZMTENUM
            conf={this.props.conf}
            title={this.state.title}
            handlePreorder={this.props.handlePreorder}
            business={this.props.business}
            reset={this.state.reset}
            isSealed={this.props.isSealed}
            start={`${this.state.start}`}
            end={`${this.state.end}`}
          />
        )}
        {this.state.title && this.state.end && (
          <GamesLHCZMTELM
            conf={this.props.conf}
            title={this.state.title}
            handlePreorder={this.props.handlePreorder}
            business={this.props.business}
            reset={this.state.reset}
            isSealed={this.props.isSealed}
            start={`${this.state.start}`}
            end={`${this.state.end}`}
          />
        )}
      </Paper>
    );
  }
}
GamesLHCZMTE.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCZMTE);
