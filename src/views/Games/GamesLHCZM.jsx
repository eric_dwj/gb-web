import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
import Tool from "../../api/util/common";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCZM extends React.Component {
  _numbers = [];

  constructor(props) {
    super(props);
    let rows;
    for (let index = 1; index <= 49; index++) {
      if (index % 4 === 1) {
        rows = [];
      }
      const noteUnit = {};
      noteUnit.times = this.props.conf.settle_lhc_zm_in_numbers_times;
      noteUnit.settle = window.settlement.SETTLE_LHC_ZM_IN_NUMBERS;
      noteUnit.number = Tool.get00Number(index);
      noteUnit.name = Tool.get00Number(index);
      rows.push(noteUnit);
      if (index % 4 === 0) {
        this._numbers.push(rows);
      } else if (index === 49) {
        this._numbers.push(rows);
      }
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((row, index) => {
              return (
                <TableRow key={index}>
                  {row &&
                    row.map((value, key) => {
                      return (
                        <TableCell key={key}>
                          <GamesNoteUnit
                            type={value.name}
                            times={value.times ? `${value.times}` : "NAN"}
                            settlement={value.settle}
                            handlePreorder={this.props.handlePreorder}
                            number={value.number}
                            business={this.props.business}
                            reset={this.props.reset}
                            start={this.props.start}
                            end={this.props.end}
                            group={this.props.title}
                            isSealed={this.props.isSealed}
                            logo={Config.CONST_LHC}
                          />
                        </TableCell>
                      );
                    })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCZM.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCZM);
