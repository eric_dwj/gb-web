/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import Grid from "@material-ui/core/Grid";
import GamesKL8LMZH from "./GamesKL8LMZH.jsx";
import GamesKL8QHDS from "./GamesKL8QHDS.jsx";
import GamesKL8WX from "./GamesKL8WX.jsx";
import GamesKL8ZM from "./GamesKL8ZM.jsx";
import Config from "../../config.js";
const GamesKL8 = () => {};

GamesKL8.processKL8 = (props, state, handlePreorder, views, indents, i) => {
  if (views[i] === "KL8_Summary") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesKL8LMZH
          key={`KL8_Summary_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_ZH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`20`}
        />
      </Grid>
    );
  } else if (views[i] === "KL8_Front_End_Sum") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesKL8QHDS
          key={`KL8_Front_End_Sum_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_QHH_DSH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`20`}
        />
      </Grid>
    );
  } else if (views[i] === "KL8_WUXING") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesKL8WX
          key={`KL8_WUXING_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_WUXING}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`20`}
        />
      </Grid>
    );
  } else if (views[i] === "KL8_ZHENGMA") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesKL8ZM
          key={`KL8_ZHENGMA_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_ZHENGMA}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`20`}
        />
      </Grid>
    );
  }
};
export default GamesKL8;
