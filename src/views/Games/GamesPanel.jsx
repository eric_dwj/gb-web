/* eslint-disable prettier/prettier */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import GamesBar from "./GamesBar.jsx";
import GamesSSC from './GamesSSC.jsx';
import GamesSC from './GamesSC.jsx';
import GamesK3 from './GamesK3.jsx';
import GamesKL8 from './GamesKL8.jsx';
import GamesLHC from './GamesLHC';
import Grid from "@material-ui/core/Grid";
import note from "../../api/service/note";
import GamesOrderBoard from "./GamesOrderBoard.jsx";
import GamesSubmitBtn from "./GamesSubmitBtn";

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  tab: {
    fontSize: 20
  }
});

class GamesPanel extends React.Component {
  state = {
    value: 0,
    reset: false,
    isSealed: false,
    notifyNewOrder: true
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleFixedClick = () => {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  };

  handlePreorder = (type, business, size, number, times, settlement, start, end) => {
    note.addOrder(type, business, this.props.name, note.getNextRound(business), size, number, times, settlement, start, end);
  };

  handleSealOrder = isSealed => {
    if (!isSealed) {
      this.handleResetOrder(false);
    }
    this.setState({ isSealed }, () => {
      this.forceUpdate();
    });
  };

  handleResetOrder = isPutOrderResp => {
    if (isPutOrderResp) {
      const notifyNewOrder = !this.state.notifyNewOrder;
      this.setState({ reset: true, notifyNewOrder }, () => {
        this.forceUpdate(() => {
          this.setState({ reset: false });
        });
      });
    } else {
      this.setState({ reset: true }, () => {
        this.forceUpdate(() => {
          this.setState({ reset: false });
        });
      });
    }
  };

  _handleCatagoryViews = (views, menus) => {
    const indents = [];
    if (Array.isArray(views)) {
      for (let i = 0; i < views.length; i++) {
        GamesSSC.processSSC(this.props, this.state, this.handlePreorder, views, indents, i);
        GamesSC.processSC(this.props, this.state, this.handlePreorder, views, indents, i);
        GamesK3.processK3(this.props, this.state, this.handlePreorder, views, indents, i);
        GamesKL8.processKL8(this.props, this.state, this.handlePreorder, views, indents, i);
        GamesLHC.processLHC(this.props, this.state, this.handlePreorder, views, menus, indents, i);
      }
    }
    return indents;
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <GamesBar
            name={this.props.name}
            business={this.props.business}
            handleSealOrder={this.handleSealOrder}
          />
          <Tabs
            value={value}
            onChange={this.handleChange}
            variant="scrollable"
            scrollButtons="auto"
          >
            {this.props.catagory &&
              this.props.catagory.map((record, index) => {
                return <Tab key={index} label={record.name} style={{ fontSize: 20 }} />;
              })}
          </Tabs>
        </AppBar>
        <Grid container spacing={24}>
          <GamesSubmitBtn
            handleResetOrder={this.handleResetOrder}
            business={this.props.business}
            isSealed={this.state.isSealed}
          />
          {this.props.catagory &&
            this.props.catagory.map((record, index) => {
              if (value === index) {
                return this._handleCatagoryViews(record.views, record.menus);
              } else {
                return <div key={index} />;
              }
            })}
        </Grid>
        <GamesOrderBoard
          fixedClasses={this.state.fixedClasses}
          handleFixedClick={this.handleFixedClick}
          notifyNewOrder={this.state.notifyNewOrder}
        />
      </div>
    );
  }
}

GamesPanel.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  catagory: PropTypes.array.isRequired,
  business: PropTypes.string.isRequired
};

export default withStyles(styles)(GamesPanel);
