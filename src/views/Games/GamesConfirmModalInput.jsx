import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import TextField from "@material-ui/core/TextField";

const styles = () => {
  return {
    divider: {
      marginTop: "15px",
      borderRight: "0.02em solid"
    }
  };
};
class GamesConfirmModalInput extends React.Component {
  state = {
    backgroundColor: "white",
    noteSize: ""
  };
  handleSizeChange = e => {
    let size;
    if (e.target.value) {
      size = e.target.value;
    } else {
      size = "";
    }
    this.setState({ noteSize: size });
    this.props.handleSizeChange(size, this.props.orderkey);
  };
  componentDidMount = () => {
    this.setState({ noteSize: this.props.size });
  };

  render() {
    return (
      <TextField
        id="outlined-dense"
        label={`金额`}
        variant="outlined"
        onChange={this.handleSizeChange}
        value={this.state.noteSize}
      />
    );
  }
}

GamesConfirmModalInput.propTypes = {
  size: PropTypes.string.isRequired,
  orderkey: PropTypes.string.isRequired,
  handleSizeChange: PropTypes.func.isRequired
};
export default withStyles(styles)(GamesConfirmModalInput);
