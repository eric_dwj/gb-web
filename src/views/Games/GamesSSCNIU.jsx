import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesQZH extends React.Component {
  _numbers = [
    {
      name: Config.CONST_NIUNIU,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "0"
    },
    {
      name: Config.CONST_NIUJIU,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "9"
    },
    {
      name: Config.CONST_NIUBA,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "8"
    },
    {
      name: Config.CONST_NIUQI,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "7"
    }
  ];

  _numbers1 = [
    {
      name: Config.CONST_NIULIU,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "6"
    },
    {
      name: Config.CONST_NIUWU,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "5"
    },
    {
      name: Config.CONST_NIUSI,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "4"
    },
    {
      name: Config.CONST_NIUSAN,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "3"
    }
  ];

  _numbers2 = [
    {
      name: Config.CONST_NIUER,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "2"
    },
    {
      name: Config.CONST_NIUYI,
      times: this.props.conf.settle_ssc_niu_times,
      settle: window.settlement.SETTLE_SSC_NIU,
      number: "1"
    },
    {
      name: Config.CONST_WUNIU,
      times: this.props.conf.settle_ssc_niu_0_times,
      settle: window.settlement.SETTLE_SSC_0_NIU,
      number: "-1"
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
              <TableCell />
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesQZH.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesQZH);
