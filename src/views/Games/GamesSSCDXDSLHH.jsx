import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      //minWidth: 700
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    },
    divider: {
      borderRight: "0.1em solid #bdbdbd"
    }
  };
};
class GamesDXDSLHH extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={4} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_BIG}
                  times={
                    this.props.conf.settle_ssc_total_big_times
                      ? `${this.props.conf.settle_ssc_total_big_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_TOTAL_BIG}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_BIG}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_SMALL}
                  times={
                    this.props.conf.settle_ssc_total_small_times
                      ? `${this.props.conf.settle_ssc_total_small_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_TOTAL_SMALL}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_SMALL}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_ODD}
                  times={
                    this.props.conf.settle_ssc_total_odd_times
                      ? `${this.props.conf.settle_ssc_total_odd_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_TOTAL_ODD}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_ODD}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_EVEN}
                  times={
                    this.props.conf.settle_ssc_total_even_times
                      ? `${this.props.conf.settle_ssc_total_even_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_TOTAL_EVEN}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_EVEN}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_LONG}
                  times={
                    this.props.conf.settle_ssc_long_times
                      ? `${this.props.conf.settle_ssc_long_times}`
                      : "0"
                  }
                  settlement={window.settlement.SETTLE_SSC_LONG}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_LONG}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_HU}
                  times={
                    this.props.conf.settle_ssc_hu_times
                      ? `${this.props.conf.settle_ssc_hu_times}`
                      : "0"
                  }
                  settlement={window.settlement.SETTLE_SSC_HU}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_HU}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider}>
                <GamesNoteUnit
                  type={Config.CONST_HE}
                  times={
                    this.props.conf.settle_ssc_he_times
                      ? `${this.props.conf.settle_ssc_he_times}`
                      : "0"
                  }
                  settlement={window.settlement.SETTLE_SSC_HE}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDSLHH${Config.CONST_HE}`}
                  reset={this.props.reset}
                  business={this.props.business}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
              <TableCell className={classes.divider} />
            </TableRow>
            <TableRow />
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesDXDSLHH.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesDXDSLHH);
