import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config.js";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesQZH extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BZ,
      times: this.props.conf.settle_ssc_same_number_times,
      settle: window.settlement.SETTLE_SSC_SAME_NUMBER
    },
    {
      name: Config.CONST_SZ,
      times: this.props.conf.settle_ssc_straight_times,
      settle: window.settlement.SETTLE_SSC_STRAIGHT
    },
    {
      name: Config.CONST_DZ,
      times: this.props.conf.settle_ssc_2_same_number_times,
      settle: window.settlement.SETTLE_SSC_2_SAME_NUMBER
    },
    {
      name: Config.CONST_BS,
      times: this.props.conf.settle_ssc_half_straight_times,
      settle: window.settlement.SETTLE_SSC_HALF_STRAIGHT
    },
    {
      name: Config.CONST_ZL,
      times: this.props.conf.settle_ssc_disorder_times,
      settle: window.settlement.SETTLE_SSC_DISORDER
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>     
              {this._numbers.map((value, index) => {
                return (
                  <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={`QZH${value.name}`}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesQZH.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesQZH);
