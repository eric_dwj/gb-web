import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCZXiao extends React.Component {
  _numbers = [
    {
      name: `2${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_2_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "2"
    },
    {
      name: `3${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_3_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "3"
    },
    {
      name: `4${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_4_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "4"
    },
    {
      name: `5${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_5_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "5"
    }
  ];

  _numbers1 = [
    {
      name: `6${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_6_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "6"
    },
    {
      name: `7${Config.CONST_XIAO}`,
      times: this.props.conf.settle_lhc_zh_xiao_7_number_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_NUMBER,
      number: "7"
    },
    {
      name: Config.CONST_ZHONGXIAODAN,
      times: this.props.conf.settle_lhc_zh_xiao_odd_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_ODD,
      number: Config.CONST_ZHONGXIAODAN
    },
    {
      name: Config.CONST_ZHONGXIAOSHUANG,
      times: this.props.conf.settle_lhc_zh_xiao_even_times,
      settle: window.settlement.SETTLE_LHC_ZH_XIAO_EVEN,
      number: Config.CONST_ZHONGXIAOSHUANG
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCZXiao.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCZXiao);
