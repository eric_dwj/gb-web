import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import Timer from "@material-ui/icons/Timer";
import Today from "@material-ui/icons/Today";
import Avatar from "@material-ui/core/Avatar";
import red from "@material-ui/core/colors/red";
import Typography from "@material-ui/core/Typography";
import note from "../../api/service/note";
import GamesImg from "./GamesImg";
import Config from "../../config";
const styles = theme => ({
  root: {
    flexGrow: 1
  },
  chip: {
    backgroundColor: theme.palette.background.paper,
    margin: 5,
    fontSize: 20
  },
  font: {
    color: "white"
  },
  fontRed: {
    color: "red"
  },
  orangeAvatar: {
    margin: 5,
    color: "#fff",
    backgroundColor: "#1976d2",
    fontSize: 20
  },
  blueDeep: {
    margin: 5,
    color: "#fff",
    backgroundColor: "#1976d2",
    fontSize: 20
  },
  blueLight: {
    margin: 5,
    color: "#fff",
    backgroundColor: "#83d0f2",
    fontSize: 20
  },
  sumInfo: {
    marginLeft: 5,
    marginTop: -22,
    width: 38,
    fontSize: 20,
    color: red[500],
    backgroundColor: "#fff"
  },
  toolbar: {
    marginTop: 20
  }
});

class GamesBar extends React.Component {
  _isMounted = true;
  _sealedFlag = false;
  _firstCheck = true;
  state = {
    msg: {
      finalTime: "",
      nextRound: "",
      sealTime: "",
      conf: {},
      latestNumber: ""
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidMount = () => {
    note.register(this.props.business, msg => {
      if (this._firstCheck && this._isMounted) {
        msg.sealTime === Config.CONST_ZERO_TIME
          ? this.props.handleSealOrder(true)
          : this.props.handleSealOrder(false);
        this._firstCheck = false;
      }
      if (this._isMounted) {
        this.setState({ msg });
        const nextRound = note.getNextRound(this.props.business);

        if (nextRound !== msg.nextRound) {
          note.setNextRound(this.props.business, msg.nextRound);
        }

        if (
          msg.sealTime === Config.CONST_ZERO_TIME &&
          this._sealedFlag === false
        ) {
          this.props.handleSealOrder(true);
        }

        if (
          msg.sealTime !== Config.CONST_ZERO_TIME &&
          this._sealedFlag === true
        ) {
          this.props.handleSealOrder(false);
        }

        if (msg.sealTime === Config.CONST_ZERO_TIME) {
          this._sealedFlag = true;
        } else {
          this._sealedFlag = false;
        }
      }
    });
  };
  _mapKL8LastRoundImg = (value, index, classes) => {
    if (parseInt(value) <= 40) {
      return (
        <Avatar key={index} className={classes.blueLight}>
          {value}
        </Avatar>
      );
    } else {
      return (
        <Avatar key={index} className={classes.blueDeep}>
          {value}
        </Avatar>
      );
    }
  };
  _mapLastRoundImg = (value, index, classes) => {
    if (String(this.props.business).indexOf(Config.CONST_K3) !== -1) {
      return <GamesImg key={index} number={value} type={Config.CONST_K3} />;
    } else if (String(this.props.business).indexOf(Config.CONST_SSC) !== -1) {
      return <GamesImg key={index} number={value} type={Config.CONST_SSC} />;
    } else if (String(this.props.business).indexOf(Config.CONST_SC) !== -1) {
      return <GamesImg key={index} number={value} type={Config.CONST_SC} />;
    } else if (String(this.props.business).indexOf(Config.CONST_LHC) !== -1) {
      return <GamesImg key={index} number={value} type={Config.CONST_LHC} />;
    } else {
      return (
        <Avatar key={index} className={classes.orangeAvatar}>
          {value}
        </Avatar>
      );
    }
  };
  render() {
    const { classes } = this.props;
    let resArr;
    if (this.state.msg && this.state.msg.latestNumber) {
      resArr = String(this.state.msg.latestNumber).split(",");
    }

    let display2Lines;
    if (Array.isArray(resArr)) {
      resArr.length > 10 ? (display2Lines = true) : (display2Lines = false);
    }

    let resArr1, resArr2;
    if (display2Lines) {
      resArr1 = resArr.slice(0, 10);
      resArr2 = resArr.slice(10, 20);
    }
    return (
      <div className={classes.root}>
        <Toolbar variant="dense" className={classes.toolbar}>
          <Grid container spacing={24}>
            <Grid item xs={7}>
              <Grid container spacing={24}>
                <Grid item xs={4}>
                  <Grid container>
                    <Typography
                      variant="h6"
                      className={classes.font}
                      align="center"
                    >
                      {`${
                        this.state.msg.latestRound
                          ? this.state.msg.latestRound
                          : "--"
                      } ${Config.CONST_KJJG}:`}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item xs={8}>
                  {!display2Lines && (
                    <Grid container>
                      {resArr &&
                        resArr.map((value, index) => {
                          return this._mapLastRoundImg(value, index, classes);
                        })}
                    </Grid>
                  )}
                  {display2Lines && (
                    <Grid container>
                      {resArr1 &&
                        resArr1.map((value, index) => {
                          return this._mapKL8LastRoundImg(
                            value,
                            index,
                            classes
                          );
                        })}
                    </Grid>
                  )}
                  {display2Lines && (
                    <Grid container>
                      {resArr2 &&
                        resArr2.map((value, index) => {
                          return this._mapKL8LastRoundImg(
                            value,
                            index,
                            classes
                          );
                        })}
                    </Grid>
                  )}
                </Grid>
                <Grid item xs={4} />
                <Grid item xs={8}>
                  <Grid container>
                    {this.state.msg &&
                      this.state.msg.lastInfo &&
                      this.state.msg.lastInfo.map((value, index) => {
                        return (
                          <Chip
                            key={index}
                            label={value}
                            color="secondary"
                            className={classes.sumInfo}
                          />
                        );
                      })}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={2}>
              {!this.state.msg.openStatus && (
                <Typography variant="h6" className={classes.font} align="left">
                  {`${Config.CONST_XIAYIQI}:
                      ${Config.CONST_WEIKAIPAN}`}
                </Typography>
              )}
              {this.state.msg.openStatus && (
                <Typography variant="h6" className={classes.font} align="left">
                  {`
                      ${Config.CONST_XIAYIQI}:
                      ${
                        this.state.msg.nextRound
                          ? this.state.msg.nextRound
                          : "--"
                      }`}
                </Typography>
              )}
            </Grid>
            <Grid item xs={3}>
              <Grid container>
                <Chip
                  icon={<Timer />}
                  label={
                    this.state.msg.sealTime === "00:00"
                      ? `${Config.CONST_FENGPAN}: ${Config.CONST_FENGPAN}`
                      : `${Config.CONST_FENGPAN}:  ${this.state.msg.sealTime}`
                  }
                  className={classes.chip}
                  color="secondary"
                  variant="outlined"
                />
              </Grid>
              <Grid container>
                <Chip
                  icon={<Today />}
                  label={`${Config.CONST_KAIJIANG}:  ${
                    this.state.msg.openStatus
                      ? this.state.msg.finalTime
                      : Config.CONST_WEIKAIPAN
                  }`}
                  className={classes.chip}
                  color="secondary"
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </div>
    );
  }
}

GamesBar.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  handleSealOrder: PropTypes.func.isRequired
};

export default withStyles(styles)(GamesBar);
