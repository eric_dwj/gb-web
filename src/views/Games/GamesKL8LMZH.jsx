import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesKL8LMZH extends React.Component {
  _numbers = [
    {
      name: Config.CONST_ZHDA,
      times: this.props.conf.settle_kl_total_big_times,
      settle: window.settlement.SETTLE_KL_TOTAL_BIG,
      number: Config.CONST_ZHDA
    },
    {
      name: Config.CONST_ZHX,
      times: this.props.conf.settle_kl_total_small_times,
      settle: window.settlement.SETTLE_KL_TOTAL_SMALL,
      number: Config.CONST_ZHX
    },
    {
      name: Config.CONST_ZHDAN,
      times: this.props.conf.settle_kl_total_odd_times,
      settle: window.settlement.SETTLE_KL_TOTAL_ODD,
      number: Config.CONST_ZHDAN
    }
  ];

  _numbers1 = [
    {
      name: Config.CONST_ZHS,
      times: this.props.conf.settle_kl_total_even_times,
      settle: window.settlement.SETTLE_KL_TOTAL_EVEN,
      number: Config.CONST_ZHS
    },
    {
      name: Config.CONST_ZHDD,
      times: this.props.conf.settle_kl_total_odd_big_times,
      settle: window.settlement.SETTLE_KL_TOTAL_ODD_BIG,
      number: Config.CONST_ZHDD
    },
    {
      name: Config.CONST_ZHDS,
      times: this.props.conf.settle_kl_total_even_big_times,
      settle: window.settlement.SETTLE_KL_TOTAL_EVEN_BIG,
      number: Config.CONST_ZHDS
    }
  ];

  _numbers2 = [
    {
      name: Config.CONST_ZHXD,
      times: this.props.conf.settle_kl_total_odd_small_times,
      settle: window.settlement.SETTLE_KL_TOTAL_ODD_SMALL,
      number: Config.CONST_ZHXD
    },
    {
      name: Config.CONST_ZHXS,
      times: this.props.conf.settle_kl_total_even_small_times,
      settle: window.settlement.SETTLE_KL_TOTAL_EVEN_SMALL,
      number: Config.CONST_ZHXS
    },
    {
      name: Config.CONST_ZH810,
      times: this.props.conf.settle_kl_810_times,
      settle: window.settlement.SETTLE_KL_810,
      number: Config.CONST_ZH810
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesKL8LMZH.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesKL8LMZH);
