/*eslint-disable*/
import React, { Component } from "react";
import classnames from "classnames";
import note from "../../api/service/note";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";

class GamesOrderBoard extends Component {
  _waiting = true;
  constructor(props) {
    super(props);
    this.state = {
      classes: "dropdown show",
      bg_checked: true,
      bgImage: this.props.bgImage,
      orders: null
    };
  }
  UNSAFE_componentWillReceiveProps = () => {
    if (this._waiting) {
      this._waiting = false;
      if (Array.isArray(note.handleLatestOrders())) {
        this.setState({ orders: note.handleLatestOrders() }, () => {
          this.forceUpdate();
          this._waiting = true;
        })
      } else {
        this._waiting = true;
      }
    }
  }
  handleClick = () => {
    this.props.handleFixedClick();
  }
  render() {
    return (
      <div className={classnames("fixed-plugin", { "rtl-fixed-plugin": this.props.rtlActive })}>
        <div id="fixedPluginClasses" className={this.props.fixedClasses}>
          <div onClick={this.handleClick}>
            <i className="fa fa-cog fa-2x" />
          </div>
          <ul className="dropdown-menu">
            <li className="header-title" style={{ backgroundColor: indigo[500] }}><Typography style={{ color: 'white' }} variant="h6" component="h6" >最新注单</Typography></li>
            { this.state.orders && this.state.orders.map((order, index) => {
              return <li key={index}>{` 注单: ${order.type}@${order.times} 下注金额:${order.note_size}`}</li>
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default GamesOrderBoard;
