import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const styles = {
  tab: {
    fontSize: 18
  }
};

class GamesMenu extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value }, () => {
      this.props.onClickMenu(this.props.menus[value]);
    });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <Tabs
        value={value}
        onChange={this.handleChange}
        variant="scrollable"
        scrollButtons="auto"
        style={{ padding: 20 }}
      >
        {this.props.menus &&
          this.props.menus.map((value, index) => {
            return <Tab key={index} className={classes.tab} label={value} />;
          })}
      </Tabs>
    );
  }
}

GamesMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  menus: PropTypes.array.isRequired
};

export default withStyles(styles)(GamesMenu);
