import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesK3NotExist extends React.Component {
  _numbers = [
    {
      name: "1",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "1"
    },
    {
      name: "2",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "2"
    },
    {
      name: "3",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "3"
    },
    {
      name: "4",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "4"
    },
    {
      name: "5",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "5"
    },
    {
      name: "6",
      times: this.props.conf.settle_k3_number_no_exist_times,
      settle: window.settlement.SETTLE_K3_NUMBER_NO_EXIST,
      number: "6"
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_K3_NOT_EXIST}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesK3NotExist.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesK3NotExist);
