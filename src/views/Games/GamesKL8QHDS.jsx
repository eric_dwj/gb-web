import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesKL8QHDS extends React.Component {
  _numbers = [
    {
      name: Config.CONST_ZHQIANDUO,
      times: this.props.conf.settle_kl_total_qd_times,
      settle: window.settlement.SETTLE_KL_TOTAL_QD,
      number: Config.CONST_ZHQIANDUO
    },
    {
      name: Config.CONST_ZHHOUDUO,
      times: this.props.conf.settle_kl_total_hd_times,
      settle: window.settlement.SETTLE_KL_TOTAL_HD,
      number: Config.CONST_ZHHOUDUO
    },
    {
      name: Config.CONST_ZHQIANHOUHE,
      times: this.props.conf.settle_kl_total_qhh_times,
      settle: window.settlement.SETTLE_KL_TOTAL_QHH,
      number: Config.CONST_ZHQIANHOUHE
    }
  ];

  _numbers1 = [
    {
      name: Config.CONST_ZHDANDUO,
      times: this.props.conf.settle_kl_total_dd_times,
      settle: window.settlement.SETTLE_KL_TOTAL_DD,
      number: Config.CONST_ZHDANDUO
    },
    {
      name: Config.CONST_ZHSHUANDUO,
      times: this.props.conf.settle_kl_total_sd_times,
      settle: window.settlement.SETTLE_KL_TOTAL_SD,
      number: Config.CONST_ZHSHUANDUO
    },
    {
      name: Config.CONST_ZHDANSHUANHE,
      times: this.props.conf.settle_kl_total_dsh_times,
      settle: window.settlement.SETTLE_KL_TOTAL_DSH,
      number: Config.CONST_ZHDANSHUANHE
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesKL8QHDS.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesKL8QHDS);
