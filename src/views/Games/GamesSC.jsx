/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import GamesSCDXDSLH from "./GamesSCDXDSLH.jsx";
import GamesSCDXDS from "./GamesSCDXDS.jsx";
import GamesSCRank from "./GamesSCRank.jsx";
import GamesSCGYHE from "./GamesSCGYHE.jsx";
import Grid from "@material-ui/core/Grid";
import Config from "../../config.js";
const GamesSC = () => {};

GamesSC.processSC = (props, state, handlePreorder, views, indents, i) => {
  if (views[i] === "SC_LM_1") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDSLH
          key={`SC_LM_1_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_CHANPION}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_2") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDSLH
          key={`SC_LM_2_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SECOND_PALACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_3") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDSLH
          key={`SC_LM_3_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_THIRD_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_4") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDSLH
          key={`SC_LM_4_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FOURTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_5") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDSLH
          key={`SC_LM_5_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIFTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_6") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDS
          key={`SC_LM_6_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SIXTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_7") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDS
          key={`SC_LM_7_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SEVENTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_8") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDS
          key={`SC_LM_8_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_HIGHTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_9") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDS
          key={`SC_LM_9_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_NINETH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_LM_10") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCDXDS
          key={`SC_LM_10_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_TENTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_GY_HE") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesSCGYHE
          key={`SC_GY_HE_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_GYHE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`10`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_1_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_1_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_CHANPION}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`1`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_2_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_2_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SECOND_PALACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`1`}
          end={`2`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_3_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_3_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_THIRD_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`2`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_4_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_4_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FOURTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`3`}
          end={`4`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_5_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_5_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIFTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`4`}
          end={`5`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_6_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_6_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SIXTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`5`}
          end={`6`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_7_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_7_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SEVENTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`6`}
          end={`7`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_8_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_8_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_HIGHTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`7`}
          end={`8`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_9_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_9_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_NINETH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`8`}
          end={`9`}
        />
      </Grid>
    );
  } else if (views[i] === "SC_10_number") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesSCRank
          key={`SC_10_number_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_TENTH_PLACE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`9`}
          end={`10`}
        />
      </Grid>
    );
  }
};
export default GamesSC;
