import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCLMDXDS extends React.Component {
  _numbers = [
    {
      name: Config.CONST_TEDA,
      times: this.props.conf.settle_lhc_te_big_times,
      settle: window.settlement.SETTLE_LHC_TE_BIG,
      number: Config.CONST_TEDA
    },
    {
      name: Config.CONST_TEXIAO,
      times: this.props.conf.settle_lhc_te_small_times,
      settle: window.settlement.SETTLE_LHC_TE_SMALL,
      number: Config.CONST_TEXIAO
    },
    {
      name: Config.CONST_TEDAN,
      times: this.props.conf.settle_lhc_te_odd_times,
      settle: window.settlement.SETTLE_LHC_TE_ODD,
      number: Config.CONST_TEDAN
    },
    {
      name: Config.CONST_TESHUANG,
      times: this.props.conf.settle_lhc_te_even_times,
      settle: window.settlement.SETTLE_LHC_TE_EVEN,
      number: Config.CONST_TESHUANG
    },
    {
      name: Config.CONST_TEDADAN,
      times: this.props.conf.settle_lhc_te_dd_times,
      settle: window.settlement.SETTLE_LHC_TE_DD,
      number: Config.CONST_TEDADAN
    },
    {
      name: Config.CONST_TEDASHUANG,
      times: this.props.conf.settle_lhc_te_ds_times,
      settle: window.settlement.SETTLE_LHC_TE_DS,
      number: Config.CONST_TEDASHUANG
    },
    {
      name: Config.CONST_TEXIAODAN,
      times: this.props.conf.settle_lhc_te_xd_times,
      settle: window.settlement.SETTLE_LHC_TE_XD,
      number: Config.CONST_TEXIAODAN
    },
    {
      name: Config.CONST_TEXIAOSHUANG,
      times: this.props.conf.settle_lhc_te_xs_times,
      settle: window.settlement.SETTLE_LHC_TE_XS,
      number: Config.CONST_TEXIAOSHUANG
    },
    {
      name: Config.CONST_TEWEIDA,
      times: this.props.conf.settle_lhc_te_wei_big_times,
      settle: window.settlement.SETTLE_LHC_TE_WEI_BIG,
      number: Config.CONST_TEWEIDA
    },
    {
      name: Config.CONST_TEWEIXIAO,
      times: this.props.conf.settle_lhc_te_wei_small_times,
      settle: window.settlement.SETTLE_LHC_TE_WEI_SMALL,
      number: Config.CONST_TEWEIXIAO
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCLMDXDS.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCLMDXDS);
