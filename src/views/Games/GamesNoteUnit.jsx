import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import indigo from "@material-ui/core/colors/indigo";
import GamesImg from "./GamesImg.jsx";
import Config from "../../config";
import Grid from "@material-ui/core/Grid";
const styles = () => {
  return {
    divider: {
      marginTop: "15px",
      borderRight: "0.02em solid",
      borderLeft: "0.02em solid",
      color: indigo[400]
    },
    font: {
      marginTop: "15px"
    },
    blueDeep: {
      margin: 5,
      color: "#fff",
      backgroundColor: "#1976d2",
      fontSize: 20
    }
  };
};
class GamesNoteUnit extends React.Component {
  _waiting = true;
  state = {
    backgroundColor: "white",
    noteSize: ""
  };

  handleSizeChange = e => {
    this.setState({ noteSize: "" });
    if (e.target.value) {
      this.setState({ noteSize: e.target.value });
      this.props.handlePreorder(
        `${this.props.group}${this.props.type}`,
        this.props.business,
        e.target.value,
        this.props.number,
        this.props.times,
        this.props.settlement,
        this.props.start,
        this.props.end
      );
      this.setState({ backgroundColor: indigo[100] });
    } else {
      this.props.handlePreorder(
        `${this.props.group}${this.props.type}`,
        this.props.business,
        "0",
        this.props.number,
        this.props.times,
        this.props.settlement,
        this.props.start,
        this.props.end
      );
      this.setState({ backgroundColor: "white" });
    }
  };

  UNSAFE_componentWillReceiveProps = () => {
    if (this.props.reset && this._waiting) {
      this._waiting = false;
      this.setState({ noteSize: "", backgroundColor: "white" }, () => {
        this._waiting = true;
      });
    }
  };

  _SCRANK = () => {
    return <GamesImg number={this.props.type} type={Config.CONST_SC} />;
  };

  _LHCBALL = () => {
    return <GamesImg number={this.props.type} type={Config.CONST_LHC} />;
  };

  _SSCBALL = () => {
    return <GamesImg number={this.props.type} type={Config.CONST_SSC} />;
  };

  _K3ExistOrNotExist = () => {
    return <GamesImg number={this.props.type} type={Config.CONST_K3} />;
  };

  _KL8ZM = () => {
    return <GamesImg number={this.props.type} type={Config.CONST_KL8} />;
  };

  _K32T = () => {
    let k32t;
    if (this.props.logo === Config.CONST_K32T) {
      k32t = String(this.props.type).split(",");
    }
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={3}>
            <GamesImg number={k32t[0]} type={Config.CONST_K3} />
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={3}>
            <GamesImg number={k32t[1]} type={Config.CONST_K3} />
          </Grid>
        </Grid>
      </div>
    );
  };

  _K33LOr3T = () => {
    let k33lOr3t;
    if (this.props.logo === Config.CONST_K33LOr3T) {
      k33lOr3t = String(this.props.type).split(",");
    }
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={2}>
            <GamesImg number={k33lOr3t[0]} type={Config.CONST_K3} />
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={2}>
            <GamesImg number={k33lOr3t[1]} type={Config.CONST_K3} />
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={2}>
            <GamesImg number={k33lOr3t[2]} type={Config.CONST_K3} />
          </Grid>
          <Grid item xs={1} />
        </Grid>
      </div>
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <Grid container spacing={24}>
        <Grid item xs={4}>
          {this.props.logo === Config.CONST_LHC && this._LHCBALL()}
          {this.props.logo === Config.CONST_SCRANK && this._SCRANK()}
          {this.props.logo === Config.CONST_SSCLJ && this._SSCBALL()}
          {this.props.logo === Config.CONST_K3_EXIST &&
            this._K3ExistOrNotExist()}
          {this.props.logo === Config.CONST_K3_NOT_EXIST &&
            this._K3ExistOrNotExist()}
          {this.props.logo === Config.CONST_K32T && this._K32T()}
          {this.props.logo === Config.CONST_K33LOr3T && this._K33LOr3T()}
          {this.props.logo === Config.CONST_FONT && (
            <Typography variant="h6" className={classes.font} align="center">
              {this.props.type}
            </Typography>
          )}
          {this.props.logo === Config.CONST_KL8ZM && this._KL8ZM()}
        </Grid>
        <Grid item xs={4}>
          <div className={classes.divider}>
            <Typography variant="h6" align="center" color="secondary">
              {this.props.times}
            </Typography>
          </div>
        </Grid>
        <Grid item xs={4}>
          {!this.props.isSealed && (
            <TextField
              id="outlined-dense"
              label={Config.CONST_NUMBER}
              variant="outlined"
              style={{ backgroundColor: this.state.backgroundColor }}
              onChange={this.handleSizeChange}
              value={this.state.noteSize}
            />
          )}
          {this.props.isSealed && (
            <TextField
              id="outlined-dense"
              label=""
              variant="outlined"
              style={{ backgroundColor: "#C0C0C0" }}
              value={Config.CONST_FENGPAN}
            />
          )}
        </Grid>
      </Grid>
    );
  }
}

GamesNoteUnit.propTypes = {
  classes: PropTypes.object.isRequired,
  times: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  settlement: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  number: PropTypes.string.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  group: PropTypes.string.isRequired,
  isSealed: PropTypes.bool.isRequired,
  logo: PropTypes.string.isRequired
};
export default withStyles(styles)(GamesNoteUnit);
