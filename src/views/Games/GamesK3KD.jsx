import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesK3KD extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BIG,
      times: this.props.conf.settle_k3_kd_big_times,
      settle: window.settlement.SETTLE_K3_KD_BIG,
      number: Config.CONST_BIG
    },
    {
      name: Config.CONST_SMALL,
      times: this.props.conf.settle_k3_kd_small_times,
      settle: window.settlement.SETTLE_K3_KD_SMALL,
      number: Config.CONST_SMALL
    },
    {
      name: Config.CONST_ODD,
      times: this.props.conf.settle_k3_kd_odd_times,
      settle: window.settlement.SETTLE_K3_KD_ODD,
      number: Config.CONST_ODD
    },
    {
      name: Config.CONST_EVEN,
      times: this.props.conf.settle_k3_kd_even_times,
      settle: window.settlement.SETTLE_K3_KD_EVEN,
      number: Config.CONST_EVEN
    }
  ];

  _numbers1 = [
    {
      name: "0",
      times: this.props.conf.settle_k3_kd_0_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "0"
    },
    {
      name: "1",
      times: this.props.conf.settle_k3_kd_1_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "1"
    },
    {
      name: "2",
      times: this.props.conf.settle_k3_kd_2_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "2"
    },
    {
      name: "3",
      times: this.props.conf.settle_k3_kd_3_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "3"
    }
  ];

  _numbers2 = [
    {
      name: "4",
      times: this.props.conf.settle_k3_kd_4_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "4"
    },
    {
      name: "5",
      times: this.props.conf.settle_k3_kd_5_times,
      settle: window.settlement.SETTLE_K3_KD_NUMBER,
      number: "5"
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
              <TableCell />
              <TableCell />
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesK3KD.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesK3KD);
