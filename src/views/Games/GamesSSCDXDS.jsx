import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesDXDS extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <GamesNoteUnit
                  type={Config.CONST_BIG}
                  times={
                    this.props.conf.settle_ssc_single_big_times
                      ? `${this.props.conf.settle_ssc_single_big_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_SINGLE_BIG}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDS${Config.CONST_BIG}`}
                  business={this.props.business}
                  reset={this.props.reset}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <GamesNoteUnit
                  type={Config.CONST_SMALL}
                  times={
                    this.props.conf.settle_ssc_single_small_times
                      ? `${this.props.conf.settle_ssc_single_small_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_SINGLE_SMALL}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDS${Config.CONST_SMALL}`}
                  business={this.props.business}
                  reset={this.props.reset}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <GamesNoteUnit
                  type={Config.CONST_ODD}
                  times={
                    this.props.conf.settle_ssc_single_odd_times
                      ? `${this.props.conf.settle_ssc_single_odd_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_SINGLE_ODD}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDS${Config.CONST_ODD}`}
                  business={this.props.business}
                  reset={this.props.reset}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <GamesNoteUnit
                  type={Config.CONST_EVEN}
                  times={
                    this.props.conf.settle_ssc_single_even_times
                      ? `${this.props.conf.settle_ssc_single_even_times}`
                      : "NAN"
                  }
                  settlement={window.settlement.SETTLE_SSC_SINGLE_EVEN}
                  handlePreorder={this.props.handlePreorder}
                  number={`DXDS${Config.CONST_EVEN}`}
                  business={this.props.business}
                  reset={this.props.reset}
                  start={this.props.start}
                  end={this.props.end}
                  group={this.props.title}
                  isSealed={this.props.isSealed}
                  logo={Config.CONST_FONT}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesDXDS.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesDXDS);
