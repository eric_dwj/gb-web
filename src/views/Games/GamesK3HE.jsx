import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesK3HE extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BIG,
      times: this.props.conf.settle_k3_he_big_times,
      settle: window.settlement.SETTLE_K3_HE_BIG,
      number: Config.CONST_BIG
    },
    {
      name: Config.CONST_SMALL,
      times: this.props.conf.settle_k3_he_small_times,
      settle: window.settlement.SETTLE_K3_HE_SMALL,
      number: Config.CONST_SMALL
    },
    {
      name: Config.CONST_ODD,
      times: this.props.conf.settle_k3_he_odd_times,
      settle: window.settlement.SETTLE_K3_HE_ODD,
      number: Config.CONST_ODD
    },
    {
      name: Config.CONST_EVEN,
      times: this.props.conf.settle_k3_he_even_times,
      settle: window.settlement.SETTLE_K3_HE_EVEN,
      number: Config.CONST_EVEN
    }
  ];

  _numbers1 = [
    {
      name: "3",
      times: this.props.conf.settle_k3_he_3_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "3"
    },
    {
      name: "4",
      times: this.props.conf.settle_k3_he_4_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "4"
    },
    {
      name: "5",
      times: this.props.conf.settle_k3_he_5_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "5"
    },
    {
      name: "6",
      times: this.props.conf.settle_k3_he_6_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "6"
    }
  ];

  _numbers2 = [
    {
      name: "7",
      times: this.props.conf.settle_k3_he_7_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "7"
    },
    {
      name: "8",
      times: this.props.conf.settle_k3_he_8_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "8"
    },
    {
      name: "9",
      times: this.props.conf.settle_k3_he_9_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "9"
    },
    {
      name: "10",
      times: this.props.conf.settle_k3_he_10_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "10"
    }
  ];

  _numbers3 = [
    {
      name: "11",
      times: this.props.conf.settle_k3_he_11_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "11"
    },
    {
      name: "12",
      times: this.props.conf.settle_k3_he_12_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "12"
    },
    {
      name: "13",
      times: this.props.conf.settle_k3_he_13_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "13"
    },
    {
      name: "14",
      times: this.props.conf.settle_k3_he_14_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "14"
    }
  ];

  _numbers4 = [
    {
      name: "15",
      times: this.props.conf.settle_k3_he_15_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "15"
    },
    {
      name: "16",
      times: this.props.conf.settle_k3_he_16_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "16"
    },
    {
      name: "17",
      times: this.props.conf.settle_k3_he_17_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "17"
    },
    {
      name: "18",
      times: this.props.conf.settle_k3_he_18_times,
      settle: window.settlement.SETTLE_K3_HE_NUMBER,
      number: "18"
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers3.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers4.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesK3HE.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesK3HE);
