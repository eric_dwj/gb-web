import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesKL8WX extends React.Component {
  _numbers = [
    {
      name: Config.CONST_JING,
      times: this.props.conf.settle_kl_wx_jing_times,
      settle: window.settlement.SETTLE_KL_WX_JING,
      number: Config.CONST_JING
    },
    {
      name: Config.CONST_MU,
      times: this.props.conf.settle_kl_wx_mu_times,
      settle: window.settlement.SETTLE_KL_WX_MU,
      number: Config.CONST_MU
    },
    {
      name: Config.CONST_SHUI,
      times: this.props.conf.settle_kl_wx_shui_times,
      settle: window.settlement.SETTLE_KL_WX_SHUI,
      number: Config.CONST_SHUI
    },
    {
      name: Config.CONST_HUO,
      times: this.props.conf.settle_kl_wx_huo_times,
      settle: window.settlement.SETTLE_KL_WX_HUO,
      number: Config.CONST_HUO
    },
    {
      name: Config.CONST_TU,
      times: this.props.conf.settle_kl_wx_tu_times,
      settle: window.settlement.SETTLE_KL_WX_TU,
      number: Config.CONST_TU
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesKL8WX.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesKL8WX);
