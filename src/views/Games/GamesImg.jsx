import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import lhcUtil from "../../api/util/lhcUtil";
import Config from "../../config";

const styles = () => ({
  img: {
    width: 35,
    margin: 1
  },
  // orangeAvatar: {
  //   margin: 2,
  //   color: "#fff",
  //   backgroundColor: red[500]
  // },
  blueDeep: {
    margin: 2,
    color: "#fff",
    backgroundColor: "#1976d2"
  },
  blueLight: {
    margin: 2,
    color: "#fff",
    backgroundColor: "#83d0f2"
  },
  blue: {
    margin: 5,
    backgroundColor: "#00bfff"
  },
  red: {
    margin: 5,
    backgroundColor: "#ff3300"
  },
  green: {
    margin: 5,
    backgroundColor: "#4caf50"
  }
});

class GamesImg extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        {window.resource[`${this.props.type}_${this.props.number}`] && (
          <img
            src={window.resource[`${this.props.type}_${this.props.number}`]}
            style={{ width: 30, margin: 4 }}
            alt={this.props.type}
          />
        )}
        {!window.resource[`${this.props.type}_${this.props.number}`] &&
          this.props.type === Config.CONST_KL8 && (
            <Avatar
              className={
                parseInt(this.props.number, 10) > 40
                  ? classes.blueDeep
                  : classes.blueLight
              }
            >
              {this.props.number}
            </Avatar>
          )}
        {!window.resource[`${this.props.type}_${this.props.number}`] &&
          this.props.type === Config.CONST_LHC && (
            <Avatar className={classes[lhcUtil.numBose(this.props.number)]}>
              {this.props.number}
            </Avatar>
          )}
      </div>
    );
  }
}

GamesImg.propTypes = {
  classes: PropTypes.object.isRequired,
  number: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
};
export default withStyles(styles)(GamesImg);
