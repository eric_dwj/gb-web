import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCTMTouShu extends React.Component {
  _numbers = [
    {
      name: `0${Config.CONST_TOU}`,
      times: this.props.conf.settle_lhc_te_tou_0_number_times,
      settle: window.settlement.SETTLE_LHC_TE_TOU_NUMBER,
      number: "0"
    },
    {
      name: `1${Config.CONST_TOU}`,
      times: this.props.conf.settle_lhc_te_tou_1_number_times,
      settle: window.settlement.SETTLE_LHC_TE_TOU_NUMBER,
      number: "1"
    },
    {
      name: `2${Config.CONST_TOU}`,
      times: this.props.conf.settle_lhc_te_tou_2_number_times,
      settle: window.settlement.SETTLE_LHC_TE_TOU_NUMBER,
      number: "2"
    },
    {
      name: `3${Config.CONST_TOU}`,
      times: this.props.conf.settle_lhc_te_tou_3_number_times,
      settle: window.settlement.SETTLE_LHC_TE_TOU_NUMBER,
      number: "3"
    }
  ];

  _numbers1 = [
    {
      name: `4${Config.CONST_TOU}`,
      times: this.props.conf.settle_lhc_te_tou_4_number_times,
      settle: window.settlement.SETTLE_LHC_TE_TOU_NUMBER,
      number: "4"
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCTMTouShu.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCTMTouShu);
