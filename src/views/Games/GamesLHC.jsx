import React from "react";
import Grid from "@material-ui/core/Grid";
import GamesLHCTM from "./GamesLHCTM";
import GamesLHCZM from "./GamesLHCZM";
import GamesLHCTMLM from "./GamesLHCTMLM";
import GamesLHCLMDXDS from "./GamesLHCLMDXDS";
import GamesLHCLMZH from "./GamesLHCLMZH";
import GamesLHCZM16 from "./GamesLHCZM16";
import GamesLHCLMTMHS from "./GamesLHCLMTMHS";
import GamesLHCLMTMSX from "./GamesLHCLMTMSX";
import GamesLHCZMLM from "./GamesLHCZMLM";
import GamesLHCZMTE from "./GamesLHCZMTE";
import GamesLHCTMBOSE from "./GamesLHCTMBOSE";
import GamesLHCTM7SEBO from "./GamesLHCTM7SEBO";
import GamesLHCTMRedSEBO from "./GamesLHCTMRedSEBO";
import GamesLHCTMBlueSEBO from "./GamesLHCTMBlueSEBO";
import GamesLHCTMGreenSEBO from "./GamesLHCTMGreenSEBO";
import GamesLHCTMTouShu from "./GamesLHCTMTouShu";
import GamesLHCTMWeiShu from "./GamesLHCTMWeiShu";
import GamesLHCTMXiao from "./GamesLHCTMXiao";
import GamesLHCZMXiao from "./GamesLHCZMXiao";
import GamesLHCZXiao from "./GamesLHCZXiao";
import GamesLHCWuXing from "./GamesLHCWuXing";
import Config from "../../config.js";

const GamesLHC = () => {};

GamesLHC.processLHC = (
  props,
  state,
  handlePreorder,
  views,
  menus,
  indents,
  i
) => {
  if (views[i] === "LHC_TEMA") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesLHCTM
          key={`LHC_TEMA${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_TEMA}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
        <GamesLHCTMLM
          key={`LHC_TEMA_LIANMIAN${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_LIANGMIAN}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_Liang_Mian") {
    indents.push(
      <Grid key={`Grid_TMHS_${i}`} item xs={4}>
        <GamesLHCLMTMHS
          conf={window.conf[props.business]}
          title={Config.CONST_TEMAHESHU}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
        <GamesLHCLMTMSX
          conf={window.conf[props.business]}
          title={Config.CONST_TEMASHENGXIAO}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
    indents.push(
      <Grid key={`Grid_TM_${i}`} item xs={4}>
        <GamesLHCLMDXDS
          conf={window.conf[props.business]}
          title={Config.CONST_TEMA}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
    indents.push(
      <Grid key={`Grid_ZH_${i}`} item xs={4}>
        <GamesLHCLMZH
          conf={window.conf[props.business]}
          title={Config.CONST_ZH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZHENGMA") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesLHCZM
          key={`LHC_ZHENGMA${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_ZHENGMA}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM_Liang_Mian") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesLHCZMLM
          key={`LHC_ZM_Liang_Mian${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_LIANGMIAN}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM1") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={4}>
        <GamesLHCZM16
          key={`LHC_ZM1${i}`}
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}1`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM2") {
    indents.push(
      <Grid key={`LHC_ZM2${i}`} item xs={4}>
        <GamesLHCZM16
          key={`LHC_ZM_Liang_Mian${i}`}
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}2`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM3") {
    indents.push(
      <Grid key={`LHC_ZM3${i}`} item xs={4}>
        <GamesLHCZM16
          key={`LHC_ZM3${i}`}
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}3`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM4") {
    indents.push(
      <Grid key={`LHC_ZM4${i}`} item xs={4}>
        <GamesLHCZM16
          key={`LHC_ZM4${i}`}
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}4`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM5") {
    indents.push(
      <Grid key={`LHC_ZM5${i}`} item xs={4}>
        <GamesLHCZM16
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}5`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZM6") {
    indents.push(
      <Grid key={`LHC_ZM6${i}`} item xs={4}>
        <GamesLHCZM16
          conf={window.conf[props.business]}
          title={`${Config.CONST_ZHENGMA}6`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZTE") {
    indents.push(
      <Grid key={`LHC_ZTE${i}`} item xs={12}>
        <GamesLHCZMTE
          conf={window.conf[props.business]}
          title={Config.CONST_ZHENGTE}
          menus={menus}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_TEMA_SEBO") {
    indents.push(
      <Grid key={`LHC_TEMA_SEBO${i}`} item xs={12}>
        <GamesLHCTMBOSE
          conf={window.conf[props.business]}
          title={`${Config.CONST_TEMA}${Config.CONST_SEBO}`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_7_SEBO") {
    indents.push(
      <Grid key={`LHC_7_SEBO${i}`} item xs={12}>
        <GamesLHCTM7SEBO
          conf={window.conf[props.business]}
          title={`7${Config.CONST_SEBO}`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_RED_SEBO") {
    indents.push(
      <Grid key={`LHC_RED_SEBO${i}`} item xs={4}>
        <GamesLHCTMRedSEBO
          conf={window.conf[props.business]}
          title={Config.CONST_HONGQIU}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_BLUE_SEBO") {
    indents.push(
      <Grid key={`LHC_BLUE_SEBO${i}`} item xs={4}>
        <GamesLHCTMBlueSEBO
          conf={window.conf[props.business]}
          title={Config.CONST_LANGQIU}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_GREEN_SEBO") {
    indents.push(
      <Grid key={`LHC_GREEN_SEBO${i}`} item xs={4}>
        <GamesLHCTMGreenSEBO
          conf={window.conf[props.business]}
          title={Config.CONST_LVQIU}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_TEMA_TOU") {
    indents.push(
      <Grid key={`LHC_TEMA_TOU${i}`} item xs={12}>
        <GamesLHCTMTouShu
          conf={window.conf[props.business]}
          title={`${Config.CONST_TEMA}${Config.CONST_TOUSHU}`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_TEMA_WEI") {
    indents.push(
      <Grid key={`LHC_TEMA_WEI${i}`} item xs={12}>
        <GamesLHCTMWeiShu
          conf={window.conf[props.business]}
          title={`${Config.CONST_TEMA}${Config.CONST_WEISHU}`}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_TEXIAO") {
    indents.push(
      <Grid key={`LHC_TEXIAO${i}`} item xs={12}>
        <GamesLHCTMXiao
          conf={window.conf[props.business]}
          title={Config.CONST_TEMASHENGXIAO}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZHENGXIAO") {
    indents.push(
      <Grid key={`LHC_ZHENGXIAO${i}`} item xs={12}>
        <GamesLHCZMXiao
          conf={window.conf[props.business]}
          title={Config.CONST_ZHENGXIAO}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_ZHONGXIAO") {
    indents.push(
      <Grid key={`LHC_ZHONGXIAO${i}`} item xs={12}>
        <GamesLHCZXiao
          conf={window.conf[props.business]}
          title={Config.CONST_ZHONGXIAO}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  } else if (views[i] === "LHC_WUXING") {
    indents.push(
      <Grid key={`LHC_WUXING${i}`} item xs={5}>
        <GamesLHCWuXing
          conf={window.conf[props.business]}
          title={Config.CONST_WUXING}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`49`}
        />
      </Grid>
    );
  }
};
export default GamesLHC;
