/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import GamesK3HE from "./GamesK3HE.jsx";
import GamesK33L from "./GamesK33L.jsx";
import GamesK33T from "./GamesK33T.jsx";
import GamesK32T from "./GamesK32T.jsx";
import GamesK3KD from "./GamesK3KD.jsx";
import GamesK3PD from "./GamesK3PD.jsx";
import GamesK3NotExist from "./GamesK3NotExist.jsx";
import GamesK3Exist from "./GamesK3Exist.jsx";
import Grid from "@material-ui/core/Grid";
import Config from "../../config.js";
const GamesK3 = () => {};

GamesK3.processK3 = (props, state, handlePreorder, views, indents, i) => {
  if (views[i] === "K3_HE") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesK3HE
          key={`K3_HE_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_HEZHI}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_3L") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={5}>
        <GamesK33L
          key={`K3_3L_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SLH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_3T") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={5}>
        <GamesK33T
          key={`K3_3T_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_STH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_2T") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={5}>
        <GamesK32T
          key={`K3_2T_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_ETH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_KD") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesK3KD
          key={`K3_KD_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_KD}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_PD") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesK3PD
          key={`K3_PD_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_PD}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_NOT_EXIST") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesK3NotExist
          key={`K3_NOT_EXIST_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_NOT_EXIST_NUMBER}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "K3_EXIST") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesK3Exist
          key={`K3_EXIST_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_EXIST_NUMBER}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  }
};
export default GamesK3;
