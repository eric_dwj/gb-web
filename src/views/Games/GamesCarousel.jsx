import React from "react";
import PropTypes from "prop-types";
import AliceCarousel from "react-alice-carousel";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import GamesCard from "./GamesCard.jsx";
import "react-alice-carousel/lib/alice-carousel.css";

class GamesCarousel extends React.Component {
  state = {
    currentIndex: 0,
    responsive: { 1024: { items: 6 } },
    galleryItems: this.props.gamelist.map((game, index) => (
      <GamesCard
        key={index}
        img={game.img}
        name={game.name}
        msg={game.msg}
        business={game.business}
        catagory={game.catagory}
        gamelist={this.props.gamelist}
        index={index}
        onSelectGame={this.props.onSelectGame}
      />
    ))
  };

  slideTo = i => this.setState({ currentIndex: i });

  onSlideChanged = e => this.setState({ currentIndex: e.item });

  slideNext = () =>
    this.setState({ currentIndex: this.state.currentIndex + 1 });

  slidePrev = () =>
    this.setState({ currentIndex: this.state.currentIndex - 1 });

  render() {
    const { galleryItems, responsive, currentIndex } = this.state;
    return (
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <AliceCarousel
            dotsDisabled={true}
            buttonsDisabled={true}
            items={galleryItems}
            responsive={responsive}
            slideToIndex={currentIndex}
            onSlideChanged={this.onSlideChanged}
            autoPlayInterval={5000}
            autoPlayDirection="rtl"
            autoPlay={true}
            fadeOutAnimation={true}
            mouseDragEnabled={true}
          />
        </Grid>
        <Grid item xs={3} />
        <Grid item xs={2}>
          <Button
            onClick={() => this.slidePrev()}
            color="primary"
            variant="outlined"
            size="large"
          >
            {`上一页`}
          </Button>
        </Grid>
        <Grid item xs={2} />
        <Grid item xs={2}>
          <Button
            onClick={() => this.slideNext()}
            color="primary"
            variant="outlined"
            size="large"
          >
            {`下一页`}
          </Button>
        </Grid>
        <Grid item xs={3} />
      </Grid>
    );
  }
}
GamesCarousel.propTypes = {
  gamelist: PropTypes.array.isRequired,
  onSelectGame: PropTypes.func.isRequired
};
export default GamesCarousel;
