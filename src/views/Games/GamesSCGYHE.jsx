import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config.js";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesSCGYHE extends React.Component {
  _numbers = [
    {
      name: Config.CONST_GY_BIG,
      times: this.props.conf.settle_sc_gy_big_times,
      settle: window.settlement.SETTLE_SC_GY_BIG,
      number: Config.CONST_GY_BIG
    },
    {
      name: Config.CONST_GY_SMALL,
      times: this.props.conf.settle_sc_gy_small_times,
      settle: window.settlement.SETTLE_SC_GY_SMALL,
      number: Config.CONST_GY_SMALL
    },
    {
      name: Config.CONST_GY_ODD,
      times: this.props.conf.settle_sc_gy_odd_times,
      settle: window.settlement.SETTLE_SC_GY_ODD,
      number: Config.CONST_GY_ODD
    },
    {
      name: Config.CONST_GY_EVEN,
      times: this.props.conf.settle_sc_gy_even_times,
      settle: window.settlement.SETTLE_SC_GY_EVEN,
      number: Config.CONST_GY_EVEN
    }
  ];

  _numbers1 = [
    {
      name: "3",
      times: this.props.conf.settle_sc_gy_3_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "3"
    },
    {
      name: "4",
      times: this.props.conf.settle_sc_gy_4_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "4"
    },
    {
      name: "5",
      times: this.props.conf.settle_sc_gy_5_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "5"
    },
    {
      name: "6",
      times: this.props.conf.settle_sc_gy_6_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "6"
    }
  ];

  _numbers2 = [
    {
      name: "7",
      times: this.props.conf.settle_sc_gy_7_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "7"
    },
    {
      name: "8",
      times: this.props.conf.settle_sc_gy_8_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "8"
    },
    {
      name: "9",
      times: this.props.conf.settle_sc_gy_9_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "9"
    },
    {
      name: "10",
      times: this.props.conf.settle_sc_gy_10_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "10"
    }
  ];

  _numbers3 = [
    {
      name: "11",
      times: this.props.conf.settle_sc_gy_11_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "11"
    },
    {
      name: "12",
      times: this.props.conf.settle_sc_gy_12_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "12"
    },
    {
      name: "13",
      times: this.props.conf.settle_sc_gy_13_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "13"
    },
    {
      name: "14",
      times: this.props.conf.settle_sc_gy_14_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "14"
    }
  ];

  _numbers4 = [
    {
      name: "15",
      times: this.props.conf.settle_sc_gy_15_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "15"
    },
    {
      name: "16",
      times: this.props.conf.settle_sc_gy_16_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "16"
    },
    {
      name: "17",
      times: this.props.conf.settle_sc_gy_17_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "17"
    },
    {
      name: "18",
      times: this.props.conf.settle_sc_gy_18_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "18"
    }
  ];

  _numbers5 = [
    {
      name: "19",
      times: this.props.conf.settle_sc_gy_19_times,
      settle: window.settlement.SETTLE_SC_GY_NUMBER,
      number: "19"
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers3.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers4.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers5.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
              <TableCell />
              <TableCell />
              <TableCell />
              <TableCell />
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesSCGYHE.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesSCGYHE);
