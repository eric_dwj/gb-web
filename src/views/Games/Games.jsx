import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Fab from "@material-ui/core/Fab";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import GamesCarousel from "./GamesCarousel.jsx";
import GamesPanel from "./GamesPanel.jsx";
import assetServ from "../../api/service/asset";
const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "8px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

class Games extends React.Component {
  constructor() {
    super();
    this.state = {
      catagoryTabs: [],
      gamesTabs: [],
      business: null
    };
  }
  handleReturn = () =>{
    this.setState({ business: null});
  }
  onSelectGame = (business, name, gamelist, index) => {
    this.setState({ business, name, gamelist, selectedValue: index });
    const gamesTabs = [];
    if (Array.isArray(gamelist)) {
      gamelist.forEach((game, index) => {
        const tab = {
          tabName: game.name,
          tabContent: (
            <GamesPanel
              key={index}
              name={game.name}
              business={game.business}
              catagory={game.catagory}
              msg={game.msg}
            />
          )
        };
        gamesTabs.push(tab);
      });
    }
    this.setState({ gamesTabs });
  };
  componentDidMount = () => {
    const catagoryTabs = [];
    if (Array.isArray(window.gameTags)) {
      window.gameTags.forEach((tag, index) => {
        const tab = {
          tabName: tag.title,
          tabContent: (
            <GamesCarousel
              key={index}
              onSelectGame={this.onSelectGame}
              title={tag.title}
              gamelist={tag.data}
            />
          )
        };
        catagoryTabs.push(tab);
      });
    }
    this.setState({ catagoryTabs }, () => {
      const promise = assetServ.getBalance();
      if (promise !== null) {
        promise.then(resp => {
          window.balance = resp.data.data;
        });
      }
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={1} sm={1} md={1} />
        <GridItem xs={10} sm={10} md={10}>
          {!this.state.business && (
            <CustomTabs
              title="所有分类:"
              headerColor="danger"
              selectedValue={0}
              tabs={this.state.catagoryTabs}
            />
          )}
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          {this.state.business &&
            <Fab
              color="primary"
              variant="extended"
              className={classes.fab}
              onClick={this.handleReturn}
              style={{ marginRight: 20, fontSize: 20 }}
            >
              {` 返 回 `}
           </Fab>}
          {this.state.business && (
            <CustomTabs
              title="游戏列表:"
              headerColor="danger"
              selectedValue={this.state.selectedValue}
              tabs={this.state.gamesTabs}
            />
          )}
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(styles)(Games);
