import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config.js";
const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCTM7SEBO extends React.Component {
  _numbers = [
    {
      name: Config.CONST_HONGBO,
      times: this.props.conf.settle_lhc_7_bose_red_times,
      settle: window.settlement.SETTLE_LHC_7_BOSE_RED,
      number: Config.CONST_HONGBO
    },
    {
      name: Config.CONST_LANGBO,
      times: this.props.conf.settle_lhc_7_bose_blue_times,
      settle: window.settlement.SETTLE_LHC_7_BOSE_BLUE,
      number: Config.CONST_LANGBO
    },
    {
      name: Config.CONST_LVBO,
      times: this.props.conf.settle_lhc_7_bose_green_times,
      settle: window.settlement.SETTLE_LHC_7_BOSE_GREEN,
      number: Config.CONST_LVBO
    },
    {
      name: Config.CONST_HEJU,
      times: this.props.conf.settle_lhc_7_bose_he_times,
      settle: window.settlement.SETTLE_LHC_7_BOSE_HE,
      number: Config.CONST_HEJU
    }
  ];
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCTM7SEBO.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCTM7SEBO);
