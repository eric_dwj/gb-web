import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCTMGreenSEBO extends React.Component {
  _numbers = [
    {
      name: Config.CONST_LVDAN,
      times: this.props.conf.settle_lhc_te_bose_green_odd_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_ODD,
      number: Config.CONST_LVDAN
    },
    {
      name: Config.CONST_LVSHUANG,
      times: this.props.conf.settle_lhc_te_bose_green_even_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_EVEN,
      number: Config.CONST_LVSHUANG
    },
    {
      name: Config.CONST_LVBIG,
      times: this.props.conf.settle_lhc_te_bose_green_big_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_BIG,
      number: Config.CONST_LVBIG
    },
    {
      name: Config.CONST_LVSMALL,
      times: this.props.conf.settle_lhc_te_bose_green_small_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_SMALL,
      number: Config.CONST_LVSMALL
    },
    {
      name: Config.CONST_LVDD,
      times: this.props.conf.settle_lhc_te_bose_green_dd_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_DD,
      number: Config.CONST_LVDD
    },
    {
      name: Config.CONST_LVDS,
      times: this.props.conf.settle_lhc_te_bose_green_ds_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_DS,
      number: Config.CONST_LVDS
    },
    {
      name: Config.CONST_LVXD,
      times: this.props.conf.settle_lhc_te_bose_green_xd_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_XD,
      number: Config.CONST_LVXD
    },
    {
      name: Config.CONST_LVXS,
      times: this.props.conf.settle_lhc_te_bose_green_xs_times,
      settle: window.settlement.SETTLE_LHC_TE_BOSE_GREEN_XS,
      number: Config.CONST_LVXS
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={1} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this._numbers.map((value, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCTMGreenSEBO.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCTMGreenSEBO);
