import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCZMXiao extends React.Component {
  _numbers = [
    {
      name: Config.lhcxiaoname.shu,
      times: this.props.conf.settle_lhc_zm_xiao_shu_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "shu"
    },
    {
      name: Config.lhcxiaoname.niu,
      times: this.props.conf.settle_lhc_zm_xiao_niu_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "niu"
    },
    {
      name: Config.lhcxiaoname.hu,
      times: this.props.conf.settle_lhc_zm_xiao_hu_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "hu"
    },
    {
      name: Config.lhcxiaoname.tu,
      times: this.props.conf.settle_lhc_zm_xiao_tu_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "tu"
    }
  ];

  _numbers1 = [
    {
      name: Config.lhcxiaoname.long,
      times: this.props.conf.settle_lhc_zm_xiao_long_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "long"
    },
    {
      name: Config.lhcxiaoname.she,
      times: this.props.conf.settle_lhc_zm_xiao_she_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "she"
    },
    {
      name: Config.lhcxiaoname.ma,
      times: this.props.conf.settle_lhc_zm_xiao_ma_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "ma"
    },
    {
      name: Config.lhcxiaoname.yang,
      times: this.props.conf.settle_lhc_zm_xiao_yang_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "yang"
    }
  ];

  _numbers2 = [
    {
      name: Config.lhcxiaoname.hou,
      times: this.props.conf.settle_lhc_zm_xiao_hou_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "hou"
    },
    {
      name: Config.lhcxiaoname.ji,
      times: this.props.conf.settle_lhc_zm_xiao_ji_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "ji"
    },
    {
      name: Config.lhcxiaoname.gou,
      times: this.props.conf.settle_lhc_zm_xiao_gou_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "gou"
    },
    {
      name: Config.lhcxiaoname.zhu,
      times: this.props.conf.settle_lhc_zm_xiao_zhu_times,
      settle: window.settlement.SETTLE_LHC_ZM_XIAO,
      number: "zhu"
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCZMXiao.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCZMXiao);
