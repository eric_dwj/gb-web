import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import indigo from "@material-ui/core/colors/indigo";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import GamesConfirmModalInput from "./GamesConfirmModalInput";
import note from "../../api/service/note";
import config from "../../config";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    width: "35%",
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: "none"
  },
  table: {
    //minWidth: 700
  },
  tableHead: {
    backgroundColor: indigo[100],
    colors: "white"
  }
});

class GamesOrderConfirmModal extends React.Component {
  _waiting = true;
  state = {
    open: false,
    total: this.props.total,
    isSealed: false,
    error: false,
    errorMsg: null
  };

  UNSAFE_componentWillReceiveProps = () => {
    if (this._waiting) {
      this._waiting = false;
      if (this.props.isSealed) {
        this.setState({ open: false, total: this.props.total }, () => {
          this._waiting = true;
        });
        note.resetOrder();
      } else {
        this.setState(
          {
            open: this.props.open,
            total: this.props.total
          },
          () => {
            this._waiting = true;
          }
        );
      }
    }
  };
  handleSizeChange = (size, key) => {
    note.updateOrder(key, size);
    this.setState({ total: note.orderTotal(), error: false }, () => {
      this.forceUpdate();
    });
  };

  handleClose = () => {
    this.props.handleCloseModal(()=>{
      this.setState({open: false})
    });
  };

  handleConfirmClick = () => {
    if (!window.user) {
      this.setState({ error: true, errorMsg: `您还未登陆，请先登陆` });
      return;
    }
    let total = 0;
    let balanceFlag = true;

    for (const key in note.getOrder()) {
      if (note.getOrder().hasOwnProperty(key)) {
        const order = note.getOrder()[key];
        total = note.add(order.size, total);
      }
    }

    if (window.balance && window.balance.length > 0) {
      for (let index = 0; index < window.balance.length; index++) {
        const balance = window.balance[index];
        if (
          String(balance.coin).toUpperCase() ===
          String(config.LOTTER_COIN).toUpperCase()
        ) {
          if (parseFloat(balance.Available) < parseFloat(total)) {
            balanceFlag = false;
          }
        }
      }
    }

    if (parseFloat(total) === 0) {
      this.setState({ error: true, errorMsg: `下注额不能为0` });
    }

    if (!balanceFlag) {
      this.setState({ error: true, errorMsg: `您的账户余额不足，请充值。` });
    } else {
      note.sendOrder().then(resp => {
        if (resp.data.state === "success") {
          window.balance = resp.data.balance;
          note
            .getOrdersFromServer(
              this.props.business,
              note.getNextRound(this.props.business)
            )
            .then(resp => {
              if (resp.data.state === "success") {
                note.handleLatestOrders(resp.data.data);
                note.resetOrder();
                this.setState({ open: false }, () => {
                  this.forceUpdate();
                });
                this.props.handleResetOrder(true);
              }
            });
        } else if (
          resp.data.state === "failed" &&
          resp.data.msg.error === "balance not enough"
        ) {
          this.setState({
            error: true,
            errorMsg: `您的账户余额不足，请充值。`
          });
        } else {
          this.setState({ error: true, errorMsg: `系统错误` });
        }
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.state.open}
        onClose={this.handleClose}
      >
        <div style={getModalStyle()} className={classes.paper}>
          <Typography variant="h6">{`下注明细 (请确认注单)`}</Typography>
          <Table className={classes.table}>
            <TableHead className={classes.tableHead}>
              <TableRow>
                <TableCell>
                  <Typography variant="h6">{`号码`}</Typography>
                </TableCell>
                <TableCell>
                  <Typography
                    variant="h6"
                    id="modal-title"
                  >{`赔率`}</Typography>
                </TableCell>
                <TableCell>
                  <Typography
                    variant="h6"
                    id="modal-title"
                  >{`金额`}</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.props.orders &&
                Object.entries(this.props.orders).map(([key, value]) => {
                  return (
                    <TableRow key={key}>
                      <TableCell>{value.type}</TableCell>
                      <TableCell style={{ color: "red" }}>
                        {value.times}
                      </TableCell>
                      <TableCell>
                        <GamesConfirmModalInput
                          handleSizeChange={this.handleSizeChange}
                          size={value.size}
                          orderkey={key}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              <TableRow>
                <TableCell>
                  {`组数:${Object.entries(this.props.orders).length}`}
                </TableCell>
                <TableCell colSpan={2}>
                  <Grid container spacing={24}>
                    <Grid item xs={3}>
                      {`下注总额:`}
                    </Grid>
                    <Grid item xs={3}>
                      <div style={{ color: "red" }}>{this.state.total}</div>
                    </Grid>
                  </Grid>
                  <Typography />
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Grid style={{ padding: 28 }} container spacing={24}>
            <Grid item xs={3} />
            <Grid item xs={4}>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={this.handleConfirmClick}
                size="large"
                disabled={this.state.isSealed}
              >
                确定
              </Button>
            </Grid>
            <Grid item xs={5}>
              <Button
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={this.handleClose}
                size="large"
              >
                取消
              </Button>
            </Grid>
            <Grid item xs={3} />
            <Grid item xs={9}>
              {this.state.error && (
                <Typography color="secondary" variant="h6" component="h6">
                  {this.state.errorMsg}
                </Typography>
              )}
            </Grid>
          </Grid>
        </div>
      </Modal>
    );
  }
}

GamesOrderConfirmModal.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  orders: PropTypes.object.isRequired,
  total: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  handleResetOrder: PropTypes.func.isRequired,
  isSealed: PropTypes.bool.isRequired
};

export default withStyles(styles)(GamesOrderConfirmModal);
