import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Fab from "@material-ui/core/Fab";

import Typography from "@material-ui/core/Typography";
import NavigationIcon from "@material-ui/icons/Navigation";
const styles = theme => ({
  wrapper: {
    padding: 2
  },
  card: {
    maxWidth: 245
  },
  media: {
    borderRadius: theme.shape.borderRadius
  },
  margin: {
    margin: 1
  },
  extendedIcon: {
    marginRight: 1
  }
});

class GamesCard extends React.Component {
  handEnterGame = () => {
    this.props.onSelectGame(
      this.props.business,
      this.props.name,
      this.props.gamelist,
      this.props.index
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <Card className={classes.card}>
          <CardActionArea>
            <CardMedia
              component="img"
              className={classes.media}
              height="188"
              image={this.props.img}
              title="image"
              onClick={this.handEnterGame}
            />

            <CardContent>
              <Typography gutterBottom variant="h6" component="h6">
                {this.props.name}
              </Typography>
              <Typography component="p">{this.props.msg}</Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Fab
              variant="extended"
              color="secondary"
              aria-label="Add"
              className={classes.margin}
              onClick={this.handEnterGame}
            >
              <NavigationIcon className={classes.extendedIcon} />
              进入游戏
            </Fab>
          </CardActions>
        </Card>
      </div>
    );
  }
}

GamesCard.propTypes = {
  msg: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  gamelist: PropTypes.array.isRequired,
  business: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  onSelectGame: PropTypes.func.isRequired
};

export default withStyles(styles)(GamesCard);
