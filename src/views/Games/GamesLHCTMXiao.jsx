import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCTMXiao extends React.Component {
  _numbers = [
    {
      name: window.lhc.lhcxiaoname.shu,
      times: this.props.conf.settle_lhc_te_xiao_shu_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "shu"
    },
    {
      name: window.lhc.lhcxiaoname.niu,
      times: this.props.conf.settle_lhc_te_xiao_niu_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "niu"
    },
    {
      name: window.lhc.lhcxiaoname.hu,
      times: this.props.conf.settle_lhc_te_xiao_hu_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "hu"
    },
    {
      name: window.lhc.lhcxiaoname.tu,
      times: this.props.conf.settle_lhc_te_xiao_tu_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "tu"
    }
  ];

  _numbers1 = [
    {
      name: window.lhc.lhcxiaoname.long,
      times: this.props.conf.settle_lhc_te_xiao_long_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "long"
    },
    {
      name: window.lhc.lhcxiaoname.she,
      times: this.props.conf.settle_lhc_te_xiao_she_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "she"
    },
    {
      name: window.lhc.lhcxiaoname.ma,
      times: this.props.conf.settle_lhc_te_xiao_ma_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "ma"
    },
    {
      name: window.lhc.lhcxiaoname.yang,
      times: this.props.conf.settle_lhc_te_xiao_yang_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "yang"
    }
  ];

  _numbers2 = [
    {
      name: window.lhc.lhcxiaoname.hou,
      times: this.props.conf.settle_lhc_te_xiao_hou_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "hou"
    },
    {
      name: window.lhc.lhcxiaoname.ji,
      times: this.props.conf.settle_lhc_te_xiao_ji_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "ji"
    },
    {
      name: window.lhc.lhcxiaoname.gou,
      times: this.props.conf.settle_lhc_te_xiao_gou_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "gou"
    },
    {
      name: window.lhc.lhcxiaoname.zhu,
      times: this.props.conf.settle_lhc_te_xiao_zhu_times,
      settle: window.settlement.SETTLE_LHC_TE_XIAO,
      number: "zhu"
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers2.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCTMXiao.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCTMXiao);
