/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import GamesDXDSLHH from "./GamesSSCDXDSLHH.jsx";
import GamesDXDS from "./GamesSSCDXDS.jsx";
import GamesLJ from "./GamesSSCLJ.jsx";
import GamesQZH from "./GamesSSCQZH.jsx";
import GamesSSCNIU from "./GamesSSCNIU.jsx";
import Config from "../../config.js";
import Grid from "@material-ui/core/Grid";
const GamesSSC = () => {};
GamesSSC.processSSC = (props, state, handlePreorder, views, indents, i) => {
  if (views[i] === "SSC_Summary_Dragon_Tiger") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesDXDSLHH
          key={`Summary_Dragon_Tiger_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_LHH}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`5`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_First_Ball_DXDS") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesDXDS
          key={`First_Ball_DXDS_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIRST_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`1`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Second_Ball_DXDS") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesDXDS
          key={`Second_Ball_DXDS${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SECOND_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`1`}
          end={`2`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Third_Ball_DXDS") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesDXDS
          key={`Third_Ball_DXDS${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_THIRD_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`2`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Fourth_Ball_DXDS") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesDXDS
          key={`Fourth_Ball_DXDS${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FOURTH_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`3`}
          end={`4`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Fifth_Ball_DXDS") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesDXDS
          key={`Fifth_Ball_DXDS${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIFTH_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`4`}
          end={`5`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_First_Ball") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesLJ
          key={`First_Ball${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIRST_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`1`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Second_Ball") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesLJ
          key={`Second_Ball${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_SECOND_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`1`}
          end={`2`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Third_Ball") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesLJ
          key={`Third_Ball${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_THIRD_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`2`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Fourth_Ball") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesLJ
          key={`Fourth_Ball${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FOURTH_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`3`}
          end={`4`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Fifth_Ball") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={3}>
        <GamesLJ
          key={`Fifth_Ball${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIFTH_BALL}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`4`}
          end={`5`}
        />
      </Grid>
    );
    //'Front_Three','Middle_Three','Final_Three'
  } else if (views[i] === "SSC_Front_Three") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={4}>
        <GamesQZH
          key={`Front_Three${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_FIRST_THREE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`3`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Middle_Three") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={4}>
        <GamesQZH
          key={`Middle_Three${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_MIDDLE_THREE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`1`}
          end={`4`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_Final_Three") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={4}>
        <GamesQZH
          key={`Final_Three${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_LAST_THREE}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`2`}
          end={`5`}
        />
      </Grid>
    );
  } else if (views[i] === "SSC_NIU") {
    indents.push(
      <Grid key={`Grid_${i}`} item xs={12}>
        <GamesSSCNIU
          key={`NIU_${i}`}
          conf={window.conf[props.business]}
          title={Config.CONST_DOUNIU}
          handlePreorder={handlePreorder}
          business={props.business}
          reset={state.reset}
          isSealed={state.isSealed}
          start={`0`}
          end={`5`}
        />
      </Grid>
    );
  }
};
export default GamesSSC;
