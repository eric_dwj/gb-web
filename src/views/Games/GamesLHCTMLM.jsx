import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import indigo from "@material-ui/core/colors/indigo";
import Typography from "@material-ui/core/Typography";
import GamesNoteUnit from "./GamesNoteUnit.jsx";
import Config from "../../config";

const styles = () => {
  return {
    root: {
      width: "100%"
    },
    table: {
      minWidth: 200
    },
    tableHead: {
      backgroundColor: indigo[100],
      colors: "white"
    }
  };
};
class GamesLHCTMLM extends React.Component {
  _numbers = [
    {
      name: Config.CONST_BIG,
      times: this.props.conf.settle_lhc_te_big_times,
      settle: window.settlement.SETTLE_LHC_TE_BIG,
      number: Config.CONST_BIG
    },
    {
      name: Config.CONST_SMALL,
      times: this.props.conf.settle_lhc_te_small_times,
      settle: window.settlement.SETTLE_LHC_TE_SMALL,
      number: Config.CONST_SMALL
    },
    {
      name: Config.CONST_ODD,
      times: this.props.conf.settle_lhc_te_odd_times,
      settle: window.settlement.SETTLE_LHC_TE_ODD,
      number: Config.CONST_ODD
    },
    {
      name: Config.CONST_EVEN,
      times: this.props.conf.settle_lhc_te_even_times,
      settle: window.settlement.SETTLE_LHC_TE_EVEN,
      number: Config.CONST_EVEN
    }
  ];

  _numbers1 = [
    {
      name: Config.CONST_HONGBO,
      times: this.props.conf.settle_lhc_zm_bose_red_times,
      settle: window.settlement.SETTLE_LHC_ZM_BOSE_RED,
      number: Config.CONST_HONGBO
    },
    {
      name: Config.CONST_LANGBO,
      times: this.props.conf.settle_lhc_zm_bose_blue_times,
      settle: window.settlement.SETTLE_LHC_ZM_BOSE_BLUE,
      number: Config.CONST_LANGBO
    },
    {
      name: Config.CONST_LVBO,
      times: this.props.conf.settle_lhc_zm_bose_green_times,
      settle: window.settlement.SETTLE_LHC_ZM_BOSE_GREEN,
      number: Config.CONST_LVBO
    }
  ];

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell colSpan={5} align="center">
                <Typography variant="h6">{this.props.title}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {this._numbers.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
            <TableRow>
              {this._numbers1.map((value, index) => {
                return (
                  <TableCell key={index}>
                    <GamesNoteUnit
                      type={value.name}
                      times={value.times ? `${value.times}` : "NAN"}
                      settlement={value.settle}
                      handlePreorder={this.props.handlePreorder}
                      number={value.number}
                      business={this.props.business}
                      reset={this.props.reset}
                      start={this.props.start}
                      end={this.props.end}
                      group={this.props.title}
                      isSealed={this.props.isSealed}
                      logo={Config.CONST_FONT}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

GamesLHCTMLM.propTypes = {
  classes: PropTypes.object.isRequired,
  conf: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  handlePreorder: PropTypes.func.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  reset: PropTypes.bool.isRequired,
  isSealed: PropTypes.bool.isRequired
};
export default withStyles(styles)(GamesLHCTMLM);
