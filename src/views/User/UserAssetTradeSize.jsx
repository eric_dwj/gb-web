import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import config from "../../config";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import assetServ from "../../api/service/asset";

const styles = () => ({
  root: {
    display: "flex"
  }
});
class UserAssetTradeSize extends React.Component {
  state = {
    open: false,
    amount: null,
    assetbalance: null,
    lottybalance: null,
    balanceNotEnough: false
  };

  componentDidMount = async () => {
    let lottybalance, assetbalance;
    if (window.balance) {
      const resp = await assetServ.getBalance();
      if (resp.data && resp.data.data && resp.data.data.length > 0) {
        assetServ.setBalance(resp.data.data);
      }
    }
    for (let index = 0; index < window.balance.length; index++) {
      const record = window.balance[index];
      if (
        String(record.coin).toUpperCase() ===
        String(config.LOTTER_COIN).toUpperCase()
      ) {
        lottybalance = record;
      }
      if (
        String(record.coin).toUpperCase() ===
        String(this.props.coin).toUpperCase()
      ) {
        assetbalance = record;
      }
    }
    this.setState({ assetbalance, lottybalance });
  };

  handleTooltipClose = () => {
    this.setState({ open: false });
  };

  handleTooltipOpen = () => {
    this.setState({ open: true });
  };
  handleSellChange = event => {
    if (
      parseFloat(this.state.lottybalance.Available) >=
      parseFloat(event.target.value)
    ) {
      const sellLottySize = assetServ.calculateSell(
        this.state.assetbalance,
        this.state.lottybalance,
        event.target.value
      );
      this.props.onAmountInput(event.target.value, sellLottySize);
      this.setState({
        amount: event.target.value,
        balanceNotEnough: false
      });
    } else {
      this.setState({
        balanceNotEnough: true
      });
    }
  };
  handleBuyChange = event => {
    if (
      parseFloat(this.state.assetbalance.Available) >=
      parseFloat(event.target.value)
    ) {
      const buyLottySize = assetServ.calculateBuy(
        this.state.assetbalance,
        this.state.lottybalance,
        event.target.value
      );
      this.props.onAmountInput(event.target.value, buyLottySize);
      this.setState({
        amount: event.target.value,
        balanceNotEnough: false
      });
    } else {
      this.setState({
        balanceNotEnough: true
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={5} />
          <Grid item xs={5}>
            {this.props.side === "1" &&
              this.state.assetbalance &&
              this.state.lottybalance && (
                <div>
                  <Typography variant="h6" color="primary" gutterBottom>
                    {`您有${this.state.assetbalance.coin}数量${
                      this.state.assetbalance.Available
                    }，`}
                    {`最多可以兑换LOT数量:${assetServ.calculateBuy(
                      this.state.assetbalance,
                      this.state.lottybalance
                    )}。`}
                  </Typography>
                </div>
              )}
            {this.props.side === "1" && (
              <TextField
                id="outlined-name"
                label="交易数量"
                onChange={this.handleBuyChange}
                margin="normal"
                variant="outlined"
              />
            )}
            {this.props.side === "1" &&
              this.state.amount &&
              !this.state.balanceNotEnough && (
                <Typography variant="h6" color="secondary" gutterBottom>
                  {`您将使用${this.state.amount}个${
                    this.state.assetbalance.coin
                  }兑换LOT:${assetServ.calculateBuy(
                    this.state.assetbalance,
                    this.state.lottybalance,
                    this.state.amount
                  )}`}
                </Typography>
              )}

            {this.props.side === "2" &&
              this.state.assetbalance &&
              this.state.lottybalance && (
                <div>
                  <Typography variant="h6" color="primary" gutterBottom>
                    {`您有${this.state.lottybalance.coin}数量${
                      this.state.lottybalance.Available
                    }，`}
                    {`最多可以兑换${
                      this.state.assetbalance.coin
                    }数量:${assetServ.calculateSell(
                      this.state.assetbalance,
                      this.state.lottybalance
                    )}。`}
                  </Typography>
                </div>
              )}
            {this.props.side === "2" && (
              <TextField
                id="outlined-name"
                label="交易数量"
                onChange={this.handleSellChange}
                margin="normal"
                variant="outlined"
              />
            )}
            {this.state.balanceNotEnough && (
              <Typography variant="h6" color="secondary" gutterBottom>
                {`您的余额不足`}
              </Typography>
            )}
            {this.props.side === "2" &&
              this.state.amount &&
              !this.state.balanceNotEnough && (
                <Typography variant="h6" color="secondary" gutterBottom>
                  {`您将使用${this.state.amount}个${
                    this.state.lottybalance.coin
                  }兑换${
                    this.state.assetbalance.coin
                  }:${assetServ.calculateSell(
                    this.state.assetbalance,
                    this.state.lottybalance,
                    this.state.amount
                  )}`}
                </Typography>
              )}
          </Grid>
        </Grid>
      </div>
    );
  }
}
UserAssetTradeSize.propTypes = {
  classes: PropTypes.object.isRequired,
  onAmountInput: PropTypes.func.isRequired,
  coin: PropTypes.string.isRequired,
  side: PropTypes.string.isRequired
};

export default withStyles(styles)(UserAssetTradeSize);
