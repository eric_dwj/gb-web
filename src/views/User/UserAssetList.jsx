import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import Config from "../../config.js";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import UserAssetItem from "./UserAssetItem.jsx";
import assetServ from "../../api/service/asset";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  button: {
    margin: 10
  }
});
class UserAssetList extends React.Component {
  state = {
    balances: []
  };

  listInvited = () => {
    assetServ.getBalance().then(resp => {
      if (resp.data && resp.data.data && resp.data.data.length > 0) {
        assetServ.setBalance(resp.data.data);
        this.setState({ balances: resp.data.data });
      }
    });
  };

  componentDidMount = () => {
    this.listInvited();
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => {
            window.location.href = "/games/deposit";
          }}
        >
          <Typography variant="h6" style={{ color: "#fff" }} align="center">
            {Config.CONST_CUNBI}
          </Typography>
        </Button>
        <Button
          variant="contained"
          color="secondary"
          className={classes.button}
          onClick={() => {
            window.location.href = "/games/withdraw";
          }}
        >
          <Typography variant="h6" style={{ color: "#fff" }} align="center">
            {Config.CONST_QUBI}
          </Typography>
        </Button>
        <br /><br />
        <List
          component="nav"
          subheader={
            <ListSubheader component="div">
              <Typography variant="h6" align="left">
                {Config.CONST_WODEZICHANLIEBIAO}
              </Typography>
            </ListSubheader>
          }
        >
          <br />
          <UserAssetItem
            coin={Config.CONST_BIZHONG}
            price={Config.CONST_JIAGE}
            available={Config.CONST_KEYONGYUE}
            frozen={Config.CONST_XIADANDONGJIE}
            total={Config.CONST_HEJI}
          />
          <Divider />
          {this.state.balances.map((value, index) => {
            return (
              <UserAssetItem
                key={index}
                coin={value.coin}
                price={value.Price}
                available={`${value.Available}`}
                frozen={`${value.Frozen}`}
                total={value.Total}
              />
            );
          })}
        </List>
      </div>
    );
  }
}

UserAssetList.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(UserAssetList);
