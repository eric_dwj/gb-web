import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
const styles = () => ({
  root: {
    width: "100%"
  }
});
class UserExchangeSubmit extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={5} />
          <Grid item xs={5}>
            {this.props.side === "1" && (
              <List component="nav">
                <ListItem button>
                  <Typography variant="h6">{`业务类型：购买LOT`}</Typography>
                </ListItem>
                <ListItem button>
                  <Typography variant="h6">
                    {`您将支付：${this.props.amount} ${this.props.coin}`}
                  </Typography>
                </ListItem>
                <ListItem button>
                  <Typography variant="h6">
                    {`您将购入LOT: ${this.props.buyAmount}`}
                  </Typography>
                </ListItem>
              </List>
            )}
            {this.props.side === "2" && (
              <List component="nav">
                <ListItem button>
                  <Typography variant="h6">{`业务类型: 出售LOT`}</Typography>
                </ListItem>
                <ListItem button>
                  <Typography variant="h6">
                    {`您将支付: ${this.props.amount} LOT`}
                  </Typography>
                </ListItem>
                <ListItem button>
                  <Typography variant="h6">
                    {`您将购入${this.props.coin}: ${this.props.buyAmount}`}
                  </Typography>
                </ListItem>
              </List>
            )}
          </Grid>
        </Grid>
      </div>
    );
  }
}

UserExchangeSubmit.propTypes = {
  classes: PropTypes.object.isRequired,
  side: PropTypes.string.isRequired,
  coin: PropTypes.string.isRequired,
  amount: PropTypes.string.isRequired,
  buyAmount: PropTypes.string.isRequired
};

export default withStyles(styles)(UserExchangeSubmit);
