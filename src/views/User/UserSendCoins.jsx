import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import UserSendConfirm from "../User/UserSendConfirm";
import Typography from "@material-ui/core/Typography";
import inviteServ from "../../api/service/invite";
import assetServ from "../../api/service/asset";
import Config from "../../config";
const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    fontSize: 20
  },
  margin: {
    margin: theme.spacing.unit
  }
});

class UserSendCoins extends React.Component {
  state = {
    size: "",
    asset: Config.LOTTER_COIN,
    commissions: [],
    assets: [],
    userid: "",
    email: "",
    available: "",
    balance: [],
    error: false,
    open: false,
    sendObj: {}
  };

  loadData = async () => {
    if (!window.traderConfig || !window.traderConfig.coins) {
      await inviteServ.traderConfig();
    }

    const respInvited = await inviteServ.listInvited();
    const respParent = await inviteServ.getParent();
    const respBalance = await assetServ.getBalance();
    let sendArray;
    if (
      Array.isArray(respInvited.data.result) &&
      Array.isArray(respParent.data.result)
    ) {
      sendArray = respInvited.data.result.concat(respParent.data.result);
    }
    if (
      respBalance &&
      respBalance.data &&
      respBalance.data &&
      respInvited.data &&
      Array.isArray(respBalance.data.data)
    ) {
      let available = 0;
      for (let index = 0; index < respBalance.data.data.length; index++) {
        const coin = respBalance.data.data[index];
        if (
          String(coin.coin).toUpperCase() ===
          String(this.state.asset).toUpperCase()
        ) {
          available = coin.Available;
        }
      }

      this.setState({
        assets: window.traderConfig.coins,
        commissions: sendArray,
        balance: respBalance.data.data,
        available
      });
    }
  };

  componentDidMount = async () => {
    await this.loadData();
  };

  handleSend = () => {
    if (!this.state.size.match(/^([0-9])(([0-9]*)(\.([0-9]+))?)$/)) {
      this.setState({ error: true, errorMsg: `请输入正确转账数量` });
    } else if (parseFloat(this.state.size) > this.state.available) {
      this.setState({ error: true, errorMsg: `可用余额不足` });
    } else if (parseFloat(this.state.size) <= 0) {
      this.setState({ error: true, errorMsg: `余额必须大于0` });
    } else if (!this.state.userid) {
      this.setState({ error: true, errorMsg: `请选择收款人` });
    } else {
      const sendObj = {
        email: this.state.email,
        size: this.state.size,
        coin: this.state.asset
      };
      this.setState({ open: true, sendObj });
    }
  };

  handleUserChange = event => {
    let email;
    for (let index = 0; index < this.state.commissions.length; index++) {
      const commission = this.state.commissions[index];
      if (commission.userid === event.target.value) {
        email = commission.email;
      }
    }
    this.setState({
      userid: event.target.value,
      email,
      error: false
    });
  };

  handleAssetChange = event => {
    let available = 0;
    for (let index = 0; index < this.state.balance.length; index++) {
      const coin = this.state.balance[index];
      if (
        String(coin.coin).toUpperCase() ===
        String(event.target.value).toUpperCase()
      ) {
        available = coin.Available;
      }
    }

    this.setState({
      error: false,
      asset: event.target.value,
      available
    });
  };
  onSend = () => {
    assetServ
      .transfer(this.state.userid, this.state.asset, this.state.size)
      .then(resp => {
        if (resp && resp.data && resp.data.from) {
          window.balance = resp.data.from;
          let available = 0;
          for (let index = 0; index < window.balance.length; index++) {
            const coin = window.balance[index];
            if (
              String(coin.coin).toUpperCase() ===
              String(this.state.asset).toUpperCase()
            ) {
              available = coin.Available;
            }
          }
          this.setState({ open: false, available });
        }
      });
  };
  onClose = () => {
    this.setState({ open: false });
  };

  handleSizeChange = event => {
    this.setState({
      error: false,
      size: event.target.value
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <UserSendConfirm
          open={this.state.open}
          sendObj={this.state.sendObj}
          onClose={this.onClose}
          onSend={this.onSend}
        />
        <Grid container spacing={24}>
          <Grid item xs={2} />
          <Grid item xs={8}>
            <TextField
              id="outlined-select-user"
              select
              fullWidth
              label={Config.CONST_SHOUKUANGREN}
              className={classes.textField}
              value={this.state.userid}
              onChange={this.handleUserChange}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
              margin="normal"
              variant="outlined"
            >
              <option key={0}>{``}</option>
              {this.state.commissions.map(option => (
                <option key={option.userid} value={option.userid}>
                  {option.email}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={2} />
          <Grid item xs={8}>
            <TextField
              id="outlined-select-asset"
              select
              fullWidth
              label={Config.CONST_BIZHONG}
              className={classes.textField}
              value={this.state.asset}
              onChange={this.handleAssetChange}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
              margin="normal"
              variant="outlined"
            >
              {this.state.assets.map(asset => (
                <option key={asset} value={asset}>
                  {asset}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={2} />
          <Grid item xs={8}>
            <TextField
              id="outlined-size"
              label={Config.CONST_SHULIANG}
              fullWidth
              className={classes.textField}
              value={this.state.size}
              onChange={this.handleSizeChange}
              margin="normal"
              variant="outlined"
            />
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={2} />
          <Grid item xs={8}>
            <Typography variant="h6" color="secondary">
              {`${Config.CONST_NINDE}${this.state.asset}${Config.CONST_KEYONGYUE}：${this.state.available}`}
            </Typography>
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={2} />
          <Grid item xs={8}>
            <Button
              variant="contained"
              size="large"
              color="primary"
              className={classes.margin}
              onClick={this.handleSend}
            >
            <Typography variant="h6" style={{color:"#fff"}}>
              {Config.CONST_QUERENHUIKUANG}
            </Typography>
             
            </Button>
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={2} />
          <Grid item xs={8}>
            {this.state.error && (
              <Typography color="secondary" variant="h6" component="h6">
                {this.state.errorMsg}
              </Typography>
            )}
          </Grid>
        </Grid>
      </form>
    );
  }
}

UserSendCoins.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(UserSendCoins);
