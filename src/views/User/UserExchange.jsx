import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepButton from "@material-ui/core/StepButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import UserBusinessSelect from "./UserBusinessSelect.jsx";
import UserAssetTradeSize from "./UserAssetTradeSize.jsx";
import UserAssetSelector from "./UserAssetSelector.jsx";
import UserExchangeSubmit from "./UserExchangeSubmit.jsx";
import UserSubmitConfirm from "./UserSubmitConfirm.jsx";
import assetServ from "../../api/service/asset";
import Config from "../../config";

const styles = theme => ({
  root: {
    width: "90%"
  },
  button: {
    marginRight: theme.spacing.unit
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 5
  },
  font: {
    color: "white",
  },
  fontSize:{
    fontSize: 20
  }
});

function getSteps() {
  return ["选择业务", "选择币种", "确认交易数量", "提交交易"];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return "步骤 1: 选择购买或者出售LOT";
    case 1:
      return "步骤 2: 选择币种";
    case 2:
      return "步骤 3: 请输入购买或者出售数量";
    case 3:
      return "步骤 4: 提交交易";
    default:
      return "Unknown step";
  }
}

class UserExchange extends React.Component {
  state = {
    activeStep: 0,
    completed: {},
    isFinalStep: false,
    side: "1",
    amount: 0,
    buyAmount: 0,
    coin: "BTC",
    open: false
  };

  handleClickSubmit = () => {
    this.setState({ open: true }, () => {
      this.forceUpdate();
    });
  };

  onSubmit = () => {
    return assetServ.exchangeAsset(
      this.state.amount,
      this.state.coin,
      this.state.side
    );
  };

  totalSteps = () => getSteps().length;

  onSideSelect = side => {
    this.setState({ side });
  };

  onAmountInput = (amount, buyAmount) => {
    this.setState({ amount, buyAmount });
  };

  onSelectCoin = coin => {
    this.setState({ coin });
  };

  handleNext = () => {
    const activeStep = this.state.activeStep + 1;
    this.isLastStep(activeStep)
      ? this.setState({ activeStep, isFinalStep: true })
      : this.setState({ activeStep, isFinalStep: false });
  };

  handleBack = () => {
    const activeStep = this.state.activeStep - 1;
    this.isLastStep(activeStep)
      ? this.setState({ activeStep, isFinalStep: true })
      : this.setState({ activeStep, isFinalStep: false });
  };

  handleStep = step => () => {
    this.setState({
      activeStep: step
    });
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
      completed: {}
    });
  };

  isLastStep(activeStep) {
    return activeStep === this.totalSteps() - 1;
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <UserSubmitConfirm
          open={this.state.open}
          onSubmit={this.onSubmit}
          onFinishExchange={this.props.onFinishExchange}
        />
        <Stepper nonLinear activeStep={activeStep}>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton
                onClick={this.handleStep(index)}
                completed={this.state.completed[index]} 
              >
              <span className={classes.fontSize}>{label}</span>
                
              </StepButton>
            </Step>
          ))}
        </Stepper>
        <div>
          <div>
            <Typography variant="h6" className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div>
            <div style={{height:300}}>
              {this.state.activeStep === 0 && (
                <UserBusinessSelect onSideSelect={this.onSideSelect} />
              )}
              {this.state.activeStep === 1 && (
                <UserAssetSelector
                  onSelectCoin={this.onSelectCoin}
                  side={this.state.side}
                />
              )}
              {this.state.activeStep === 2 && (
                <UserAssetTradeSize
                  onAmountInput={this.onAmountInput}
                  coin={this.state.coin}
                  side={this.state.side}
                />
              )}
              {this.state.activeStep === 3 && (
                <UserExchangeSubmit
                  side={this.state.side}
                  coin={this.state.coin}
                  amount={this.state.amount}
                  buyAmount={this.state.buyAmount}
                />
              )}
              </div>
              <Button
                disabled={activeStep === 0}
                onClick={this.handleBack}
                color="secondary"
                className={classes.button}
              >
                <Typography variant="h6">
                  {Config.CONST_SHANGYIBU}
                </Typography>

              </Button>
              {!this.state.isFinalStep && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleNext}
                  className={classes.button}
                >
                  <Typography variant="h6" className={classes.font} >
                    {Config.CONST_XIAYIBU}
                  </Typography>

                </Button>
              )}
              {this.state.isFinalStep && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickSubmit}
                  className={classes.button}
                >
                  <Typography variant="h6" className={classes.font} >
                    {Config.CONST_TIJIAO}
                  </Typography>
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserExchange.propTypes = {
  classes: PropTypes.object.isRequired,
  onFinishExchange: PropTypes.func.isRequired
};

export default withStyles(styles)(UserExchange);
