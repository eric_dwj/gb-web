import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import ListAlt from "@material-ui/icons/ListAlt";
import SendIcon from "@material-ui/icons/Send";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import Typography from "@material-ui/core/Typography";
import UserAssetList from "./UserAssetList.jsx";
import UserExchange from "./UserExchange.jsx";
import UserSendCoins from "./UserSendCoins.jsx";
import Config from "../../config.js";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};
const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  font: {
    fontSize: 20
  }
});

function User(props) {
  const { classes } = props;
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function onFinishExchange() {
    setValue(0);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="secondary"
          textColor="secondary"
        >
          <Tab
            label={Config.CONST_WODEZICHAN}
            className={classes.font}
            icon={<PersonPinIcon />}
          />
          <Tab
            label={Config.CONST_DUIHUANG}
            className={classes.font}
            icon={<ListAlt />}
          />
          <Tab
            label={Config.CONST_ZHUANGZHANG}
            className={classes.font}
            icon={<SendIcon />}
          />
        </Tabs>
      </AppBar>
      {value === 0 && (
        <TabContainer>
          <UserAssetList />
        </TabContainer>
      )}
      {value === 1 && (
        <TabContainer>
          <UserExchange onFinishExchange={onFinishExchange} />
        </TabContainer>
      )}
      {value === 2 && (
        <TabContainer>
          <UserSendCoins />
        </TabContainer>
      )}
    </div>
  );
}
User.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(User);
