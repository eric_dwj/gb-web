import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { withStyles } from "@material-ui/core/styles";
import assetServ from "../../api/service/asset";
const styles = () => ({
  length: {
    width: 500
  }
});
class UserSubmitConfirm extends React.Component {
  state = {
    open: false,
    finished: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleAfterSubmit = () => {
    this.setState({ open: false }, () => {
      this.props.onFinishExchange();
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSubmit = () => {
    this.props.onSubmit().then(resp => {
      if (resp.data.state === "success") {
        assetServ.setBalance(resp.data.data.tradeAsset);
        this.setState({ finished: true });
      }
    });
  };

  UNSAFE_componentWillReceiveProps = () => {
    this.setState({ open: this.props.open });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog open={this.state.open} onClose={this.handleClose}>
          {!this.state.finished && (
            <div>
              <DialogTitle>{"请确认提交交易"}</DialogTitle>
              <DialogContent className={classes.length}>
                <DialogContentText>{`提交交易请求？`}</DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  {`取消`}
                </Button>
                <Button onClick={this.handleSubmit} color="primary" autoFocus>
                  {`确认提交`}
                </Button>
              </DialogActions>
            </div>
          )}
          {this.state.finished && (
            <div>
              <DialogTitle>{"交易提交成功"}</DialogTitle>
              <DialogContent className={classes.length}>
                <DialogContentText>{`已经完成交易，请前往确认余额`}</DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={this.handleAfterSubmit}
                  color="primary"
                  autoFocus
                >
                  {`确认`}
                </Button>
              </DialogActions>
            </div>
          )}
        </Dialog>
      </div>
    );
  }
}
UserSubmitConfirm.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onFinishExchange: PropTypes.func.isRequired
};

export default withStyles(styles)(UserSubmitConfirm);
