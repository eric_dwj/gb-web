import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    display: "flex"
  },
  formControl: {
    margin: theme.spacing.unit * 3
  },
  group: {
    margin: `${theme.spacing.unit}px 0`
  },
  font: {
    fontSize: 20
  }
});

class UserBusinessSelect extends React.Component {
  state = {
    value: "1"
  };

  handleChange = event => {
    this.props.onSideSelect(event.target.value);
    this.setState({ value: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={5} />
          <Grid item xs={5}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel
                component="legend"
                className={classes.font}
              >{`选择业务`}</FormLabel>
              <RadioGroup
                className={classes.group}
                value={this.state.value}
                onChange={this.handleChange}
              >
                <FormControlLabel
                  value="1"
                  control={<Radio />}
                  className={classes.font}
                  label={
                    <Typography variant="h6" color="inherit">
                      {`购买LOT`}
                    </Typography>
                  }
                />
                <FormControlLabel
                  value="2"
                  control={<Radio />}
                  className={classes.font}
                  label={
                    <Typography variant="h6" color="inherit">
                      {`出售LOT`}
                    </Typography>
                  }
                />
              </RadioGroup>
            </FormControl>
          </Grid>
        </Grid>
      </div>
    );
  }
}

UserBusinessSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  onSideSelect: PropTypes.func.isRequired
};

export default withStyles(styles)(UserBusinessSelect);
