import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
const styles = () => ({
  length: {
    width: 500
  }
});
class AlertDialog extends React.Component {
  handleClose = () => {
    this.props.onClose(false);
  };

  handleSend = () => {
    this.props.onSend();
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog open={this.props.open} onClose={this.handleClose}>
          <DialogTitle id="alert-dialog-title">{"请确认转账明细"}</DialogTitle>
          <DialogContent className={classes.length}>
            <Typography color="secondary" variant="h6" component="h6">
              {`用户：${this.props.sendObj.email}`}
            </Typography>
            <Typography color="secondary" variant="h6" component="h6">
              {`币种：${this.props.sendObj.coin}`}
            </Typography>
            <Typography color="secondary" variant="h6" component="h6">
              {`数量：${this.props.sendObj.size}`}
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              取消
            </Button>
            <Button onClick={this.handleSend} color="primary" autoFocus>
              确认
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSend: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  sendObj: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
