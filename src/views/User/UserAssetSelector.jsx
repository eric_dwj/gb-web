import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Select from "@material-ui/core/Select";
import assetServ from "../../api/service/asset";
import Grid from "@material-ui/core/Grid";
import Config from "../../config";
const styles = theme => ({
  button: {
    display: "block",
    marginTop: theme.spacing.unit * 2
  },
  formControl: {
    minWidth: 120,
    margin: 10
  }
});

class UserAssetSelector extends React.Component {
  state = {
    coin: "BTC",
    open: false,
    assets: null
  };

  setAssets = assets => {
    const tmpAssets = [];
    for (let index = 0; index < assets.length; index++) {
      const asset = assets[index];
      if (
        String(asset).toUpperCase() !== String(Config.LOTTER_COIN).toUpperCase()
      ) {
        tmpAssets.push(asset);
      }
    }
    this.setState({ assets: tmpAssets });
  };

  componentDidMount = () => {
    if (!window.traderConfig || !window.traderConfig.coins) {
      assetServ.traderConfig().then(() => {
        this.setAssets(window.traderConfig.coins);
      });
    } else {
      this.setAssets(window.traderConfig.coins);
    }
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.props.onSelectCoin(event.target.value);
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  render() {
    const { classes } = this.props;
    return (
      <Grid container spacing={24}>
        <Grid item xs={5} />
        <Grid item xs={5}>
          <form autoComplete="off">
            <FormControl
              required
              component="fieldset"
              className={classes.formControl}
            >
              <FormGroup>
                <FormControl variant="filled">
                  <Select
                    open={this.state.open}
                    onClose={this.handleClose}
                    onOpen={this.handleOpen}
                    value={this.state.coin}
                    onChange={this.handleChange}
                    input={
                      <OutlinedInput
                        name="coin"
                        id="open-select"
                        labelWidth={0}
                      />
                    }
                  >
                    {this.state.assets &&
                      this.state.assets.map((asset, index) => {
                        return (
                          <MenuItem key={index} value={asset}>
                            {asset}
                          </MenuItem>
                        );
                      })}
                  </Select>
                </FormControl>
              </FormGroup>
            </FormControl>
          </form>
        </Grid>
      </Grid>
    );
  }
}

UserAssetSelector.propTypes = {
  classes: PropTypes.object.isRequired,
  onSelectCoin: PropTypes.func.isRequired,
  side: PropTypes.string.isRequired
};

export default withStyles(styles)(UserAssetSelector);
