import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: 10
  },
  font: {
    fontSize: 20
  },
  listItemText: {
    fontSize: 20
  }
});
class UserAssetItem extends React.Component {
  state = {};
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <ListItem button>
          <Grid container spacing={24}>
            <Grid item xs={1} />
            <Grid item xs={2}>
              <ListItemText
                primary={this.props.coin}
                classes={{ primary: classes.listItemText }}
              />
            </Grid>
            <Grid item xs={2}>
              <ListItemText
                primary={this.props.price}
                classes={{ primary: classes.listItemText }}
              />
            </Grid>
            <Grid item xs={2}>
              <ListItemText
                primary={this.props.available}
                classes={{ primary: classes.listItemText }}
              />
            </Grid>
            <Grid item xs={2}>
              <ListItemText
                primary={this.props.frozen}
                classes={{ primary: classes.listItemText }}
              />
            </Grid>
            <Grid item xs={2}>
              <ListItemText
                primary={this.props.total}
                classes={{ primary: classes.listItemText }}
              />
            </Grid>
          </Grid>
        </ListItem>
      </div>
    );
  }
}

UserAssetItem.propTypes = {
  classes: PropTypes.object.isRequired,
  coin: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  available: PropTypes.string.isRequired,
  frozen: PropTypes.string.isRequired,
  total: PropTypes.string.isRequired
};
export default withStyles(styles)(UserAssetItem);
