import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
import tool from "../../api/util/common";
import Config from "../../config";

const styles = () => ({
  root: {
    margin: 20
  }
});

class WithdrawResult extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <SnackbarContent
          message={`${Config.CONST_JISOYIFEIYONG} 
          ${this.props.fee} ${Config.CONST_NINKEYIHUODE}
          ${tool.minus(this.props.size, this.props.fee)}`}
          color="danger"
        />
      </div>
    );
  }
}

WithdrawResult.propTypes = {
  classes: PropTypes.object.isRequired,
  fee: PropTypes.number.isRequired,
  size: PropTypes.string.isRequired
};
export default withStyles(styles)(WithdrawResult);
