import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CoinSelector from "./DepositCoinSelector.jsx";
import AddressDisplayer from "./AddressDisplayer.jsx";
import AssetTxTable from "./AssetTxTable.jsx";
import Qrcode from "./Qrcode";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import depositServ from "../../api/service/deposit";
import Config from "../../config";
import Typography from "@material-ui/core/Typography";
import socketUtil from "../../api/util/socketio";
import { EventEmitter } from "../../api/util/events";
class Deposit extends React.Component {
  state = {
    coin: Config.LOTTER_COIN,
    address: "",
    assetTx: null,
    depositCoin: "",
    confirms: "0"
  };
  _loadData = confirms => {
    const promise = depositServ.assetTx(this.state.coin);
    if (promise) {
      promise.then(resp => {
        this.setState({
          address:
            window.user.user.account[`${Config.LOTTER_COIN}Account`].address,
          assetTx: resp.data,
          confirms: confirms ? confirms : "0"
        });
      });
    }
  };
  componentDidMount = async () => {
    if (!window.traderConfig) {
      depositServ.traderConfig().then(() => {
        this.forceUpdate();
      });
    }
    if (window.user && window.user.user) {
      this._loadData();
      socketUtil.registerDepositEvent(window.user.user.userid);
      EventEmitter.subscribe(EventEmitter.DEPO_NOTIFY_EVENT, data => {
        let isRefresh = true;
        for (const tx of this.state.assetTx) {
          if (tx.txid === data.txid) {
            isRefresh = false;
          }
        }
        if (isRefresh) {
          this._loadData(data.confirms);
        } else {
          this.setState({ confirms: data.confirms });
        }
      });
    }
  };

  onSelectCoin = coin => {
    const promise = depositServ.assetTx(coin);
    if (promise) {
      promise.then(resp => {
        this.setState({
          coin,
          address: window.user.user.account[`${coin}Account`].address,
          assetTx: resp.data
        });
      });
    }
  };

  render() {
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={5}>
          <Card>
            <CardHeader color="danger" style={{ height: 50 }}>
              <Typography variant="h5" style={{ color: "#fff" }}>
                {Config.CONST_CUNBI}
              </Typography>
              <Typography variant="h6" style={{ color: "#fff" }}>
                {window.traderConfig &&
                  window.traderConfig.netWorks &&
                  `需要${window.traderConfig.netWorks[this.state.coin]} ${
                    Config.CONST_CWLQRKWCCZ
                  }`}
              </Typography>
            </CardHeader>
            <CardBody style={{ height: window.outerHeight - 50 }}>
              <GridContainer>
                <GridItem xs={12}>
                  <CoinSelector onSelectCoin={this.onSelectCoin} />
                </GridItem>
              </GridContainer>
              <AddressDisplayer
                coin={this.state.coin}
                address={this.state.address}
              />
              {this.state.address && (
                <Qrcode coin={this.state.coin} address={this.state.address} />
              )}
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={7}>
          <Card>
            <CardHeader color="danger" style={{ height: 50 }}>
              <Typography variant="h5" style={{ color: "#fff" }}>
                {Config.CONST_CUNBILISHI}
              </Typography>
              {window.traderConfig && window.traderConfig.netWorks && (
                <Typography variant="h6" style={{ color: "#fff" }}>
                  {`${Config.CONST_WANGLUOQUERENCISHU} ${this.state.confirms}/${
                    window.traderConfig.netWorks[this.state.coin]
                  } `}
                </Typography>
              )}
            </CardHeader>
            <CardBody style={{ height: window.outerHeight - 50 }}>
              {this.state.assetTx && (
                <AssetTxTable assetTx={this.state.assetTx} />
              )}
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

Deposit.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Deposit);
