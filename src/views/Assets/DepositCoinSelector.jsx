import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import Config from "../../config";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Select from "@material-ui/core/Select";
import depositServ from "../../api/service/deposit";

const styles = theme => ({
  button: {
    display: "block",
    marginTop: theme.spacing.unit * 2
  },
  formControl: {
    minWidth: 120,
    margin: 10
  },
  font: {
    fontSize: 20,
    color: "black"
  }
});

class DepositCoinSelector extends React.Component {
  state = {
    coin: Config.LOTTER_COIN,
    open: false,
    assets: null
  };

  componentDidMount = () => {
    if (!window.traderConfig || !window.traderConfig.coins) {
      depositServ.traderConfig().then(() => {
        this.setState({ assets: window.traderConfig.coins });
      });
    } else {
      this.setState({ assets: window.traderConfig.coins });
    }
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value }, () => {
      this.props.onSelectCoin(event.target.value);
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  render() {
    const { classes } = this.props;
    return (
      <form autoComplete="off">
        <br />
        <FormControl
          required
          component="fieldset"
          className={classes.formControl}
        >
          <FormLabel component="legend" className={classes.font}>
            {Config.CONST_BIZHONG}
          </FormLabel>
          <FormGroup>
            <FormControl variant="filled">
              <Select
                open={this.state.open}
                onClose={this.handleClose}
                onOpen={this.handleOpen}
                value={this.state.coin}
                onChange={this.handleChange}
                input={
                  <OutlinedInput name="coin" id="open-select" labelWidth={0} />
                }
              >
                {this.state.assets &&
                  this.state.assets.map((asset, index) => {
                    return (
                      <MenuItem key={index} value={asset}>
                        {asset}
                      </MenuItem>
                    );
                  })}
              </Select>
            </FormControl>
          </FormGroup>
        </FormControl>
        <br />
        <br />
      </form>
    );
  }
}

DepositCoinSelector.propTypes = {
  classes: PropTypes.object.isRequired,
  onSelectCoin: PropTypes.func.isRequired
};

export default withStyles(styles)(DepositCoinSelector);
