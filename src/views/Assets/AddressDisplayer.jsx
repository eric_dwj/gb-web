import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";
import depositServ from "../../api/service/deposit";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Config from "../../config";

const styles = () => ({
  root: {
    margin: 10
  },
  formControl: {
    width: 450
  }
});

class AddressDisplayer extends React.Component {
  state = {
    coin: "",
    open: false,
    assets: null,
    copied: false
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <FormControl
          required
          component="fieldset"
          className={classes.formControl}
        >
          <FormLabel component="legend" style={{ fontSize: 20, color:"black" }}>{`${
            this.props.coin
          }${Config.CONST_CUNBIDIZHI}`}</FormLabel>
          <FormGroup>
            <TextField variant="outlined" value={this.props.address} disabled />
          </FormGroup>
        </FormControl>

        <CopyToClipboard
          text={this.props.address}
          onCopy={() =>
            this.setState({ copied: true }, async () => {
              await depositServ.sleep(2000);
              this.setState({ copied: false });
            })
          }
        >
          <Chip
            label={this.state.copied ?  Config.CONST_YIFUZHI : Config.CONST_FUZHI }
            style={{ marginTop: 50, marginLeft: 10 }}
            onClick={this.handleCopy}
            className={classes.chip}
          />
        </CopyToClipboard>
      </div>
    );
  }
}

AddressDisplayer.propTypes = {
  classes: PropTypes.object.isRequired,
  coin: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired
};

export default withStyles(styles)(AddressDisplayer);
