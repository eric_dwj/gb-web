import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import PersonIcon from "@material-ui/icons/Person";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import blue from "@material-ui/core/colors/blue";
import Grid from "@material-ui/core/Grid";
import withdrawServ from "../../api/service/withdraw";
import Typography from "@material-ui/core/Typography";
import Config from "../../config";
let addresses = [];
const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },
  font: {
    fontSize: 20,
    color: "black"
  }
};

class AddressesDialog extends React.Component {
  handleClose = () => {
    this.props.onClose(this.props.selectedaddress);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog
        onClose={this.handleClose}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <DialogTitle id="simple-dialog-title">选择历史账号</DialogTitle>
        <div>
          <List>
            {addresses.map(address => (
              <ListItem
                button
                onClick={() => this.handleListItemClick(address)}
                key={address}
              >
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={address} />
              </ListItem>
            ))}
          </List>
        </div>
      </Dialog>
    );
  }
}

AddressesDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedaddress: PropTypes.string
};

const AddressesDialogWrapped = withStyles(styles)(AddressesDialog);

class WithdrawHistoricalAddress extends React.Component {
  state = {
    open: false,
    selectedaddress: ""
  };

  handleClickOpen = () => {
    withdrawServ.historyAddress(this.props.coin).then(resp => {
      if (resp.data && resp.data) {
        addresses = resp.data;
        if (addresses && addresses.length > 0) {
          this.setState({
            open: true
          });
        }
      }
    });
  };

  handleClose = value => {
    this.props.onAddressChange(String(value));
    this.setState({ selectedaddress: String(value), open: false });
  };

  handleChange = event => {
    this.props.onAddressChange(String(event.target.value));
    this.setState({ selectedaddress: String(event.target.value) });
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={{ margin: 20 }}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <FormControl
              required
              component="fieldset"
              style={{ minWidth: 500 }}
            >
              <FormLabel component="legend" className={classes.font}>{`${
                this.props.coin
              }${Config.CONST_QUBIDIZHI}`}</FormLabel>
              <FormGroup>
                <TextField
                  id="outlined-bare"
                  variant="outlined"
                  onChange={this.handleChange}
                  value={this.state.selectedaddress}
                />
              </FormGroup>
            </FormControl>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleClickOpen}
            >
              <Typography variant="h6" style={{ color: "#fff" }} align="center">
                {Config.CONST_LISHIZHANGHAO}
              </Typography>
            </Button>
            <AddressesDialogWrapped
              selectedaddress={this.state.selectedaddress}
              open={this.state.open}
              onClose={this.handleClose}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
WithdrawHistoricalAddress.propTypes = {
  onAddressChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  coin: PropTypes.string.isRequired
};
export default withStyles(styles)(WithdrawHistoricalAddress);
