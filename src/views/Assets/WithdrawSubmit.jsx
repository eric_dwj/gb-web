import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Config from "../../config";

const styles = () => ({
  root: {
    margin: 20
  }
});

class WithdrawSubmit extends React.Component {
  handleSubmit = () => {
    this.props.onSubmit();
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button
          onClick={this.handleSubmit}
          variant="contained"
          size="large"
          color="primary"
        >
          <Typography variant="h6" style={{ color: "#fff" }} align="center">
            {Config.CONST_TIJIAO}
          </Typography>
        </Button>
      </div>
    );
  }
}

WithdrawSubmit.propTypes = {
  classes: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  coin: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  withdrawSize: PropTypes.string.isRequired
};
export default withStyles(styles)(WithdrawSubmit);
