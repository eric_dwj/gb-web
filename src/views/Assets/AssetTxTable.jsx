import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Config from "../../config";
const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 400
  },
  font: {
    fontSize: 20
  },
  fontCell: {
    fontSize: 15
  }
});

class AssetTxTable extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell align="center" className={classes.font}>
                {Config.CONST_BIZHONG}
              </TableCell>
              <TableCell align="center" className={classes.font}>
                {Config.CONST_SHULIANG}
              </TableCell>
              <TableCell align="center" className={classes.font}>
                {Config.CONST_STATUS}
              </TableCell>
              <TableCell align="center" className={classes.font}>
                {Config.CONST_RIQI}
              </TableCell>
              <TableCell
                align="center"
                className={classes.font}
              >{`TXID`}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.assetTx.map(row => (
              <TableRow key={row.ts}>
                <TableCell align="center" className={classes.fontCell}>
                  {row.coin}
                </TableCell>
                <TableCell align="center" className={classes.fontCell}>
                  {row.value}
                </TableCell>
                <TableCell align="center" className={classes.fontCell}>
                  {row.status === Config.CONST_COMPLETED ||
                  row.status === Config.CONST_CHARGED
                    ? Config.CONST_YIQUEREN
                    : Config.CONST_YITIJIAO}
                </TableCell>
                <TableCell align="center" className={classes.fontCell}>
                  {row.ts}
                </TableCell>
                <TableCell
                  align="center"
                  className={classes.fontCell}
                >{`TXID`}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

AssetTxTable.propTypes = {
  classes: PropTypes.object.isRequired,
  assetTx: PropTypes.array.isRequired
};

export default withStyles(styles)(AssetTxTable);
