import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import depositServ from "../../api/service/deposit";

const styles = () => ({
  card: {
    maxWidth: 300,
    marginTop: 20
  }
});
class Qrcode extends React.Component {
  state = {
    qrcodeImage: ""
  };

  fetchQRcode = () => {
    if (this.props.address) {
      depositServ.qrcode(this.props.address).then(rqcode => {
        this.setState({ qrcodeImage: rqcode });
      });
    }
  };

  componentDidMount = () => {
    this.fetchQRcode();
  };

  UNSAFE_componentWillReceiveProps = () => {
    this.fetchQRcode();
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.state.qrcodeImage && (
          <Grid container spacing={24}>
            <Grid item xs={3} />
            <Grid item xs={6}>
              <Card className={classes.card}>
                <CardMedia
                  component="img"
                  className={classes.media}
                  image={this.state.qrcodeImage}
                  title="qrcode"
                />
              </Card>
            </Grid>
          </Grid>
        )}
      </div>
    );
  }
}
Qrcode.propTypes = {
  classes: PropTypes.object.isRequired,
  coin: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired
};

export default withStyles(styles)(Qrcode);
