import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import Config from "../../config";

const styles = () => ({
  root: {
    margin: 20
  },
  font: {
    fontSize: 20,
    color: "black"
  }
});

class WithdrawSize extends React.Component {
  handleChange = e => {
    this.props.onSizeInput(e.target.value);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <FormControl required component="fieldset">
          <FormLabel component="legend" className={classes.font}>
            {Config.CONST_SHULIANG}
          </FormLabel>
          <FormGroup>
            <TextField
              id="outlined-bare"
              variant="outlined"
              onChange={this.handleChange}
            />
          </FormGroup>
        </FormControl>
      </div>
    );
  }
}

WithdrawSize.propTypes = {
  classes: PropTypes.object.isRequired,
  onSizeInput: PropTypes.func.isRequired
};
export default withStyles(styles)(WithdrawSize);
