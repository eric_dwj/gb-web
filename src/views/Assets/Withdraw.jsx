import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CoinSelector from "./WithdrawCoinSelector.jsx";
import WithdrawHistoricalAddress from "./WithdrawHistoricalAddress.jsx";
import WithdrawResult from "./WithdrawResult.jsx";
import WithdrawSize from "./WithdrawSize.jsx";
import WithdrawSubmit from "./WithdrawSubmit.jsx";
import AssetTxTable from "./AssetTxTable.jsx";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import withdrawServ from "../../api/service/withdraw";
import tool from "../../api/util/common";
import Config from "../../config";

class Withdraw extends React.Component {
  state = {
    coin: Config.LOTTER_COIN,
    address: "",
    withdrawSize: "0",
    fee: 0,
    assetTx: null
  };

  componentDidMount = () => {
    if (window.user && window.user.user) {
      const promise = withdrawServ.assetTx(this.state.coin);
      if (promise) {
        promise.then(resp => {
          this.setState({
            coin: Config.LOTTER_COIN,
            assetTx: resp.data,
            fee: window.traderConfig.withdrawFee[this.state.coin]
          });
        });
      }
    }
  };

  onSizeInput = withdrawSize => {
    this.setState({ withdrawSize });
  };

  onSubmit = () => {
    const { address, withdrawSize, fee, coin } = this.state;
    withdrawServ
      .submit(address, tool.minus(withdrawSize, fee), coin)
      .then(resp => {
        console.log(resp);
      });
  };

  onSelectCoin = coin => {
    const promise = withdrawServ.assetTx(coin);
    if (promise) {
      promise.then(resp => {
        this.setState({
          coin,
          assetTx: resp.data,
          fee: window.traderConfig.withdrawFee[coin]
        });
      });
    }
  };

  onAddressChange = address => {
    this.setState({ address });
  };

  render() {
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={5}>
            <Card>
              <CardHeader color="danger" style={{ height: 50 }}>
                <Typography variant="h5" style={{ color: "#fff" }}>
                  {Config.CONST_QUBI}
                </Typography>
              </CardHeader>
              <CardBody style={{ height: window.outerHeight - 50 }}>
                <CoinSelector onSelectCoin={this.onSelectCoin} />
                {this.state.coin && (
                  <WithdrawHistoricalAddress
                    coin={this.state.coin}
                    onAddressChange={this.onAddressChange}
                  />
                )}
                <WithdrawSize onSizeInput={this.onSizeInput} />
                {parseFloat(this.state.withdrawSize) >=
                  parseFloat(this.state.fee) && (
                  <WithdrawResult
                    fee={this.state.fee}
                    size={this.state.withdrawSize}
                  />
                )}
                <WithdrawSubmit
                  onSubmit={this.onSubmit}
                  coin={this.state.coin}
                  address={this.state.address}
                  withdrawSize={this.state.withdrawSize}
                />
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={7}>
            <Card>
              <CardHeader color="danger" style={{ height: 50 }}>
                <Typography variant="h5" style={{ color: "#fff" }}>
                  {Config.CONST_QUBILISHI}
                </Typography>
              </CardHeader>
              <CardBody style={{ height: window.outerHeight - 50 }}>
                {this.state.assetTx && (
                  <AssetTxTable assetTx={this.state.assetTx} />
                )}
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Withdraw.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Withdraw);
