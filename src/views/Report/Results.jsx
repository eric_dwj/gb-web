import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Chip from "@material-ui/core/Chip";
import red from "@material-ui/core/colors/red";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import ResultNumberRow from "./ResultNumberRow.jsx";
import Search from "./Search.jsx";
import resultServ from "../../api/service/result";
import CircularProgress from "@material-ui/core/CircularProgress";
import Config from "../../config";
import ResultDetail from "./ResultDetail";
const styles = () => {
  return {
    loading: {
      marginTop: 20,
      marginLeft: "45%"
    },
    font: {
      color: "white"
    },
    kl8: {
      height: 90,
      marginTop: 20
    },
    kl8gap: {
      marginBottom: 10
    },
    padding: {
      paddig: 0
    },
    cellFont: {
      fontSize: 20,
      padding: 0
    },
    root: {
      overflowY: "auto"
    },
    sumInfo: {
      marginRight: 5,
      fontSize: 20,
      color: "white",
      backgroundColor: red[500]
    }
  };
};
const StyledTableCell = withStyles(() => {
  return {
    head: {
      backgroundColor: "#f44336",
      padding: 5
    },
    body: {
      fontSize: 20
    }
  };
})(TableCell);

class Results extends React.Component {
  state = {
    result: [],
    loading: false,
    business: "",
    title: ""
  };

  componentDidMount = () => {
    this.setState({ title: Config.CONST_KAIJIANGJIEGUO });
  };
  updateSearchData = (result, business) => {
    this.setState({ result, business, loading: false });
  };
  updateStatus = loading => {
    this.setState({ loading });
  };
  _processResult = (value, classes) => {
    let result = [];
    const resultInfo = JSON.parse(value.result_info);
    if (resultInfo && Array.isArray(resultInfo.Sum)) {
      for (let index = 0; index < resultInfo.Sum.length; index++) {
        const data = resultInfo.Sum[index];
        result.push(
          <Chip
            key={index}
            label={data}
            color="secondary"
            className={classes.sumInfo}
          />
        );
      }
    }
    return result;
  };
  _rowNumber = (value, classes) => {
    if (this.state.business.indexOf(Config.CONST_LHC) !== -1) {
      return <ResultNumberRow number={value.number} type={Config.CONST_LHC} />;
    } else if (this.state.business.indexOf(Config.CONST_SSC) !== -1) {
      return <ResultNumberRow number={value.number} type={Config.CONST_SSC} />;
    } else if (this.state.business.indexOf(Config.CONST_SC) !== -1) {
      return <ResultNumberRow number={value.number} type={Config.CONST_SC} />;
    } else if (this.state.business.indexOf(Config.CONST_K3) !== -1) {
      return <ResultNumberRow number={value.number} type={Config.CONST_K3} />;
    } else if (this.state.business.indexOf(Config.CONST_KL8) !== -1) {
      const number1 = value.number
        .split(",")
        .slice(0, 10)
        .join(",");
      const number2 = value.number
        .split(",")
        .slice(10, 20)
        .join(",");
      return (
        <div className={classes.kl8}>
          <ResultNumberRow number={number1} type={Config.CONST_KL8} />
          <ResultNumberRow number={number2} type={Config.CONST_KL8} />
        </div>
      );
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Search
          title={this.state.title}
          updateSearchData={this.updateSearchData}
          updateStatus={this.updateStatus}
          loading={this.state.loading}
        />

        <Card className={classes.root}>
          <CardBody style={{ maxHeight: window.outerHeight - 50 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <StyledTableCell>
                    <Typography
                      variant="h6"
                      align="left"
                      className={classes.font}
                    >
                      {Config.CONST_QIHAO}
                    </Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography
                      variant="h6"
                      align="left"
                      className={classes.font}
                    >
                      {Config.CONST_KAIJIANGSHIJIAN}
                    </Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography
                      variant="h6"
                      align="left"
                      className={classes.font}
                    >
                      {Config.CONST_KAICHUHAOMA}
                    </Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography
                      variant="h6"
                      align="left"
                      className={classes.font}
                    >
                      {Config.CONST_KAIJIANGZHONGHE}
                    </Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography
                      variant="h6"
                      align="left"
                      className={classes.font}
                    >
                      {Config.CONST_KAIJIANGXIANGQING}
                    </Typography>
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.loading && (
                  <TableRow>
                    <TableCell colSpan={5}>
                      <div className={classes.loading}>
                        <Typography variant="h6">
                          {Config.CONST_JIAZAIZHONG}
                        </Typography>
                        <CircularProgress disableShrink />
                      </div>
                    </TableCell>
                  </TableRow>
                )}
                {!this.state.loading &&
                  this.state.result.map((value, index) => {
                    return (
                      <TableRow key={index}>
                        <StyledTableCell className={classes.cellFont}>
                          {value.round}
                        </StyledTableCell>
                        <StyledTableCell className={classes.cellFont}>
                          {resultServ.covertTs2YYYYMMSSHHMMSS(value.ts)}
                        </StyledTableCell>
                        <StyledTableCell className={classes.cellFont}>
                          {this._rowNumber(value, classes)}
                        </StyledTableCell>
                        <StyledTableCell className={classes.cellFont}>
                          {this._processResult(value, classes)}
                        </StyledTableCell>
                        <StyledTableCell className={classes.cellFont}>
                          <ResultDetail
                            round={value.round}
                            ts={value.ts}
                            business={value.business}
                            resultInfo={value.result_info}
                            number={value.number}
                          />
                        </StyledTableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}
Results.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Results);
