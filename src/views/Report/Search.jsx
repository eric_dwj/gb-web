import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { withStyles } from "@material-ui/core/styles";
import ListIcon from "@material-ui/icons/List";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import AddAlert from "@material-ui/icons/AddAlert";
import Button from "@material-ui/core/Button";
import settle from "../../api/service/settle";
import withdrawSev from "../../api/service/withdraw";
import note from "../../api/service/note";
import resultServ from "../../api/service/result";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import Config from "../../config";

const BootstrapInput = withStyles(theme => ({
  root: {
    "label + &": {
      marginTop: theme.spacing.unit * 3
    }
  },

  input: {
    borderRadius: 4,
    position: "relative",
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "auto",
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
    }
  }
}))(InputBase);

const styles = theme => ({
  root: {
    width: "100%"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: 20
  },
  searchIcon: {
    width: theme.spacing.unit * 7,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  margin: {
    marginLeft: 20,
    marginRight: 20
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%"
  }
});

class Search extends React.Component {
  const_type_settlement = "type_settlement";
  const_type_dashboard = "type_dashboard";
  state = {
    business: "",
    coin: null,
    isNormal: true,
    isDataRange: false,
    type: null,
    oper: ""
  };
  componentDidMount = () => {
    if (window.location.pathname === withdrawSev.getDashboardURI()) {
      this.setState({
        isNormal: false,
        isDataRange: true,
        type: this.const_type_dashboard,
        from: withdrawSev.today(),
        to: withdrawSev.tomorrow(),
        coin: this.props.coins[0],
        oper: this.props.withdrawTabs[0].oper
      });
    } else if (window.location.pathname === settle.getReportURI()) {
      this.setState({
        isNormal: false,
        isDataRange: true,
        type: this.const_type_settlement,
        from: settle.today(),
        to: settle.tomorrow()
      });
    } else if (
      window.location.pathname === settle.getTodaySettlementURI() ||
      window.location.pathname === resultServ.getResultsURI()
    ) {
      this.setState({
        isNormal: true,
        isDataRange: false,
        type: this.const_type_settlement,
        date: settle.today()
      });
    } else if (window.location.pathname === settle.getUnsettlementURI()) {
      this.setState({
        isNormal: false,
        type: this.const_type_settlement,
        isDataRange: false
      });
    }
  };
  handleChange = event => {
    if (this.state.type === this.const_type_settlement) {
      this.setState({ business: event.target.value });
    } else if (this.state.type === this.const_type_dashboard) {
      this.setState({ coin: event.target.value });
    }
  };
  handleStatusChange = event => {
    this.setState({ oper: event.target.value });
  };

  showNotification = place => {
    var x = [];
    x[place] = true;
    this.setState(x);
    this.alertTimeout = setTimeout(
      function() {
        x[place] = false;
        this.setState(x);
      }.bind(this),
      6000
    );
  };

  handleSearch = () => {
    if (
      !this.state.business &&
      this.state.type === this.const_type_settlement
    ) {
      this.showNotification("tr");
      return;
    }

    this.props.updateStatus(true);
    if (window.location.pathname === withdrawSev.getDashboardURI()) {
      withdrawSev
        .listWithdrawTx(
          this.state.coin,
          this.state.from,
          this.state.to,
          this.state.oper
        )
        .then(resps => {
          this.props.updateSearchData(
            resps.data,
            this.state.coin,
            this.state.oper
          );
        })
        .catch(() => {
          this.props.updateStatus(false);
        });
    } else if (window.location.pathname === resultServ.getResultsURI()) {
      resultServ
        .listResult(this.state.business, this.state.date)
        .then(resps => {
          this.props.updateSearchData(resps.data.data, this.state.business);
        })
        .catch(() => {
          this.props.updateStatus(false);
        });
    } else if (window.location.pathname === settle.getUnsettlementURI()) {
      note
        .getPendingOrders(this.state.business)
        .then(resps => {
          this.props.updateSearchData(resps.data.data);
        })
        .catch(() => {
          this.props.updateStatus(false);
        });
    } else if (window.location.pathname === settle.getTodaySettlementURI()) {
      note
        .getTodaySettled(this.state.business, this.state.date)
        .then(resps => {
          this.props.updateSearchData(resps.data.data);
        })
        .catch(() => {
          this.props.updateStatus(false);
        });
    } else if (window.location.pathname === settle.getReportURI()) {
      note
        .getOrderHistory(this.state.business, this.state.from, this.state.to)
        .then(resps => {
          this.props.updateSearchData(resps.data.data);
        })
        .catch(() => {
          this.props.updateStatus(false);
        });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Snackbar
          place="tr"
          color="warning"
          icon={AddAlert}
          message={Config.CONST_MSG_QXZCP}
          open={this.state.tr}
          closeNotification={() => this.setState({ tr: false })}
          close
        />
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Open drawer"
            >
              <ListIcon />
            </IconButton>
            <Typography
              className={classes.title}
              variant="h6"
              color="inherit"
              noWrap
            >
              {`${this.props.title}${Config.CONST_CHAXUN}:`}
            </Typography>
            {this.state.coin && this.state.type === this.const_type_dashboard && (
              <FormControl className={classes.margin}>
                <NativeSelect
                  style={{ color: "white" }}
                  value={this.state.coin}
                  onChange={this.handleChange}
                  input={<BootstrapInput />}
                >
                  {this.props.coins.map((coin, index) => {
                    return (
                      <option
                        style={{ color: "black" }}
                        key={index}
                        value={coin}
                      >
                        {coin}
                      </option>
                    );
                  })}
                </NativeSelect>
              </FormControl>
            )}
            {this.state.oper && this.state.type === this.const_type_dashboard && (
              <FormControl className={classes.margin}>
                <NativeSelect
                  style={{ color: "white" }}
                  value={this.state.oper}
                  onChange={this.handleStatusChange}
                  input={
                    <BootstrapInput
                      name="status"
                      id="status-customized-native-simple"
                    />
                  }
                >
                  {this.props.withdrawTabs.map((tab, index) => {
                    return (
                      <option
                        style={{ color: "black" }}
                        key={index}
                        value={tab.oper}
                      >
                        {tab.name}
                      </option>
                    );
                  })}
                </NativeSelect>
              </FormControl>
            )}
            {this.state.type === this.const_type_settlement && (
              <FormControl className={classes.margin}>
                <NativeSelect
                  style={{ color: "white" }}
                  value={this.state.business}
                  onChange={this.handleChange}
                  input={
                    <BootstrapInput
                      name="age"
                      id="age-customized-native-simple"
                    />
                  }
                >
                  <option style={{ color: "black" }} value={0}>
                    {Config.CONST_SLASH_CAIPIAO}
                  </option>
                  {window.businesses &&
                    window.businesses.map((business, index) => {
                      return (
                        <option
                          style={{ color: "black" }}
                          key={index}
                          value={business.business}
                        >
                          {business.name}
                        </option>
                      );
                    })}
                </NativeSelect>
              </FormControl>
            )}

            {this.state.isNormal && (
              <div className={classes.search}>
                <InputBase
                  type="date"
                  defaultValue={settle.today()}
                  placeholder="Search…"
                  onChange={e => {
                    this.setState({ date: e.target.value });
                  }}
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                />
              </div>
            )}

            {this.state.isDataRange && (
              <div className={classes.search}>
                <div className={classes.searchIcon}>{`FROM`}</div>
                <InputBase
                  type="date"
                  defaultValue={settle.today()}
                  placeholder="From"
                  onChange={e => {
                    this.setState({ from: e.target.value });
                  }}
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                />
              </div>
            )}

            {this.state.isDataRange && (
              <div className={classes.search}>
                <div className={classes.searchIcon}>{`TO`}</div>
                <InputBase
                  type="date"
                  defaultValue={settle.tomorrow()}
                  placeholder="To"
                  onChange={e => {
                    this.setState({ to: e.target.value });
                  }}
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                />
              </div>
            )}

            <Button
              onClick={this.handleSearch}
              variant="contained"
              color="primary"
              disabled={this.props.loading}
            >
              <Typography variant="h6" style={{ color: "#fff" }} align="center">
                {Config.CONST_CHAXUN}
              </Typography>
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  updateSearchData: PropTypes.func.isRequired,
  updateStatus: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  coins: PropTypes.array,
  withdrawTabs: PropTypes.array
};

export default withStyles(styles)(Search);
