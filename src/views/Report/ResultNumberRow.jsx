import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import GamesImg from "../Games/GamesImg";
import Config from "../../config";
import lhcUtil from "../../api/util/lhcUtil";
const styles = () => {
  return {
    font: {
      color: "white"
    },
    blueDeep: {
      margin: 5,
      color: "#fff",
      backgroundColor: "#1976d2",
      fontSize: 20
    },
    blueLight: {
      margin: 5,
      color: "#fff",
      backgroundColor: "#83d0f2",
      fontSize: 20
    },
    blue: {
      margin: 5,
      backgroundColor: "#00bfff"
    },
    red: {
      margin: 5,
      backgroundColor: "#ff3300"
    },
    green: {
      margin: 5,
      backgroundColor: "#006400"
    }
  };
};
class ResultNumberRow extends React.Component {
  _mapLHCNumber = (value, index, classes) => {
    return (
      <Avatar key={index} className={classes[lhcUtil.numBose(value)]}>
        {value}
      </Avatar>
    );
  };
  _mapKL8LastRoundImg = (value, index, classes) => {
    if (parseInt(value) <= 40) {
      return (
        <Avatar key={index} className={classes.blueLight}>
          {value}
        </Avatar>
      );
    } else {
      return (
        <Avatar key={index} className={classes.blueDeep}>
          {value}
        </Avatar>
      );
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.props.type === Config.CONST_SSC && (
          <Grid container spacing={24}>
            {this.props.number.split(",").map((value, index) => {
              return (
                <Grid item xs={1} key={index}>
                  <GamesImg type={this.props.type} number={value} />
                </Grid>
              );
            })}
          </Grid>
        )}
        {this.props.type === Config.CONST_SC && (
          <Grid container spacing={16}>
            {this.props.number.split(",").map((value, index) => {
              return (
                <Grid item xs={1} key={index}>
                  <GamesImg type={this.props.type} number={value} />
                </Grid>
              );
            })}
          </Grid>
        )}
        {this.props.type === Config.CONST_K3 && (
          <Grid container spacing={24}>
            {this.props.number.split(",").map((value, index) => {
              return (
                <Grid item xs={2} key={index}>
                  <GamesImg type={this.props.type} number={value} />
                </Grid>
              );
            })}
          </Grid>
        )}
        {this.props.type === Config.CONST_KL8 && (
          <Grid container spacing={16}>
            {this.props.number.split(",").map((value, index) => {
              return this._mapKL8LastRoundImg(value, index, classes);
            })}
          </Grid>
        )}
        {this.props.type === Config.CONST_LHC && (
          <Grid container spacing={16}>
            {this.props.number.split(",").map((value, index) => {
              return this._mapLHCNumber(value, index, classes);
            })}
          </Grid>
        )}
      </div>
    );
  }
}
ResultNumberRow.propTypes = {
  classes: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired
};
export default withStyles(styles)(ResultNumberRow);
