import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Search from "./Search.jsx";
import settle from "../../api/service/settle";
import Config from "../../config";
const styles = () => {
  return {
    loading: {
      marginTop: 20,
      marginLeft: "45%"
    }
  };
};
class Settlement extends React.Component {
  state = {
    settlement: [],
    loading: false,
    title: ""
  };

  componentDidMount = () => {
    if (window.location.pathname === settle.getUnsettlementURI()) {
      this.setState({ title: Config.CONST_WEIJIEMINGXI });
    } else if (window.location.pathname === settle.getTodaySettlementURI()) {
      this.setState({ title: Config.CONST_JINGRIYIJIE });
    } else if (window.location.pathname === settle.getReportURI()) {
      this.setState({ title: Config.CONST_BAOBIAOCHAXUN });
    }
  };
  updateStatus = loading => {
    this.setState({ loading });
  };
  updateSearchData = settlement => {
    if (Array.isArray(settlement)) {
      const localSettlement = [];
      for (let index = 0; index < settlement.length; index++) {
        const jsonOrder = settlement[index];
        const arrayOrder = [
          jsonOrder.gb_order_id,
          jsonOrder.round,
          jsonOrder.type,
          jsonOrder.note_size,
          jsonOrder.times,
          jsonOrder.result,
          jsonOrder.win_size,
          settle.covertTs2YYYYMMSSHHMMSS(jsonOrder.ts)
        ];
        localSettlement.push(arrayOrder);
      }
      this.setState({ settlement: localSettlement, loading: false });
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Search
            title={this.state.title}
            updateSearchData={this.updateSearchData}
            updateStatus={this.updateStatus}
            loading={this.state.loading}
          />
        </Grid>
        <Card>
          <CardHeader color="danger">
            <h4>{this.state.title}</h4>
          </CardHeader>
          <CardBody>
            {this.state.loading && (
              <div className={classes.loading}>
                <Typography variant="h6">{Config.CONST_JIAZAIZHONG}</Typography>
                <CircularProgress disableShrink />
              </div>
            )}
            <Table
              tableHeaderColor="primary"
              tableHead={[
                Config.CONST_ZHUDANHAO,
                Config.CONST_QIHAO,
                Config.CONST_PINGXIANG,
                Config.CONST_JINGE,
                Config.CONST_PEILV,
                Config.CONST_ZHONGJIANG,
                Config.CONST_ZHONGJIANGJINGE,
                Config.CONST_SHIJIAN
              ]}
              tableData={this.state.settlement}
            />
          </CardBody>
        </Card>
      </Grid>
    );
  }
}
Settlement.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Settlement);
