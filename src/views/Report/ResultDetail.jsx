import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import red from "@material-ui/core/colors/red";
import ResultNumberRow from "./ResultNumberRow.jsx";
import resultServ from "../../api/service/result";
import Config from "../../config";
const styles = () => {
  return {
    font: {
      fontSize: 20
    },
    kl8: {
      height: 90,
      marginTop: 20
    },
    kl8gap: {
      marginBottom: 10
    },
    sumInfo: {
      marginRight: 5,
      fontSize: 20,
      color: "white",
      backgroundColor: red[500]
    }
  };
};

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing.unit,
    top: theme.spacing.unit,
    color: theme.palette.grey[500]
  }
}))(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="Close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2
  }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit
  }
}))(MuiDialogActions);

class ResultDetail extends React.Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  _rowNumber = classes => {
    if (this.props.business.indexOf(Config.CONST_LHC) !== -1) {
      return (
        <ResultNumberRow number={this.props.number} type={Config.CONST_LHC} />
      );
    } else if (this.props.business.indexOf(Config.CONST_SSC) !== -1) {
      return (
        <ResultNumberRow number={this.props.number} type={Config.CONST_SSC} />
      );
    } else if (this.props.business.indexOf(Config.CONST_SC) !== -1) {
      return (
        <ResultNumberRow number={this.props.number} type={Config.CONST_SC} />
      );
    } else if (this.props.business.indexOf(Config.CONST_K3) !== -1) {
      return (
        <ResultNumberRow number={this.props.number} type={Config.CONST_K3} />
      );
    } else if (this.props.business.indexOf(Config.CONST_KL8) !== -1) {
      const number1 = this.props.number
        .split(",")
        .slice(0, 10)
        .join(",");
      const number2 = this.props.number
        .split(",")
        .slice(10, 20)
        .join(",");
      return (
        <div className={classes.kl8}>
          <ResultNumberRow number={number1} type={Config.CONST_KL8} />
          <ResultNumberRow number={number2} type={Config.CONST_KL8} />
        </div>
      );
    }
  };

  _processResult = classes => {
    let result = [];
    const resultInfo = JSON.parse(this.props.resultInfo);
    if (resultInfo && resultInfo.Sum) {
      result.push(
        <Typography key={`Sum`} variant="h5">
          {Config.CONST_ZH}:
        </Typography>
      );
      for (let index = 0; index < resultInfo.Sum.length; index++) {
        const data = resultInfo.Sum[index];
        result.push(
          <Chip
            key={`SUM_${index}`}
            label={data}
            color="secondary"
            className={classes.sumInfo}
          />
        );
      }
    }

    if (resultInfo && resultInfo.BigOrSmall) {
      result.push(
        <Typography key={`BIGORSMALL`} variant="h5">
          {Config.CONST_DAXIAO}:
        </Typography>
      );
      for (let index = 0; index < resultInfo.BigOrSmall.length; index++) {
        const data = resultInfo.BigOrSmall[index];
        result.push(
          <Chip
            key={`BIGORSMALL_${index}`}
            label={data}
            color="secondary"
            className={classes.sumInfo}
          />
        );
      }
    }

    if (resultInfo && resultInfo.EvenOrSmall) {
      result.push(
        <Typography key={`EvenOrSmall`} variant="h5">
          {Config.CONST_DANSHUANG}:
        </Typography>
      );
      for (let index = 0; index < resultInfo.EvenOrSmall.length; index++) {
        const data = resultInfo.EvenOrSmall[index];
        result.push(
          <Chip
            key={`EvenOrSmall_${index}`}
            label={data}
            color="secondary"
            className={classes.sumInfo}
          />
        );
      }
    }
    return result;
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button
          onClick={this.handleClickOpen}
          variant="contained"
          color="primary"
        >
          <Typography variant="h6" style={{ color: "#fff" }} align="center">
            {Config.CONST_CHAKANXIANGQI}
          </Typography>
        </Button>
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="customized-result-detail"
          open={this.state.open}
        >
          <DialogTitle id="customized-result-detail" onClose={this.handleClose}>
            <Typography gutterBottom className={classes.font}>
              {`${Config.CONST_QIHAO}:${this.props.round}`}
            </Typography>
            <Typography gutterBottom className={classes.font}>
              {`${Config.CONST_SHIJIAN}:${resultServ.covertTs2YYYYMMSSHHMMSS(
                this.props.ts
              )}`}
            </Typography>
          </DialogTitle>
          <DialogContent>
            {this.props.number && this._rowNumber(classes)}
            {this.props.resultInfo && this._processResult(classes)}
            <Typography gutterBottom className={classes.font}>
              {Config.CONST_MSG_KJGZXWKF}
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              {Config.CONST_GUANBI}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
ResultDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  resultInfo: PropTypes.string.isRequired,
  round: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired,
  business: PropTypes.string.isRequired,
  ts: PropTypes.string.isRequired
};
export default withStyles(styles)(ResultDetail);
