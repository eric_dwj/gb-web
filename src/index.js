import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import onStart from "./api/service/onStart";

// core components
import Admin from "layouts/Admin.jsx";
import "assets/css/material-dashboard-react.css?v=1.6.0";

const hist = createBrowserHistory();

onStart.loadBefore().then(() => {
  ReactDOM.render(
    <Router history={hist}>
      <Switch>
        <Route path="/games" component={Admin} />
        <Redirect from="/" to="/games/games" />
      </Switch>
    </Router>,
    document.getElementById("root")
  );
});

onStart.loadAfter();
