import io from "socket.io-client/dist/socket.io";
import config from "../../config";
import { EventEmitter } from "../util/events";

const socketio = () => {};
socketio.socket = io(config.socket_io.conection, {
  secure: true,
  transports: ["websocket"]
});
socketio.registerFinalTimerEvent = (event, cb) => {
  socketio.socket.on(`${config.socket_io.timer_final_event}_${event}`, msg => {
    cb(msg);
  });
};

socketio.unregisterFinalTimerEvent = (event, cb) => {
  socketio.socket.removeListener(
    `${config.socket_io.timer_final_event}_${event}`,
    msg => {
      cb(msg);
    }
  );
};

socketio.registerDepositEvent = userid => {
  socketio.socket.on(userid, data => {
    EventEmitter.dispatch(EventEmitter.DEPO_NOTIFY_EVENT, data);
  });
};

export default socketio;
