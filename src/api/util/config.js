import conf from "../../config";
import rpc from "./rpc";
const config = () => {};

config.listGameTags = () => {
  try {
    const uri = conf.uri.gb_server + conf.uri.public;
    return rpc.post(uri, { id: 8, method: "gamesTag", params: [] });
  } catch (error) {
    throw new Error("get listGameTags error");
  }
};

config.loadResource = () => {
  try {
    const uri = conf.uri.gb_server + conf.uri.public;
    return rpc.post(uri, { id: 8, method: "webResource", params: [] });
  } catch (error) {
    throw new Error("get loadResource error");
  }
};

config.listGames = () => {
  try {
    const uri = conf.uri.gb_server + conf.uri.public;
    return rpc.post(uri, { id: 8, method: "webGames", params: [] });
  } catch (error) {
    throw new Error("get listGames error");
  }
};

config.traderConfig = () =>
  new Promise((resovle, reject) => {
    try {
      if (window.traderConfig) {
        return resovle();
      }
      const uri = conf.uri.gb_server + conf.uri.public;
      return rpc
        .post(uri, { id: 8, method: "walletConfig", params: [] })
        .then(resp => {
          window.traderConfig = resp.data;
          resovle();
        });
    } catch (error) {
      reject();
      throw new Error("get listGames error");
    }
  });
export default config;
