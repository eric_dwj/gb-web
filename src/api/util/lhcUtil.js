import config from "../../config";
const lhcUtil = () => {};

lhcUtil.isNumberValid = number => {
  let res = false;
  for (const num of window.lhc.lhcnumber) {
    if (String(num) === String(number)) {
      res = true;
    }
  }
  return res;
};
lhcUtil.isXiaoValid = xiao => {
  let res = false;
  for (const txiao of window.lhc.lhcxiao) {
    if (String(txiao) === String(xiao)) {
      res = true;
    }
  }
  return res;
};
lhcUtil.isBoseValid = bose => {
  let res = false;
  for (const bs of window.lhc.lhcbose) {
    if (String(bs) === String(bose)) {
      res = true;
    }
  }
  return res;
};
lhcUtil.isJiaXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.jiayexiao.jia) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.isYeXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.jiayexiao.ye) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.isQianXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.qianhouxiao.qian) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.isHouXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.qianhouxiao.hou) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.isTianXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.tiandixiao.tian) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.isDiXiao = xiao => {
  let result = false;
  for (const shenxiao of window.lhc.tiandixiao.di) {
    if (shenxiao === xiao) {
      result = true;
    }
  }
  return result;
};
lhcUtil.numShenXiao = number => {
  let result;
  for (const shenxiao in window.lhc.shengxiao) {
    if (window.lhc.shengxiao.hasOwnProperty(shenxiao)) {
      const arr = window.lhc.shengxiao[shenxiao];
      for (const num of arr) {
        if (String(num) === String(number)) {
          result = shenxiao;
        }
      }
    }
  }
  return result;
};
lhcUtil.getAllBose = () => {
  const boses = [];
  for (const bose in window.lhc.bose) {
    boses.push(bose);
  }
  return boses;
};
lhcUtil.getXiaoNumbers = xiao => {
  return window.lhc.shengxiao[xiao];
};

lhcUtil.numBose = number => {
  let result;
  for (const bose in window.lhc.bose) {
    if (window.lhc.bose.hasOwnProperty(bose)) {
      const arr = window.lhc.bose[bose];
      for (const num of arr) {
        if (String(num) === String(number)) {
          result = bose;
        }
      }
    }
  }
  return result;
};
lhcUtil.numWuxing = number => {
  let result;
  for (const wuxing in window.lhc.wuxing) {
    if (window.lhc.wuxing.hasOwnProperty(wuxing)) {
      const arr = window.lhc.wuxing[wuxing];
      for (const num of arr) {
        if (String(num) === String(number)) {
          result = wuxing;
        }
      }
    }
  }
  return result;
};
lhcUtil.getWei = number => {
  return String(number).split("")[1];
};
lhcUtil.getWeiNumbers = wei => {
  return window.lhc.lhcwei[wei];
};
lhcUtil.convertXiaoNames = arr => {
  let res = [];
  for (const xiao of arr) {
    res.push(window.lhc.lhcxiaoname[xiao]);
  }
  return res.join(",");
};
lhcUtil.convertWeiNames = arr => {
  let res = [];
  for (const wei of arr) {
    res.push(`${wei}${config.CONST_WEI}`);
  }
  return res.join(",");
};
lhcUtil.getXiaoNames = () => {
  return window.lhc.lhcxiaoname;
};
lhcUtil.getXiaos = () => {
  return window.lhc.lhcxiao;
};
export default lhcUtil;
