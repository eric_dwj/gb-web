const EventEmitter = {
  RESET_WITHDRAW_EVENTS: "RESET_WITHDRAW_EVENTS",
  RESET_LOGIN_EVENTS: "RESET_LOGIN_EVENTS",
  RESET_REGISTER_EVENTS: "RESET_REGISTER_EVENTS",
  DEPO_NOTIFY_EVENT: "DEPO_NOTIFY_EVENT",
  _events: {},
  dispatch: function(event, data) {
    if (!this._events[event]) return;
    this._events[event].forEach(callback => callback(data));
  },
  subscribe: function(event, callback) {
    if (!this._events[event]) this._events[event] = [];
    this._events[event].push(callback);
  }
};

module.exports = { EventEmitter };
