import axios from "axios";
const axiosUtil = () => {};
axiosUtil.get = url => {
  try {
    let headers;
    if (window.user) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.user.token}`
      };
    } else {
      headers = {
        "Content-Type": "application/json"
      };
    }
    return axios.get(url, { headers });
  } catch (error) {
    throw new Error("axiosUtil.get error url:" + url);
  }
};
axiosUtil.post = (url, data) => {
  try {
    let headers;
    if (window.user) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.user.token}`
      };
    } else {
      headers = {
        "Content-Type": "application/json"
      };
    }
    return axios.post(url, data, { headers });
  } catch (error) {
    throw new Error(
      "axiosUtil.post error url: " + url + " data: " + JSON.stringify(data)
    );
  }
};
export default axiosUtil;
