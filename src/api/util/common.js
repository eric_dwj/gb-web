const CryptoJS = require("crypto-js");
const _ = require("lodash");
const md5 = require("md5");
const Big = require("big.js");
const moment = require("moment");
const QRCode = require("qrcode");
const CommonUtil = () => {};
CommonUtil.merge = (x, y) => _.merge(x, y);
CommonUtil.qrcode = data =>
  new Promise((resolve, reject) => {
    QRCode.toDataURL(data, (err, url) => {
      resolve(url);
    });
  });

CommonUtil.mod = (a, b) => new Big(a).mod(new Big(b)).toString();
CommonUtil.div = (a, b) => new Big(a).div(new Big(b)).toString();
CommonUtil.minus = (a, b) => new Big(a).minus(new Big(b)).toString();
CommonUtil.add = (a, b) => new Big(a).add(new Big(b)).toString();
CommonUtil.times = (a, b) => new Big(a).times(new Big(b)).toString();
CommonUtil.validateEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

CommonUtil.toTS = date => {
  return moment(date).unix();
};

CommonUtil.covertTs2MMSS = ts => {
  return moment.unix(ts).format("mm:ss");
};
CommonUtil.covertTs2YYYYMMSSHHMMSS = ts => {
  return moment.unix(ts).format("YYYY-MM-DD HH:mm:ss");
};
CommonUtil.getTS = () => {
  return moment().unix();
};
CommonUtil.getTSOfTodayFromHours = (hour, minute, second) => {
  return moment()
    .hour(hour)
    .minute(minute)
    .second(second)
    .unix();
};
CommonUtil.getCurrentMinutes = () => {
  return moment().format("m");
};
CommonUtil.getCurrentHour = () => {
  return moment().format("H");
};
CommonUtil.getDateTimeYYYYMMDD = () => {
  return moment().format("YYYYMMDD");
};
CommonUtil.today = () => {
  return moment().format("YYYY-MM-DD");
};
CommonUtil.tomorrow = () => {
  return moment(new Date())
    .add(1, "days")
    .format("YYYY-MM-DD");
};
CommonUtil.getCurrentDateTime = () => {
  return moment().format("YYYY-MM-DD HH:mm:ss");
};
CommonUtil.getCurrentDate = () => {
  return moment().format("YYYY-MM-DD");
};
CommonUtil.getTSFromDate = datetime => {
  return moment(datetime).unix();
};
CommonUtil.get00Number = num => {
  if (num < 10) {
    return "0" + num;
  } else {
    return String(num);
  }
};
CommonUtil.get000Number = num => {
  if (num < 10) {
    return "00" + num;
  } else {
    return "0" + num;
  }
};
CommonUtil.getFormatDate = date => {
  const year = date.getFullYear();
  let month = date.getMonth() + 1;
  let strDate = date.getDate();
  if (month >= 1 && month <= 9) {
    month = "0" + month;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
  }
  const FormatDate = year + month + strDate;
  return FormatDate;
};

CommonUtil.sleep = ms => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

CommonUtil.encrypt = (data, password) =>
  new Promise((resolve, reject) => {
    let encryptedkeystore;
    try {
      encryptedkeystore = CryptoJS.AES.encrypt(data, password);
    } catch (exception) {
      reject(exception);
    }
    resolve(encryptedkeystore);
  });

CommonUtil.md5 = message => md5(message);
CommonUtil.decrypt = (data, password) =>
  new Promise((resolve, reject) => {
    let keyStore;
    try {
      const bytes = CryptoJS.AES.decrypt(data, password);
      keyStore = bytes.toString(CryptoJS.enc.Utf8);
    } catch (exception) {
      reject(exception);
    }
    resolve(keyStore);
  });
module.exports = CommonUtil;
