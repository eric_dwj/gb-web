// import common from "../util/common";
import config from "../util/config";
import conf from "../../config";
import rpc from "../util/rpc";
import common from "../util/common";
const withdrawServ = () => {};
withdrawServ.updateStatus = (coin, selected) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = {
      id: 8,
      method: "updateStatus",
      params: [String(coin).toUpperCase(), selected]
    };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get historyAddress error");
  }
};
withdrawServ.listWithdrawTx = (coin, start, end, oper) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = {
      id: 8,
      method: "getWithdrawTxs",
      params: [
        String(coin).toUpperCase(),
        common.getTSFromDate(start),
        common.getTSFromDate(end),
        oper
      ]
    };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get historyAddress error");
  }
};
withdrawServ.historyAddress = coin => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = {
      id: 8,
      method: "getSendAddress",
      params: [String(coin).toUpperCase()]
    };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get historyAddress error");
  }
};

withdrawServ.assetTx = asset => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }

  try {
    const data = { id: 8, method: "listWithdraws", params: [asset] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get assetTx error");
  }
};

withdrawServ.traderConfig = () => config.traderConfig();
withdrawServ.today = () => common.today();
withdrawServ.tomorrow = () => common.tomorrow();
withdrawServ.submit = (to, value, coin) => {
  try {
    const data = { id: 8, method: "withdraw", params: [to, value, coin] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("submit withdraw error");
  }
};
withdrawServ.getDashboardURI = () => conf.dash_board_uri;

export default withdrawServ;
