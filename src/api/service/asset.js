import conf from "../../config";
import rpc from "../util/rpc";
import tool from "../util/common";
import config from "../util/config";
const assetServ = () => {};
assetServ.covertTs2YYYYMMSSHHMMSS = ts => tool.covertTs2YYYYMMSSHHMMSS(ts);

assetServ.merge = (x, y) => tool.merge(x, y);

assetServ.transfer = (userid, asset, size) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = { id: 8, method: "transfer", params: [userid, asset, size] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get transfer error");
  }
};

assetServ.exchangeAsset = (size, coin, side) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = { id: 8, method: "exchangeAsset", params: [size, coin, side] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get assetTx error");
  }
};

assetServ.setBalance = balance => {
  window.balance = balance;
};

assetServ.traderConfig = () => config.traderConfig();

assetServ.calculateSell = (asset, lotty, amount) => {
  if (!amount) {
    return parseFloat(
      tool.div(tool.times(lotty.Price, lotty.Available), asset.Price)
    ).toFixed(3);
  } else {
    return parseFloat(
      tool.div(tool.times(lotty.Price, amount), asset.Price)
    ).toFixed(3);
  }
};

assetServ.calculateBuy = (asset, lotty, amount) => {
  if (!amount) {
    return parseFloat(
      tool.div(tool.times(asset.Price, asset.Available), lotty.Price)
    ).toFixed(3);
  } else {
    return parseFloat(
      tool.div(tool.times(asset.Price, amount), lotty.Price)
    ).toFixed(3);
  }
};

assetServ.listProfit = business => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = { id: 8, method: "listProfit", params: [business] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("listProfit error");
  }
};

assetServ.getBalance = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = { id: 8, method: "getBalance", params: [] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get assetTx error");
  }
};
export default assetServ;
