import common from "../util/common";
import config from "../util/config";
import conf from "../../config";
import rpc from "../util/rpc";
const depositServ = () => {};

// depositServ.receive = coin => {
//   if (!window.user || !window.user.user || !window.user.user.userid) {
//     return null;
//   }
//   try {
//     const data = { id: 8, method: "sendReceive", params: [coin] };
//     return rpc.post(conf.uri.gb_server + conf.uri.user, data);
//   } catch (error) {
//     throw new Error("get assetTx error");
//   }
// };
depositServ.getBalance = (coin, address) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri =
      conf.uri.gb_server +
      conf.uri.proxy +
      conf.uri[`${String(coin).toLowerCase()}_balance`];
    return rpc.post(uri, { address, tokenName: coin });
  } catch (error) {
    throw new Error("get assetTx error");
  }
};
depositServ.assetTx = asset => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const data = { id: 8, method: "listDeposits", params: [asset] };
    return rpc.post(conf.uri.gb_server + conf.uri.user, data);
  } catch (error) {
    throw new Error("get assetTx error");
  }
};
depositServ.traderConfig = () => config.traderConfig();
depositServ.sleep = ms => common.sleep(ms);
depositServ.qrcode = code => common.qrcode(code);
export default depositServ;
