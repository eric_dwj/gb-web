import tool from "../util/common";
import conf from "../../config";

import rpc from "../util/rpc";
const resultServ = () => {};
resultServ.getResultsURI = () => conf.results_uri;
resultServ.covertTs2YYYYMMSSHHMMSS = (ts) => tool.covertTs2YYYYMMSSHHMMSS(ts);
resultServ.listResult = (business, date) => {
  const jsonData = {
    id: 8,
    method: "results",
    params: [business, date, 0, 999999]
  };
  return rpc.post(conf.uri.gb_server + conf.uri.public, jsonData);
};
export default resultServ;
