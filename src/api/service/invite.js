import conf from "../../config";
import rpc from "../util/rpc";
import common from "../util/common";
import config from "../util/config";
const inviteServ = () => {};
inviteServ.QRCode = code => common.qrcode(code);
inviteServ.generateCode = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  const data = { id: 8, method: "addCommission", params: [] };
  return rpc.post(conf.uri.gb_server + conf.uri.user, data);
};

inviteServ.getParent = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  const data = { id: 8, method: "getParent", params: [] };
  return rpc.post(conf.uri.gb_server + conf.uri.user, data);
};

inviteServ.listInvited = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  const data = { id: 8, method: "getIssuedCommissions", params: [] };
  return rpc.post(conf.uri.gb_server + conf.uri.user, data);
};
inviteServ.listPending = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  const data = { id: 8, method: "getPendingCommissions", params: [] };
  return rpc.post(conf.uri.gb_server + conf.uri.user, data);
};
inviteServ.covertTs2YYYYMMSSHHMMSS = ts => common.covertTs2YYYYMMSSHHMMSS(ts);
inviteServ.traderConfig = () => config.traderConfig();
export default inviteServ;
