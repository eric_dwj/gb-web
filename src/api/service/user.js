import conf from "../../config";
import rpc from "../util/rpc";
const userServ = () => { };

userServ.setSession = data => {
  window.localStorage.setItem("user", JSON.stringify(data));
  window.user = data;
};

userServ.logout = () => {
  // window.sessionStorage.removeItem("user");
  window.localStorage.removeItem("user");
};

userServ.getSession = () => {
  //const session = window.sessionStorage.getItem("user");
  const session = window.localStorage.getItem("user");
  if (session) {
    try {
      window.user = JSON.parse(session);
    } catch (error) {
      window.user = null;
    }

    if (window.user && window.user.user) {
      return true;
    }
  }
  return false;
};

userServ.register = data => {
  const jsonData = {
    id: 8,
    method: "register",
    params: [
      data.password,
      data.repeatPassword,
      data.email,
      data.name,
      data.phone,
      data.qq,
      data.code,
      data.captcha
    ]
  };
  return rpc.post(conf.uri.gb_server + conf.uri.public, jsonData);
};

userServ.login = data => {
  const jsonData = {
    id: 8,
    method: "login",
    params: [data.email, data.password, data.captcha]
  };
  return rpc.post(conf.uri.gb_server + conf.uri.public, jsonData);
};

export default userServ;
