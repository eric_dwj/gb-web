import config from "../util/config";
import userServ from "./user";

userServ.getSession();
const onStart = () => {};
// todo
onStart.loadAfter = () => {
  config.loadResource().then(resp => {
    window.resource = resp.data.resource;
  });
};
// todo
onStart.loadBefore = () =>
  new Promise((resolve, reject) => {
    try {
      Promise.all([config.listGames()]).then(_resps => {
        window.gameTags = _resps[0].data.data;
        window.settlement = _resps[0].data.settlement;
        window.businesses = _resps[0].data.config;
        window.mobile_server = _resps[0].data.mobile_server;
        window.lhc = _resps[0].data.lhc;
        window.conf = {};
        for (let index = 0; index < _resps[0].data.config.length; index++) {
          const conf = _resps[0].data.config[index];
          window.conf[conf.business] = conf;
        }
        resolve(true);
      });
    } catch (error) {
      window.error = error.message;
      reject(false);
    }
  });
export default onStart;
