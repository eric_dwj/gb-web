import socket from "../util/socketio";
import common from "../util/common";
import conf from "../../config";
import rpc from "../util/rpc";
let orders = {};

const note = () => {};
note.game = {};
note.add = (x, y) => common.add(x, y);
note.register = (business, cb) => socket.registerFinalTimerEvent(business, cb);
// eslint-disable-next-line prettier/prettier
note.addOrder = (type, business, name, round, size, number, times, settlement, start, end) => {
  // eslint-disable-next-line prettier/prettier
  orders[`${business}_${settlement}_${number}_${start}_${end}`] = { type, business, name, round, size, number, times, settlement, start, end };
};

note.updateOrder = (key, size) => {
  if (!size) {
    size = "0";
  }
  orders[key].size = size;
};

note.sendOrder = () => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }

  for (const key in orders) {
    if (orders.hasOwnProperty(key)) {
      const order = orders[key];
      if (!order.size) {
        delete orders[key];
      }
    }
  }

  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, { id: 8, method: "putOrder", params: orders });
  } catch (error) {
    throw new Error(`note.sendOrder error: ${JSON.stringify(error)}`);
  }
};

note.handleLatestOrders = orders => {
  if (Array.isArray(orders)) {
    note.latestOrders = orders;
  } else {
    return note.latestOrders;
  }
};

note.getOrder = () => {
  return orders;
};

note.orderTotal = () => {
  let total = 0;
  for (const key in orders) {
    if (orders.hasOwnProperty(key)) {
      const order = orders[key];
      total = common.add(total, order.size);
    }
  }
  return total.toString();
};

note.resetOrder = () => {
  orders = {};
};

note.setNextRound = (business, nextRound) => {
  if (!note.game[business]) {
    note.game[business] = {};
  }
  note.game[business].nextRound = nextRound;
};
note.getOrderDetail = (business, orderid) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, {
      id: 8,
      method: "getOrderDetail",
      params: [business, orderid]
    });
  } catch (error) {
    throw new Error(`note.getOrderDetail error: ${JSON.stringify(error)}`);
  }
};
note.getPendingOrders = business => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, {
      id: 8,
      method: "getPendingOrders",
      params: [business]
    });
  } catch (error) {
    throw new Error(`note.getPendingOrders error: ${JSON.stringify(error)}`);
  }
};

note.getOrderHistory = (business, start, end) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, {
      id: 8,
      method: "getOrderHistory",
      params: [business, common.toTS(start), common.toTS(end)]
    });
  } catch (error) {
    throw new Error(`note.getOrderHistory error: ${JSON.stringify(error)}`);
  }
};

note.getTodaySettled = (business, date) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, {
      id: 8,
      method: "getTodaySettled",
      params: [business, common.toTS(date)]
    });
  } catch (error) {
    throw new Error(`note.getTodaySettled error: ${JSON.stringify(error)}`);
  }
};

note.getOrdersFromServer = (business, round) => {
  if (!window.user || !window.user.user || !window.user.user.userid) {
    return null;
  }
  try {
    const uri = conf.uri.gb_server + conf.uri.user;
    return rpc.post(uri, {
      id: 8,
      method: "getLatestOrders",
      params: [business, round]
    });
  } catch (error) {
    throw new Error(`note.getOrdersFromServer error: ${JSON.stringify(error)}`);
  }
};

note.getNextRound = business => {
  if (!note.game || !note.game[business]) {
    return null;
  }
  return note.game[business].nextRound;
};

export default note;
