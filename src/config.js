const _ = require("lodash");
const constant = require("./const.js");
const config = {
  socket_io: {
    conection: "http://localhost:1388",
    timer_final_event: "TIMER_FINAL_EVENT"
  },
  uri: {
    gb_server: "http://localhost:1388",
    public: "/public",
    user: "/user"
  },
  LOTTER_COIN: "LOT",
  results_uri: "/games/results",
  unsettlement_uri: "/games/unsettlement",
  today_settlement_uri: "/games/today/settlement",
  report_uri: "/games/report",
  dash_board_uri: "/games/dashboard",
  captcha: "/public/captcha"
};
const mergedConfig = _.merge(constant, config);
module.exports = mergedConfig;
