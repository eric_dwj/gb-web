import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import LocalUserRegister from "./LocalUserRegister";

const styles = theme => ({
  root: {
    "& > * + *": {
      marginTop: theme.spacing.unit * 2
    }
  },
  or: {
    textAlign: "center"
  }
});

class Register extends Component {
  static propTypes = {
    onRegister: PropTypes.func,
    registerFailed: PropTypes.string,
    disableLocal: PropTypes.bool,
    classes: PropTypes.object,
    ts: PropTypes.string
  };

  render() {
    const {
      classes,
      onRegister,
      registerFailed,
      disableLocal,
      ts
    } = this.props;

    return (
      <div className={classes.root}>
        {!disableLocal && (
          <LocalUserRegister
            onRegister={onRegister}
            registerFailed={registerFailed}
            ts={ts}
          />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(Register);
