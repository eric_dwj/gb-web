import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Formsy from "formsy-react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import Config from "../../config";
import ValidatedTextField from "./components/ValidatedTextField";
import LoginRegisterError from "./components/LoginRegisterError";

const styles = theme => ({
  root: {},
  form: {
    display: "flex",
    flexDirection: "column"
  },
  field: {
    marginTop: theme.spacing.unit
  },
  actions: {
    marginTop: theme.spacing.unit * 2
  }
});

class LocalUserLogin extends Component {
  static propTypes = {
    onLogin: PropTypes.func,
    loginFailed: PropTypes.string,
    ts: PropTypes.string,
    classes: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      canSubmit: false
    };
  }

  render() {
    const { classes, loginFailed, ts } = this.props;
    const { canSubmit } = this.state;
    return (
      <div className={classes.root}>
        <Formsy
          className={classes.form}
          onValid={this.enableSubmit}
          onInvalid={this.disableSubmit}
          onValidSubmit={this.submit}
        >
          <ValidatedTextField
            type="email"
            name="email"
            autoComplete="email"
            validations="minLength:3"
            validationErrors={{
              minLength: Config.CONST_YJGSCW
            }}
            required
            className={classes.field}
            label={Config.CONST_YHMEAIL}
            value={`admin@bole.com`}
          />

          <ValidatedTextField
            type="password"
            name="password"
            autoComplete="password"
            validations="minLength:2"
            validationErrors={{
              minLength: Config.CONST_YJGSCW
            }}
            required
            className={classes.field}
            label={Config.CONST_MIMA}
            value={`admin@bole.com`}
          />
          <Grid container spacing={24}>
            <Grid item xs={6}>
              <ValidatedTextField
                name="captcha"
                autoComplete="captcha"
                validations="minLength:4"
                validationErrors={{
                  minLength: Config.CONST_YZMCDCW
                }}
                required
                className={classes.field}
                label={Config.CONST_YANZHENGMA}
              />
            </Grid>
            <Grid item xs={6}>
              <img
                src={`${Config.uri.gb_server}${Config.captcha}?${ts}`}
                alt=""
              />
            </Grid>
          </Grid>

          {loginFailed && <LoginRegisterError message={loginFailed} />}

          <div className={classes.actions} style={{ marginTop: 230 }}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={!canSubmit}
            >
              {Config.CONST_DENGLU}
            </Button>
          </div>
        </Formsy>
      </div>
    );
  }

  disableSubmit = () => {
    this.setState({ canSubmit: false });
  };

  enableSubmit = () => {
    this.setState({ canSubmit: true });
  };

  submit = model => {
    if (this.props.onLogin) {
      this.props.onLogin(model);
    }
  };
}

export default withStyles(styles)(LocalUserLogin);
