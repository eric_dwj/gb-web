import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Formsy from "formsy-react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import Config from "../../config";
import ValidatedTextField from "./components/ValidatedTextField";
import LoginRegisterError from "./components/LoginRegisterError";

const styles = theme => ({
  root: {},
  form: {
    display: "flex",
    flexDirection: "column"
  },
  field: {
    marginTop: theme.spacing.unit
  },
  actions: {
    marginTop: theme.spacing.unit * 2
  }
});

class LocalUserRegister extends Component {
  static propTypes = {
    onRegister: PropTypes.func,
    registerFailed: PropTypes.string,
    ts: PropTypes.string,
    classes: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      canSubmit: false
    };
  }

  render() {
    const { classes, registerFailed, ts } = this.props;
    const { canSubmit } = this.state;
    return (
      <div className={classes.root}>
        <Formsy
          className={classes.form}
          onValid={this.enableSubmit}
          onInvalid={this.disableSubmit}
          onValidSubmit={this.submit}
        >
          <ValidatedTextField
            name="email"
            autoComplete="email"
            validations="minLength:3"
            validationErrors={{
              minLength: Config.CONST_YHMCDTD
            }}
            required
            className={classes.field}
            label={Config.CONST_QSRYHM}
          />

          <ValidatedTextField
            type="password"
            name="password"
            autoComplete="password"
            validations="minLength:2"
            validationErrors={{
              minLength: Config.CONST_MMCDTD
            }}
            required
            className={classes.field}
            label={Config.CONST_CJYGMA}
          />

          <ValidatedTextField
            type="password"
            name="repeatPassword"
            autoComplete="repeatPassword"
            validations="equalsField:password"
            validationErrors={{
              equalsField: Config.CONST_CFMM
            }}
            required
            className={classes.field}
            label={Config.CONST_CHONGFUMIMA}
          />

          <ValidatedTextField
            name="qq"
            autoComplete="qq"
            validations="minLength:6"
            validationErrors={{
              minLength: Config.CONST_QQCDTD
            }}
            required
            className={classes.field}
            label={Config.CONST_QSRQQ}
          />

          <ValidatedTextField
            name="name"
            autoComplete="name"
            validations="minLength:2"
            validationErrors={{
              minLength: Config.CONST_ZSXMCDTD
            }}
            required
            className={classes.field}
            label={Config.CONST_SHURUNAME}
          />

          <ValidatedTextField
            name="phone"
            autoComplete="phone"
            validations="minLength:9"
            validationErrors={{
              minLength: Config.CONST_DHCDTD
            }}
            required
            className={classes.field}
            label={Config.CONST_SHURUDIANHUA}
          />

          <ValidatedTextField
            name="code"
            autoComplete="code"
            validations="minLength:16"
            validationErrors={{
              minLength: Config.CONST_YQMTD
            }}
            required
            className={classes.field}
            label={Config.CONST_YAOQIMA}
          />

          <Grid container spacing={24}>
            <Grid item xs={6}>
              <ValidatedTextField
                name="captcha"
                autoComplete="captcha"
                validations="minLength:4"
                validationErrors={{
                  minLength: Config.CONST_YZMCW
                }}
                required
                className={classes.field}
                label={Config.CONST_YANZHENGMA}
              />
            </Grid>
            <Grid item xs={6}>
              <img
                src={`${Config.uri.gb_server}${Config.captcha}?${ts}`}
                alt=""
              />
            </Grid>
          </Grid>

          {registerFailed && <LoginRegisterError message={registerFailed} />}

          <div className={classes.actions}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={!canSubmit}
            >
              {Config.CONST_ZHUCHE}
            </Button>
          </div>
        </Formsy>
      </div>
    );
  }

  disableSubmit = () => {
    this.setState({ canSubmit: false });
  };

  enableSubmit = () => {
    this.setState({ canSubmit: true });
  };

  submit = model => {
    if (this.props.onRegister) {
      this.props.onRegister(model);
    }
  };
}

export default withStyles(styles)(LocalUserRegister);
