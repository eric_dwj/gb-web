import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import LocalUserLogin from "./LocalUserLogin";

const styles = theme => ({
  root: {
    "& > * + *": {
      marginTop: theme.spacing.unit * 2
    }
  },
  or: {
    textAlign: "center"
  }
});

class Login extends Component {
  static propTypes = {
    onLogin: PropTypes.func,
    loginFailed: PropTypes.string,
    disableLocal: PropTypes.bool,
    classes: PropTypes.object,
    ts: PropTypes.string
  };

  render() {
    const { classes, onLogin, loginFailed, disableLocal, ts } = this.props;
    return (
      <div className={classes.root}>
        {!disableLocal && (
          <LocalUserLogin onLogin={onLogin} loginFailed={loginFailed} ts={ts} />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(Login);
