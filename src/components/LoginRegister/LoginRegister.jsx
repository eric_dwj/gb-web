import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Card from "@material-ui/core/Card";
import Fade from "@material-ui/core/Fade";
import PropTypes from "prop-types";
import TabContent from "./components/TabContent";
import Login from "./Login";
import Register from "./Register";
import { EventEmitter } from "../../api/util/events";
import Config from "../../config";
const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  card: {
    flexGrow: 1,
    height: "658px",
    padding: 20,
    [theme.breakpoints.up("sm")]: {
      flexBasis: "25rem",
      flexGrow: 0
    }
  }
});

class LoginRegister extends Component {
  static propTypes = {
    transitionTimeout: PropTypes.number,
    header: PropTypes.element,
    footer: PropTypes.element,
    onLogin: PropTypes.func,
    loginFailed: PropTypes.string,
    registerFailed: PropTypes.string,
    onRegister: PropTypes.func,
    onLoginWithProvider: PropTypes.func,
    onRegisterWithProvider: PropTypes.func,
    disableLocal: PropTypes.bool,
    disableRegister: PropTypes.bool,
    disableRegisterProviders: PropTypes.bool,
    classes: PropTypes.object
  };

  static defaultProps = {
    transitionTimeout: 1000
  };

  constructor(props) {
    super(props);

    this.state = {
      tab: 0,
      err: null,
      ts: ""
    };
  }

  componentDidMount = () => {
    EventEmitter.subscribe(EventEmitter.RESET_LOGIN_EVENTS, code => {
      if (code === 405) {
        this.setState(
          {
            tab: 0,
            err: Config.CONST_YZMCW,
            ts: new Date().getTime()
          },
          () => {
            setInterval(() => {
              this.setState({ err: null });
            }, 2 * 1000);
          }
        );
      }
    });

    EventEmitter.subscribe(EventEmitter.RESET_REGISTER_EVENTS, code => {
      if (code === 405) {
        this.setState(
          {
            tab: 1,
            err: Config.CONST_YZMCW,
            ts: new Date().getTime()
          },
          () => {
            setInterval(() => {
              this.setState({ err: null });
            }, 2 * 1000);
          }
        );
      }
    });
  };
  render() {
    const {
      classes,
      transitionTimeout,
      header,
      footer,
      onLogin,
      onRegister,
      loginFailed,
      registerFailed,
      disableLocal,
      disableRegister
    } = this.props;

    const { tab } = this.state;

    let activeTab;
    switch (tab) {
      case 0:
        activeTab = (
          <TabContent>
            <Login
              onLogin={onLogin}
              loginFailed={this.state.err ? this.state.err : loginFailed}
              disableLocal={disableLocal}
              ts={this.state.ts}
            />
          </TabContent>
        );
        break;

      case 1:
        activeTab = (
          <TabContent>
            <Register
              onRegister={onRegister}
              registerFailed={this.state.err ? this.state.err : registerFailed}
              disableLocal={disableLocal}
              ts={this.state.ts}
            />
          </TabContent>
        );
        break;
      default:
        break;
    }

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          {header && <div>{header}</div>}

          {!disableLocal && !disableRegister && (
            <Tabs
              value={this.state.tab}
              onChange={this.handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
            >
              <Tab label={Config.CONST_DENGLU} />
              {!disableRegister && <Tab label={Config.CONST_ZHUCHE} />}
            </Tabs>
          )}

          {transitionTimeout > 0 ? (
            <Fade key={tab} in={true} timeout={transitionTimeout}>
              {activeTab}
            </Fade>
          ) : (
            activeTab
          )}

          {footer && <div>{footer}</div>}
        </Card>
      </div>
    );
  }

  handleTabChange = (event, value) => {
    this.setState({ tab: value });
  };
}

export default withStyles(styles)(LoginRegister);
