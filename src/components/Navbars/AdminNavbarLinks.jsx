import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Person from "@material-ui/icons/Person";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "components/CustomButtons/Button.jsx";
import Modal from "@material-ui/core/Modal";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import Fab from "@material-ui/core/Fab";
import InputIcon from "@material-ui/icons/Input";
import HelpIcon from "@material-ui/icons/Help";
import { EventEmitter } from "../../api/util/events";
import LoginRegister from "../LoginRegister/LoginRegister";
import userServ from "../../api/service/user";

function getModalStyle() {
  const top = 50;
  const left = 50;
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 50,
    boxShadow: theme.shadows[5],
    outline: "none"
  }
});

class HeaderLinks extends React.Component {
  state = {
    open: false,
    islogin: false,
    openLogout: false
  };

  componentDidMount = () => {
    if (userServ.getSession()) {
      this.setState({ islogin: true });
    }
  };
  handleClickLogoutOpen = () => {
    this.setState({ openLogout: true });
  };

  handleClickLogoutConfirm = () => {
    userServ.logout();
    window.location.href = "/games/games";
  };

  handleClickLogoutClose = () => {
    this.setState({ openLogout: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };

  handleLogin = content => {
    userServ.login(content).then(resp => {
      if (resp && resp.data && resp.data.data && resp.data.data.token) {
        userServ.setSession(resp.data.data);
        window.location.href = "/games/games";
      } else if (resp && resp.data && resp.data.code) {
        EventEmitter.dispatch(EventEmitter.RESET_LOGIN_EVENTS, resp.data.code);
      }
    });
  };

  handleRegister = content => {
    userServ.register(content).then(resp => {
      if (resp.data.state === "success") {
        userServ.setSession(resp.data.data);
        this.handleClose();
        this.setState({ islogin: true });
      } else if (resp && resp.data && resp.data.code) {
        EventEmitter.dispatch(
          EventEmitter.RESET_REGISTER_EVENTS,
          resp.data.code
        );
      }
    });
  };

  render() {
    const header = (
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            {`欢迎光临`}
          </Typography>
        </Toolbar>
      </AppBar>
    );

    const footer = (
      <div>
        <Typography variant="caption" align="center">
         {`欢迎光临`}
        </Typography>
      </div>
    );
    // eslint-disable-next-line react/prop-types
    const { classes } = this.props;
    return (
      <div>
        {!this.state.islogin && (
          <Button
            aria-label="Person"
            className={classes.buttonLink}
            onClick={this.handleOpen}
          >
            {`注册 / 登录`}
            <Person className={classes.icons} />
          </Button>
        )}
        {this.state.islogin && window.user.user && (
          <div>
            <Fab
              color="primary"
              variant="extended"
              className={classes.fab}
              style={{ marginRight: 20, fontSize: 20 }}
            >
              <Person className={classes.icons} />
              {`欢迎光临: ${window.user.user.name}`}
            </Fab>
            <Fab
              variant="extended"
              className={classes.fab}
              onClick={this.handleClickLogoutOpen}
              style={{
                marginRight: 20,
                fontSize: 20,
                color: "white",
                backgroundColor: "#E53935"
              }}
            >
              <InputIcon className={classes.extendedIcon} />
              登出
            </Fab>
          </div>
        )}
        <Modal open={this.state.open} onClose={this.handleClose}>
          <div style={getModalStyle()} className={classes.paper}>
            <LoginRegister
              header={header}
              footer={footer}
              onLogin={this.handleLogin}
              onRegister={this.handleRegister}
            />
          </div>
        </Modal>
        <Dialog
          open={this.state.openLogout}
          onClose={this.handleClickLogoutClose}
        >
          <DialogTitle>
            <HelpIcon color="secondary" /> {"您即将退出当前用户"}
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {`您确认登出?`}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClickLogoutConfirm} variant="contained">
              {`确认`}
            </Button>
            <Button onClick={this.handleClickLogoutClose} variant="contained">
              {`取消`}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(HeaderLinks);
