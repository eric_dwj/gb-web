import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Hidden from "@material-ui/core/Hidden";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import AdminNavbarLinks from "./AdminNavbarLinks.jsx";
import Button from "components/CustomButtons/Button.jsx";

import headerStyle from "assets/jss/material-dashboard-react/components/headerStyle.jsx";

class Header extends React.Component {
  makeBrand = () => {
    var name;
    this.props.routes.map(prop => {
      if (prop.layout + prop.path === this.props.location.pathname) {
        name = prop.name;
      }
      return null;
    });
    return name;
  };

  render() {
    const { classes, color } = this.props;
    const appBarClasses = classNames({
      [" " + classes[color]]: color
    });

    return (
      <AppBar className={classes.appBar + appBarClasses}>
        <Toolbar className={classes.container}>
          <IconButton
            color="inherit"
            onClick={() => {
              this.props.handleDrawerToggle();
            }}
          >
            {!this.props.open && <Menu />}
          </IconButton>
          <div className={classes.flex}>
            <Button color="transparent" href="#" className={classes.title}>
              {this.makeBrand()}
            </Button>
          </div>
          <Hidden smDown implementation="css">
            {<AdminNavbarLinks />}
          </Hidden>
        </Toolbar>
      </AppBar>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  handleDrawerToggle: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  routes: PropTypes.array.isRequired,
  open: PropTypes.bool.isRequired
};

export default withStyles(headerStyle)(Header);
