/* eslint-disable prettier/prettier */
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Games from "@material-ui/icons/Games";
import List from "@material-ui/icons/List";
import Payment from "@material-ui/icons/Payment";
import Today from "@material-ui/icons/Today";
import Account from "@material-ui/icons/AccountBalanceWallet";
import Description from "@material-ui/icons/Description";
import Recorder from "@material-ui/icons/Receipt";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import Deposit from "views/Assets/Deposit.jsx";
import Withdraw from "views/Assets/Withdraw.jsx";
import User from "views/User/User.jsx";
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import GamesPage from "views/Games/Games.jsx";
import Results from "views/Report/Results.jsx";
import Settlement from "views/Report/Settlement.jsx";
import config from "./config";
const router = [
  { access: { agent: true }, path: "/dashboard", name: config.CONST_DAILIBAOBIAO, rtlName: "Dashboard", icon: Dashboard, component: DashboardPage, layout: "/games" },
  { access: { agent: true, user: true, all: true }, path: "/games", name: config.CONST_CAIPIAO, rtlName: "Games", icon: Games, component: GamesPage, layout: "/games" },
  { access: { agent: true, user: true, all: true }, path: "/results", name: config.CONST_KAIJIANGJIEGUO, rtlName: "Results", icon: List, component: Results, layout: "/games" },
  { access: { agent: false, user: true }, path: "/unsettlement", name: config.CONST_WEIJIEMINGXI, rtlName: "Unsettlement", icon: Description, component: Settlement, layout: "/games" },
  { access: { agent: false, user: true }, path: "/today/settlement", name: config.CONST_JINGRIYIJIE, rtlName: "Settlement", icon: Today, component: Settlement, layout: "/games" },
  { access: { agent: false, user: true }, path: "/report", name: config.CONST_BAOBIAOCHAXUN, rtlName: "report", icon: Recorder, component: Settlement, layout: "/games" },
  { access: { agent: true, user: true }, path: "/user", name: config.CONST_WODEZICHAN, rtlName: "user", icon: PersonPinIcon, component: User, layout: "/games" },
  { access: { agent: true, user: true }, path: "/deposit", name: config.CONST_CUNBI, rtlName: "deposit", icon: Payment, component: Deposit, layout: "/games" },
  { access: { agent: true, user: true }, path: "/withdraw", name: config.CONST_QUBI, rtlName: "withdraw", icon: Account, component: Withdraw, layout: "/games" },
];
let dashboardRoutes = [];
if (!window.user) {
  router.forEach(rec => {
    if (rec.access.all) {
      dashboardRoutes.push(rec);
    }
  })
} else if (window.user.user.role === config.ROLE_FRESHMAN) {
  router.forEach(rec => {
    if (rec.access.user) {
      dashboardRoutes.push(rec);
    }
  })
} else {
  router.forEach(rec => {
    if (rec.access.agent) {
      dashboardRoutes.push(rec);
    }
  })
}
export default dashboardRoutes;
